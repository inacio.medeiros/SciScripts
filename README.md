# SciScripts  
Scripts written to control devices/run experiments/analyze data  

## Dependencies
Putting all dependencies together, the python code depends on [so far]:
- Linux
- Numpy, Scipy, Pandas, Matplotlib
- Portaudio, sounddevice, pyserial
- R, rpy
- Asdf, Hdf5, h5py
- Intan RHA python library [Not avaliable for download anymore, so I added a modified copy in this repository, at `Python3/Deps/Intan_RHA.py`]
- Open-ephys analysis-tools [at github.com/open-ephys/analysis-tools]

Also, some environment variables are expected to be set. You can add it to `~/.bashrc`, or `~/.profile`, or wherever your desktop environment searches for exported variables:
```bash
export DATAPATH=~/Data
export ANALYSISPATH=~/Analysis
```
changing the path to where you find appropriate.


A big TODO is to separate the python Deps folder into its own repository and structure it as a python package. While this is not done, code in other folders (`Python3/Analysis`, `Python3/Exps`...) will only work if `Python3/Deps` and Open-ephys `analysis-tools` repository are in the python path.

(at ~/.local/lib64/python3.6/site-packages/Path.pth, for example)
```bash
/home/User/Git/SciScripts/Python3/Deps/
/home/User/Git/analysis-tools/Python3/
```

## Installation

Below are instructions to make everything work. This assumes that data is on `~/Data`, analysis is on `~/Analysis` and that both this and the Open-ephys `analysis-tools` repositories are on `~/Git`. It also assumes that `R`, `Portaudio` and `HDF5` are already installed.

```bash
$ pip install --user numpy scipy matplotlib pandas sounddevice pyserial rpy2 asdf h5py

$ mkdir ~/{Data,Analysis,Git}

$ cd ~/Git

$ git clone https://gitlab.com/malfatti/SciScripts

$ git clone https://github.com/open-ephys/analysis-tools

$ cd ~/

$ echo 'export DATAPATH=~/Data \nexport ANALYSISPATH=~/Analysis' >> ~/.profile

$ for f in ~/.local/lib*/python3*/site-packages/; do echo "$HOME"/Git/SciScripts/Python3/Deps/ \n"$HOME"/Git/analysis-tools/Python3/ > "$f"/Path.pth; done

```

---
Thawann Malfatti, MSc, PhD student  
Hearing and Neuronal Activity Lab - Brain Institute  
Federal University of Rio Grande do Norte - Natal, Brazil
