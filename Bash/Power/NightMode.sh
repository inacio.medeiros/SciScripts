#!/bin/bash

Night=2850
Day=5500

if [ "${1,,}" == on ]; then
    sudo /home/malfatti/.Scripts/BackLight.sh --set 10
    redshift -P -O $Night
elif [ "${1,,}" == off ]; then
    redshift -P -O $Day
else
    echo 'Usage:'
    echo '    NightMode [on|off]'
    echo ''
fi
