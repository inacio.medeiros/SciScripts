#!/bin/bash 
#SBATCH --time=7-0:0
#SBATCH --cpus-per-task=32
#SBATCH --hint=compute_bound
#SBATCH --exclusive
#SBATCH --qos=qos2

#python3 $SCRIPTSPATH/Python3/Analysis/Units.py
#for f in $SCRATCH_GLOBAL/Analysis/Data/*/*/; do
Here=$(pwd)
cd "$1"
$HOME/.local/bin/klusta Data_Exp00.prm --cluster-only
cd "$Here"
