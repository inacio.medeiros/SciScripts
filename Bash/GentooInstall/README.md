# Installing gentoo

## Preparation

First, boot a USB system and run those commands:

```bash
# bash GentooInstall1.sh
```

This will leave you chrooted inside the new system.


## Inside the new system

```bash
# source /etc/profile
# bash /GentooNewInstall.sh /dev/sdX username
```
