#!/bin/bash

source /GentooInstall/GentooInstall.conf

Cores="$(grep "core id" /proc/cpuinfo | uniq | wc -l)"
Procs="$(grep "processor" /proc/cpuinfo | uniq | wc -l)"
Jobs="$(($Procs/$Cores))"

if [ Threads -lt 2 ]; then
    Jobs=1;
else;
    Jobs=2
fi

EmergeDispatch() {
    Repeat=True

    while [$Repeat = True ]; do
        emerge "$@"
        if [ $? -eq 0 ]; then Repeat=False; else; Repeat=True; fi
        dispatch-conf
    done
}

Portage() {
    emerge-webrsync
    emerge --sync
    eselect news read

    ProfileNo=$(eselect profile list | \
                sed "s/default\/linux\/$Arch\/[0-99]*.[0-9]* (stable)//g" | \
                grep -v default | \
                tail -n 1 | \
                cut -d\] -f1 | cut -d\[ -f2)

    ProfileArray="$(eselect profile list | cut -d[ -f2 | cut -d] -f1 | tail -n +2)"

    echo "Choose your profile:"
    eselect profile list
    echo "[default $ProfileNo]: "
    read Choice

    if [[ " ${ProfileArray[@]} " =~ "$Choice" ]]; then
        eselect locale set Choice
    else;
        echo "Choosing default profile..."
        eselect locale set ProfileNo
    fi

    eselect profile set $Choice

    CFlags=$(gcc -c -Q -march=native --help=target | \
             grep -e march= -e mtune= | \
             sed 's/ //g; s/\t//g' | \
             tr '\n' ' ')

    sed -i "s/\"-j./\"-j$Procs/g;
            s/--jobs=./--jobs=$Jobs/g;
            s/CFLAGS=.*/CFLAGS=\"$CFlags -Os -pipe\"/g" /etc/portage/make.conf

    EmergeDispatch cpuid2cpuflags
    CPUFlags=$(cpuid2cpuflags | sed 's/: /=\"/g')\"

    sed -i "s/CPU_FLAGS_X86=.*/$CPUFlags/g" /etc/portage/make.conf

    EmergeDispatch -uDN @world

    sed -i "s/USE=/\#USE=/g; s/\#\#USE/USE/g" /etc/portage/make.conf
}


Locale() {
    echo $TimeZone > /etc/timezone
    EmergeDispatch --config sys-libs/timezone-data
    sed -i "s/\#$Local/$Local/g; s/\#en_US.UTF-8/en_US.UTF-8/g" /etc/locale.gen
    locale-gen

    LocaleNo="$(eselect locale list | grep $Local | \
                tail -n1 | cut -d\] -f1 | cut -d\[ -f2)"

    LocaleArray="$(eselect locale list | cut -d[ -f2 | cut -d] -f1 | tail -n +2 | head -n -1)"

    echo "Choose your locale:"
    eselect locale list
    echo "[default $LocaleNo]: "
    read Choice

    if [[ " ${LocaleArray[@]} " =~ "$Choice" ]]; then
        eselect locale set Choice
    else;
        echo "Choosing default locale..."
        eselect locale set LocaleNo
    fi

    env-update && source /etc/profile
}


EmergeCore() {
    EmergeDispatch -av gentoolkit gentoo-sources sys-apps/pciutils genkernel linux-firmware app-admin/sysklogd sys-process/cronie sys-apps/mlocate sys-fs/e2fsprogs sys-fs/xfsprogs sys-fs/reiserfsprogs sys-fs/jfsutils sys-fs/dosfstools sys-fs/btrfs-progs ntfs3g net-misc/dhcpcd net-wireless/iw net-wireless/wpa_supplicant sys-boot/grub:2

    rc-update add sysklogd default
    rc-update add cronie default
}


Kernel() {
    Here=$(pwd)
    cp /KernelConfig /usr/src/linux/.config
    cd /usr/src/linux/
    make syncconfig
    make nconfig
    make -j"$Procs"
    make modules_install
    make install
    genkernel --disklabel --install initramfs
    cd $Here
}


Network() {
    Interfaces=$(ip addr | grep -v 'lo:'| grep -E "[[:digit:]]: " | cut -d' ' -f2 | cut -d':' -f1)
    echo "hostname=$User""Tux.LT" > /etc/conf.d/hostname
    echo "dns_domain_lo="LT"" > /etc/conf.d/net

    for i in $Interfaces;
        do echo config_"$i"=\"dhcp\" >> /etc/conf.d/net;
        ln -s /etc/init.d/net.lo /etc/init.d/net."$i";
    done

    head /etc/hosts | sed -i "s/127.0.0.1.*/127.0.0.1 $User'Tux.LT' $User localhost/g"
}


Boot() {
    echo "
    "$Dev"2  /boot  vfat  defaults,noatime  1 2
    "$Dev"3  none   swap  sw                0 0
    "$Dev"4  /      ext4  noatime			0 1
    "$Dev"5  /home  ext4  defaults,noatime  0 1
    tmpfs  /var/tmp/portage  tmpfs  size=75%,uid=portage,gid=portage,mode=775,noatime  0 0

    # For Jack
    none        /tmp/jack    tmpfs  defaults        0       0
    none        /mnt/ramfs   tmpfs  defaults        0       0
    " > /etc/fstab

    grub-install --target=x86_64-efi --efi-directory=/boot
    grub-mkconfig -o /boot/grub/grub.cfg
}


EmergeWorld() {
    EmergeDispatch -uDN --with-bdeps=y --complete-graph  @world
}


EmergeText() {
    EmergeDispatch vim sudo bash-completion w3m dev-vcs/git sys-apps/bat lolcat figlet cmatrix app-misc/screen pfl udisksctl p7zip rar unrar unp texlive pandoc lilypond mpv net-p2p/transmission xdotool
}


EmergeGraphics() {
    EmergeDispatch xorg-server bspwm openbox xterm xcalib xgamma redshift tint2 conky compton feh gimp inkscape audacity ardour pcmanfm qpdfview mupdf geany qutebrowser firefox mednafen pcsx2 qjackctl carla rakarrack
}


EmergeScience() {
    EmergeDispatch gimp inkscape audacity ardour imagej alacritty numpy scipy matplotlib h5py asdf R rpy spyder
}


Users() {
    echo "Enter password for Root:"
    passwd
    echo "Adding user $User..."
    useradd -m -G users,wheel,audio,usb,plugdev -s /bin/bash "${User,,}"
    echo "Enter password for user $User:"
    passwd "${User,,}"
}


echo "Setting up portage..."; Portage
echo "Setting up local..."; Locale
echo "Emerging core programs..."
echo "(might take a while, maybe go get a coffee :)"; EmergeCore
echo "Setting up kernel..."; Kernel
echo "Setting up network..."; Network
echo "Setting up boot..."; Boot
echo "Apply USE flags..."; EmergeWorld
echo "Emerging extra CLI programs..."; EmergeText
echo "Emerging extra GUI programs..."; EmergeGraphics
echo "Emerging extra Science programs..."; EmergeScience
echo "Creating normal user..."; Users
echo 'Done! Now get out of chroot, unmount everything and reboot to your new system \O/'


