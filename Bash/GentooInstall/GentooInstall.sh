#!/bin/bash

## Load user settings

# Get script path - got from Dave Dopson @ https://stackoverflow.com/a/246128
ScriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $ScriptPath/GentooInstall.conf


Partitions() {
    BootSize=$((BootSize + 3))
    SwapSize=$((SwapSize + BootSize))
    RootSize=$((RootSize + SwapSize))

    parted $Device mklabel gpt
    parted $Device mkpart primary 1MiB 3MiB
    parted $Device mkpart primary 3MiB "$BootSize"MiB
    parted $Device mkpart primary "$BootSize"MiB "$SwapSize"MiB
    parted $Device mkpart primary "$SwapSize"MiB "$RootSize"MiB
    parted $Device mkpart primary "$RootSize"MiB -1MiB
    parted set 1 bios_grub on
    parted set 2 boot on
    parted name 1 Bios
    parted name 2 Boot
    parted name 3 Swap
    parted name 4 RootFS
    parted name 5 HomeFS
}


FileSystems() {
    mkfs.vfat -F32 -L Boot "$Device"2
    mkswap -L Swap "$Device"3
    mkfs.ext4 -L RootFS "$Device"4
    mkfs.ext4 -L HomeFS "$Device"5
    swapon "$Device"3
}


Mount() {
    mkdir $MountPoint &>> /dev/null
    mount "$Device"4 $MountPoint
    mkdir $MountPoint/boot $MountPoint/home
    mount "$Device"2 $MountPoint/boot
    mount "$Device"5 $MountPoint/home
}


Unpack() {
    LastStage3="$Mirror/releases/$Arch/autobuilds/latest-stage3-$Arch.txt"
    LastStage3="$(curl -s $LastStage3 | grep stage3-$Arch | cut -f1 --delimiter=' ')"
    LastStage3="$Mirror""/releases/"$Arch"/autobuilds/""$LastStage3"
    wget "$LastStage3" -O $MountPoint/Stage3."${LastStage3#*.}"

    tar xpvf Stage3.* --xattrs-include='*.*' --numeric-owner -C $MountPoint
}


CopyFiles() {
    cp $ScriptPath/etc/portage/* $MountPoint/etc/portage/
    mv $MountPoint/etc/portage/repos.conf/gentoo.conf $MountPoint/etc/portage/repos.conf/gentoo.conf.git
    cp $MountPoint/usr/share/portage/config/repos.conf $MountPoint/etc/portage/repos.conf/gentoo.conf

    mkdir $MountPoint/GentooInstall
    cp $ScriptPath/{GentooInstall.conf,KernelConfig,GentooInstall_ChRoot.sh} $MountPoint/GentooInstall
    echo "" > $MountPoint/GentooInstall/ThisIsTheNewSystem
}


ChRootIn() {
    cp --dereference /etc/resolv.conf $MountPoint/etc/
    mount --types proc /proc $MountPoint/proc
    mount --rbind /sys $MountPoint/sys
    mount --rbind /dev $MountPoint/dev
    chroot $MountPoint /bin/bash
}


ChRootOut() {
    umount -l "$MountPoint"/dev/{shm,pts,mqueue}
    umount -l "$MountPoint"/{dev,proc,sys}
    umount -R "$MountPoint"
}


echo "Making partitions..."; Partitions
echo "Making filesystems..."; FileSystems
echo "Mounting partitions at $MountPoint..."; Mount
echo "Downloading and unpacking last stage3..."; Unpack
echo "Copying auxiliary files..."; CopyFiles
echo "Done."
echo "You will now change root to $MountPoint."
echo "After changing, run 'bash /GentooInstall_ChRoot.sh'"
echo "Changing root to $MountPoint..."; ChRootIn
echo "Unmounting $Device..."; ChRootOut
echo "Done! Now just reboot to your new system!"

