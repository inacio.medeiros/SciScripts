#!/bin/bash

File1=$1
File2=$2
Out=$3

diff -crB "$File1" "$File2" > "$Out"
