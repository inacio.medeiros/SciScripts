#!/bin/bash

## Change default display
Display="$1"

case "$Display" in
	"HdmiE")
		xrandr --output HDMI1 --auto
		xrandr --output HDMI1 --right-of eDP1
		sleep 1
		feh --bg-scale --randomize ~/Nebula/Wallpapers/*
		;;
	"HdmiM")
		xrandr --output HDMI1 --auto
                xrandr --output HDMI1 --same-as eDP1
                sleep 1
                feh --bg-scale --randomize ~/Nebula/Wallpapers/*
                ;;
	"HdmiPres")
		xrandr --output HDMI1 --auto
		xrandr --output HDMI1 --right-of eDP1
        xrandr --output HDMI1 --mode 1024x768
		sleep 1
		feh --bg-scale --randomize ~/Nebula/Wallpapers/*
		;;
	"HdmiOff")
		xrandr --output HDMI1 --off
                sleep 1
                feh --bg-scale --randomize ~/Nebula/Wallpapers/*
                ;;
	*)
		printf "Available displays: \n  HdmiE, HdmiM, HdmiOff."
esac
