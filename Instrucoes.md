# Baixando o projeto

Certifique-se que você possui o Python 3.8 instalado juntamente com as dependências do projeto de Thawann.

Eu fiz uma cópia do projeto dele, e adicionei nessa cópia o script adaptado para ti. Você pode baixá-lo pelo link abaixo:

https://gitlab.com/inacio.medeiros/SciScripts/-/archive/master/SciScripts-master.zip


# Executando o script

O notebook com o script adaptado está no path (dentro do projeto) `Python3/Analysis/GPIAS_Rodolfo.ipynb`. Basta
abrí-lo (no Jupyter ou no JupyterLab), e rodá-lo.


# Entendendo as saídas

Esse notebook gera dois arquivos, ambos localizados no mesmo diretório do notebook:

- ToDelete.svg
- tabela_amplitudes_latencias.csv

O `ToDelete.svg` é a figura dos gráficos conforme a gente viu e discutiu na última reunião. O `tabela_amplitudes_latencias.csv` é a tabela gerada na célula 11 do notebook.


# Utilizando outros arquivos de entrada

Os arquivos e pastas de entrada para este notebook estão no path (dentro do projeto) `Data/`.
Se você tiver outros conjuntos de arquivos/pastas (no mesmo molde, formato de nome etc.) utilizados e quiser usar no notebook, é só ir na célula 7 e substituir onde tem `../../Data` pelo path da pasta onde estão os novos dados.

Minha recomendação é dentro de `Data` você criar uma nova pasta, por exemplo, `NovasAnalises`, colocar os novos dados lá dentro, e aí na na célula 7 você trocar onde tem `../../Data` por `../../Data/NovasAnalises`.

Outra substituição que deve ser feita, para não perder a figura gerada anteriormente, e considerando aqui o nome `NovasAnalises`, é trocar onde tem `ToDelete` por `ToDeleteNovasAnalises`, ou simplesmente por `NovasAnalises` (esse `ToDelete` é o nome dado para a figura gerada, você inclusive por trocar o nome dele antes).

No mais é isso. Qualquer dúvida, podes entrar em contato.