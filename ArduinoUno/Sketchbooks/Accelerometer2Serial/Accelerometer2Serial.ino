/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

#include <Wire.h>
#include <MMA8453_n0m1.h>

MMA8453_n0m1 accel;

void setup()
{
  Serial.begin(115200);
  accel.setI2CAddr(0x1C); //change your device address if necessary, default is 0x1C
  accel.dataMode(true, 2); //enable highRes 10bit, 2g range [2g,4g,8g]
  
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop()
{
  accel.update();
  
  Serial.print("x: "); Serial.print(accel.x());
  Serial.print(" y: "); Serial.print(accel.y());
  Serial.print(" z: "); Serial.println(accel.z());
  
  int X = accel.x()+512;
  int Y = accel.y()+512;
  int Z = accel.z()+512;
  
  X = map(X, 0, 1023, 0, 511);
  Y = map(Y, 0, 1023, 0, 511);
  Z = map(Z, 0, 1023, 0, 511);

  analogWrite(9, X);
  analogWrite(10, Y);
  analogWrite(11, Z);

}
