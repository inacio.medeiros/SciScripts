/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

void setup() {
//  Reference: https://www.arduino.cc/en/Reference/PortManipulation
  DDRD = B11111111;
  PORTD = B00000000;

}

void loop() {
 PORTD = PORTD << 1;
}
