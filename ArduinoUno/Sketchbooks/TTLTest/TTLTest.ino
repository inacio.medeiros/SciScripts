/* 
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
*/

const int TTLs[6] = {13, 12, 8, 7, 4, 2};

void setup() {
  Serial.begin(19200);
  for (int i = 0; i < 6; i++) {
    pinMode(TTLs[i], OUTPUT);
    digitalWrite(TTLs[i], LOW);
  }
}

void loop() {
  
  digitalWrite(TTLs[0], HIGH);
  digitalWrite(TTLs[1], HIGH);
  digitalWrite(TTLs[2], HIGH);
  digitalWrite(TTLs[3], HIGH);
  digitalWrite(TTLs[4], HIGH);
  digitalWrite(TTLs[5], HIGH);
  delay(10);
  digitalWrite(TTLs[0], LOW);
  delay(40);
  digitalWrite(TTLs[1], LOW);
  delay(20);
  digitalWrite(TTLs[2], LOW);
  delay(30);
  digitalWrite(TTLs[3], LOW);
  delay(50);
  digitalWrite(TTLs[4], LOW);
  delay(50);
  digitalWrite(TTLs[5], LOW);
  delay(50);

}
