/*
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-06-06
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

  All pulses (uppercase chars) are 10ms long.
  A = pulse on pin 2
  B = pulse on pin 4
  C = pulse on pin 7
  D = pulse on pin 8
  E = pulse on pin 12
  P = pulse on pin 13
*/

const int inPin = 1;
const int Pins[6] = {2, 4, 7, 8, 12, 13};
const int PinNo = 6;
const int Delay = 20;

void setup() {
  Serial.begin(38400);
  analogReference(INTERNAL);

  pinMode(inPin, INPUT);
  for (int Pin = 0; Pin < PinNo; Pin++) {
    pinMode(Pins[Pin], OUTPUT);
    digitalWrite(Pins[Pin], LOW);
  }

  char ch = 0;
  int inPinV = 0;
}

void loop() {
  char ch = 0;
  int inPinV = 0;

  while (ch == 0) {
    ch = Serial.read();
    inPinV = analogRead(inPin);
    if (inPinV > 10) {
      ch = -1;
    }
  }

  if (ch == 'A') {
    digitalWrite(Pins[0], HIGH);
    delay(Delay);
    digitalWrite(Pins[0], LOW);
  }

  if (ch == 'a') {
    digitalWrite(Pins[0], HIGH);
    while (ch != 'z') {
      ch = Serial.read();
    }
    digitalWrite(Pins[0], LOW);
  }


  if (ch == 'B') {
    digitalWrite(Pins[1], HIGH);
    delay(Delay);
    digitalWrite(Pins[1], LOW);
  }

  if (ch == 'b') {
    digitalWrite(Pins[1], HIGH);
    while (ch != 'y') {
      ch = Serial.read();
    }
    digitalWrite(Pins[1], LOW);
  }

  if (ch == 'C') {
    digitalWrite(Pins[2], HIGH);
    delay(Delay);
    digitalWrite(Pins[2], LOW);
  }

  if (ch == 'c') {
    digitalWrite(Pins[2], HIGH);
    while (ch != 'x') {
      ch = Serial.read();
    }
    digitalWrite(Pins[2], LOW);
  }

  if (ch == 'D') {
    digitalWrite(Pins[3], HIGH);
    delay(Delay);
    digitalWrite(Pins[3], LOW);
  }

  if (ch == 'd') {
    digitalWrite(Pins[3], HIGH);
    while (ch != 'w') {
      ch = Serial.read();
    }
    digitalWrite(Pins[3], LOW);
  }

  if (ch == 'E') {
    digitalWrite(Pins[4], HIGH);
    delay(Delay);
    digitalWrite(Pins[4], LOW);
  }

  if (ch == 'e') {
    digitalWrite(Pins[4], HIGH);
    while (ch != 'v') {
      ch = Serial.read();
    }
    digitalWrite(Pins[4], LOW);
  }

  if (ch == 'P') {
    digitalWrite(Pins[5], HIGH);
    delay(Delay);
    digitalWrite(Pins[5], LOW);
  }
  
  inPinV = map(inPinV, 0, 1023, 0, 255);

  if (inPinV >= 0 && inPinV < 30) {
    digitalWrite(Pins[0], LOW);
    digitalWrite(Pins[1], LOW);
  }

  if (inPinV >= 40 && inPinV < 70) {
    digitalWrite(Pins[0], LOW);
    digitalWrite(Pins[1], HIGH);
  }

  if (inPinV >= 120 && inPinV < 145) {
    digitalWrite(Pins[0], HIGH);
    digitalWrite(Pins[1], LOW);
  }

  if (inPinV >= 180) {
    digitalWrite(Pins[0], HIGH);
    digitalWrite(Pins[1], HIGH);
  }

}
