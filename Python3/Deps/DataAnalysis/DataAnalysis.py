#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@year: 2017
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
import numpy as np
import os

from itertools import tee
import scipy.signal as ssig

from IO import IO


## Level 0
def Convolve(Signal1, Signal2):
    EdgeSize = Signal2.shape[0]//2
    Edge = np.zeros(EdgeSize)
    Signal1 = np.hstack((Edge, Signal1, Edge))

    if 'complex' in str(type(Signal1[0])) or 'complex' in str(type(Signal2[0])):
        DType = 'complex'
    else:
        DType = Signal1.dtype

    Signal = np.zeros(Signal1.shape[0]-1, dtype=DType)
    for S in range(Signal1.shape[0]-Signal2.shape[0]):
        Signal1Index = np.arange(Signal2.shape[0])+S
        Signal[EdgeSize+S] = (Signal2[::-1] * Signal1[Signal1Index]).sum()

    Signal /= (Signal2**2).sum()
    return(Signal)


def CumulativeMA(Base, Add, ElNo):
    if len(Base) == 0:
        Base = Add
    else:
        Base = ((Base * ElNo) + Add)/(ElNo+1)

    return(Base)


def FilterSignal(Signal, Rate, Frequency, FilterOrder=4, Coeff='butter', Type='bandpass'):
    Data = np.zeros(Signal.shape, dtype='float32')
    if len(Signal.shape) == 2:
        for C in range(Signal.shape[1]):
            print('Filtering channel', C+1, '...')
            Data[:,C] = FilterSignal(Signal[:,C], Rate, Frequency, FilterOrder, Coeff, Type)

    else:
        if Coeff == 'butter':
            if Type not in ['bandpass', 'bandstop', 'lowpass', 'highpass']:
                print("Choose 'bandpass', 'bandstop', 'lowpass' or 'highpass'.")

            elif len(Frequency) not in [1, 2]:
                print('Frequency must have 2 elements for bandpass; or 1 element for \
                lowpass or highpass.')

            else:
                passband = [_/(Rate/2) for _ in Frequency]
                f2, f1 = ssig.butter(FilterOrder, passband, Type)
                Data = ssig.filtfilt(f2, f1, Signal, padtype='odd', padlen=0)

        elif Coeff == 'fir':
            Freqs = np.arange(1,(Rate/2)+1)
            DesiredFreqs = np.zeros(int(Rate/2))
            DesiredFreqs[min(Frequency):max(Frequency)] = 1

            o = FilterOrder + ((FilterOrder%2)*-1) +1
            a = ssig.firls(o, Freqs, DesiredFreqs, nyq=Rate/2)
            Data = ssig.filtfilt(a, 1.0, Signal, padtype='odd', padlen=0)

    return(Data)


def FixTTLs(Array, TTLsToFix):
    for TTL in TTLsToFix:
        nInd = np.random.randint(1, 100)
        while nInd == TTL: nInd = np.random.randint(0, 100)
        while nInd >= len(Array): nInd = np.random.randint(0, 100)

        print('TTL', str(TTL), 'was replaced by', str(nInd))
        Array[TTL] = Array[nInd]

    return(Array)


def GenTTLVector(TTLs, TTLLen, FullLen):
    TTLVec = np.zeros([FullLen, 1])

    for TTL in TTLs:
        TTLVec[TTL:TTL+TTLLen] = np.ones([TTLLen, 1])

    return(TTLVec)


def GenFakeTTLsRising(Rate, PulseDur, PauseBefore, PauseAfter, SampleStart, PulseNo):
    BlockDur = PauseBefore+PulseDur+PauseAfter
    # FakeTTLs = [int(((PauseBefore)+(_*BlockDur))*Rate)+SampleStart for _ in range(PulseNo)]
    FakeTTLs = np.arange(SampleStart, SampleStart+(BlockDur*Rate*PulseNo), BlockDur*Rate, dtype='int')
    return(FakeTTLs)


def GetDeltaPhase(SignalRef, SignalResult):
    if len(SignalRef.shape) > 1:
        if SignalRef.shape[1] != SignalResult.shape[1]:
            print('Ref and Result dimensions have to be the same.')
            return(None)

        DeltaPhase = np.zeros(SignalRef.shape)
        for C in range(DeltaPhase.shape[1]):
            DeltaPhase[:,C] = GetDeltaPhase(SignalRef[:,C], SignalResult[:,C])

    else:
        PhaseRef = np.angle(ssig.hilbert(SignalRef))
        PhaseResult = np.angle(ssig.hilbert(SignalResult))
        DeltaPhase = np.angle(np.exp(1j * (PhaseRef - PhaseResult)))

    return(DeltaPhase)


def GetEventEdges(EventsDict, Ch, Proc, Rec, Edge='rise'):
    if Edge.lower() == 'rise': Id = 1
    elif Edge.lower() == 'fall': Id = 0
    else: print('Edge should be "rise" or "fall"')

    Events = EventsDict['sampleNum'][(EventsDict['channel'] == Ch) *
                                     (EventsDict['recordingNumber'] == Rec) *
                                     (EventsDict['nodeId'] == Proc) *
                                     (EventsDict['eventId'] == Id) *
                                     (EventsDict['eventType'] == 3)]

    return(Events)


def GetNegEquiv(Number):
    NumberNeg = Number if Number < 0 else -(100-(100/((Number+100)/100)))
    return(NumberNeg)


def GetPeaks(Signal, Std=1, FixedThreshold=None):
    if len(Signal.shape) == 2:
        Peaks = {'Pos':[], 'Neg':[]}
        for Ch in range(Signal.shape[1]):
            PeaksCh = GetPeaks(Signal[:,Ch], Std, FixedThreshold)
            for K in Peaks.keys(): Peaks[K].append(PeaksCh[K])

    else:
        if FixedThreshold: Threshold = FixedThreshold
        else: Threshold = Std*Signal.std()

        if Threshold:
            ThresholdPos = Signal.mean()+Threshold
            ThresholdNeg = Signal.mean()-Threshold
            Peaks = {
                'Pos': np.where((Signal[1:-1] > ThresholdPos) *
                                (Signal[:-2] < Signal[1:-1]) *
                                (Signal[1:-1] >= Signal[2:]))[0]+1,
                'Neg': np.where((Signal[1:-1] < ThresholdNeg) *
                                (Signal[:-2] > Signal[1:-1]) *
                                (Signal[1:-1] <= Signal[2:]))[0]+1
            }
        else:
            Peaks = {
                'Pos': np.where((Signal[:-2] < Signal[1:-1]) *
                                (Signal[1:-1] >= Signal[2:]))[0]+1,
                'Neg': np.where((Signal[:-2] > Signal[1:-1]) *
                                (Signal[1:-1] <= Signal[2:]))[0]+1
            }

    return(Peaks)


def GetPhases(Data):
    Phases = np.zeros(Data.shape)
    for Trial in range(Data.shape[1]):
        Phases[:, Trial] = np.angle(ssig.hilbert(Data[:,Trial]))

    return(Phases)


def GetPowerOf2(N):
    N = int(2**np.ceil(np.log2(N)))
    return(N)


def GetRecXValues(TTLs, Rate, TimeBeforeTTL, TimeAfterTTL):
    NoOfSamplesBefore = int((TimeBeforeTTL*Rate) * 10**-3)
    NoOfSamplesAfter = int((TimeAfterTTL*Rate) * 10**-3)
    NoOfSamples = NoOfSamplesBefore + NoOfSamplesAfter
    XValues = (range(-NoOfSamplesBefore,
                     NoOfSamples-NoOfSamplesBefore)/Rate) * 10**3

    return(XValues)


def GetStrongestCh(Data):
    BestCh = [np.mean(Data[:,Ch]**2)**0.5 for Ch in range(Data.shape[1])]
    BestCh = BestCh.index(max(BestCh))
    return(BestCh)


def GetTTLInfo(Events, EventRec, TTLCh):
    print('Get TTL data...')
    EventID = Events['TTLs']['user_data']['eventID']
    EventCh = Events['TTLs']['user_data']['event_channels']
    EventSample = Events['TTLs']['time_samples']

    TTLRecs = np.nonzero(np.bincount(EventRec))[0]
    TTLRecs = ["{0:02d}".format(_) for _ in TTLRecs]
    TTLsPerRec = {Rec: [EventSample[_] for _ in range(len(EventRec))
                         if EventRec[_] == int(Rec)
                         and EventCh[_] == TTLCh-1
                         and EventID[_] == 1]
                  for Rec in TTLRecs}

    return(TTLsPerRec)


def GetTTLThreshold(TTLCh, StdNo=3):
    if not StdNo: StdNo = 3

    # if np.mean(TTLCh) > 1000:
    #     print('Sinusoidal stimulation')
    #     Threshold = (max(TTLCh) - min(TTLCh)) / 2
    #     return(Threshold)
    # else:
    #     print('square pulses stimulation')
    Threshold = np.mean(TTLCh) + StdNo*(np.std(TTLCh))
    return(Threshold)


def Morlet(Freq, t, CyclesNo=5, FreqScalingFactor=1, Offset=0):
    SigmaT = CyclesNo/(2*np.pi*Freq)

    Gaussian = np.exp((-((t-Offset)/FreqScalingFactor)**2)/(2*SigmaT**2))
    CosSen = np.exp(1j * 2 * np.pi * Freq/FreqScalingFactor * (t-Offset))
    Psi = FreqScalingFactor * Gaussian * CosSen

    return(Psi)


def NestedClean(Nest):
    if 'numpy' in str(type(Nest)):
        if not Nest.size: return(None)
        else: return(Nest)
    else:
        if len(Nest) == 0: return(None)

    ToDel = []
    if type(Nest) == dict:
        for K, Key in Nest.items():
            Nest[K] = NestedClean(Key)

            if type(Nest[K]) == np.ndarray:
                if not Nest[K].size: ToDel.append(K)
            else:
                if not Nest[K]: ToDel.append(K)

        for K in ToDel: del(Nest[K])
        return(Nest)

    elif type(Nest) in [list, tuple]:
        for E, El in enumerate(Nest):
            Nest[E] = NestedClean(El)

            if type(Nest[E]) == np.ndarray:
                if not Nest[E].size: ToDel.append(E)
            else:
                if not Nest[E]: ToDel.append(E)

        for E in ToDel: del(Nest[E])
        return(Nest)

    else:
        return(Nest)


def Normalize(Data):
    Norm = Data.astype('float32')

    if len(Norm.shape) == 2:
        for Ch in range(Norm.shape[1]):
            Norm[:,Ch] = Normalize(Norm[:,Ch])

    elif len(Norm.shape) == 1:
        Norm /= abs(Norm).max()

    else:
        print('Only 1 or 2 dimensions allowed.')
        return(None)

    return(Norm)


def Pairwise(iterable):
    """ from https://docs.python.org/3.6/library/itertools.html#itertools-recipes
    s -> (s0,s1), (s1,s2), (s2, s3), ..."""

    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def RemapChannels(Tip, Head, Connector):
    """
    Get probe channels order. It doesn't matter what logic you follow to order
    your connector channels, but you MUST follow the same logic for your probe
    head.

    If the probe tip channels are put top-down or bottom-up, the resulting
    channel map will be ordered accordingly.

    Example:
        CustomAdaptor = [5, 6, 7, 8, 9, 10 ,11, 12, 13, 14, 15, 16, 1, 2, 3, 4]
        RHAHeadstage = [16, 15, 14, 13, 12, 11, 10, 9, 1, 2, 3, 4, 5, 6, 7, 8]
        A16OM16 = [13, 12, 14, 11, 15, 10, 16, 9, 5, 4, 6, 3, 7, 2, 8, 1]
        A16 = {'Tip': [9, 8, 10, 7, 13, 4, 12, 5, 15, 2, 16, 1, 14, 3, 11, 6],
               'Head': [8, 7, 6, 5, 4, 3, 2, 1, 9, 10, 11, 12, 13, 14, 15, 16]}

        ChannelMap = RemapChannels(A16['Tip'], A16['Head'], CustomAdaptor)
    """
    print('Get channel order... ', end='')
    ChNo = len(Tip)
    ChMap = [0]*ChNo

    for Ch in range(ChNo):
        TipCh = Tip[Ch] # What channel should be the Ch
        HeadCh = Head.index(TipCh) # Where Ch is in Head
        ChMap[Ch] = Connector[HeadCh] # Channels in depth order

    print('Done.')
    return(ChMap)


def StrRange(Start='a', End='z', Step=1):
    if max(len(Start), len(End)) > 1:
        print('Only 1-char length strings are accepted.')
        return(None)
    else:
        Range = map(chr, range(ord(Start), ord(End), Step))
        return(Range)


def SubSample(Data, Rate, NewRate, t=[]):
    SubSampleF = int(Rate/NewRate)
    if len(Data.shape) > 1:
        Data = Data[np.arange(0,Data.shape[0],SubSampleF), :]
    else:
        Data = Data[np.arange(0,Data.shape[0],SubSampleF)]

    if len(t): t = t[np.arange(0,t.shape[0],SubSampleF)]

    if len(t): return(Data, t)
    else: return(Data)


def UniqueStr(List, KeepOrder=False):
    if KeepOrder:
        used = set()
        UniqueList = [x for x in List if x not in used and (used.add(x) or True)]
    else:
        UniqueList = set(List); UniqueList = sorted(list(UniqueList))

    return(UniqueList)


## Level 1
def Comodulation(Data, Rate, PhaseFreqBand, PhaseFreqBandWidth, AmpFreqBand, AmpFreqBandWidth, FilterOrder=3, File='Comodulogram', Save=False):
    """ Calculate how strongly one frequency band modulates another. """

    Phases = np.arange(-180,161,20)
    PhaseFreq = np.arange(PhaseFreqBand[0], PhaseFreqBand[1], PhaseFreqBandWidth/2)
    AmpFreq = np.arange(AmpFreqBand[0], AmpFreqBand[1], AmpFreqBandWidth/2)

    MeanAmp = np.empty(len(Phases))

    if len(Data.shape) == 2:
        Comodulogram = np.empty((len(AmpFreq), len(PhaseFreq), Data.shape[1]))

        for C in range(Data.shape[1]):
            print('=== [Ch', str(C+1)+'] ===')
            Comodulogram[:,:,C] = Comodulation(Data[:,C], Rate, PhaseFreqBand, PhaseFreqBandWidth, AmpFreqBand, AmpFreqBandWidth, FilterOrder, Save=False)[0]

    else:
        Comodulogram = np.empty((len(AmpFreq), len(PhaseFreq)))

        for PhF,PhaseF in enumerate(PhaseFreq):
            PhaseFiltered = FilterSignal(Data, Rate, [PhaseF, PhaseF+PhaseFreqBandWidth], FilterOrder)
            Phase = np.angle(ssig.hilbert(PhaseFiltered))

            for AF,AmpF in enumerate(AmpFreq):
                if len(Data.shape) == 1: C = 0
                print('[Freq', str(int(PhaseF))+'-'+str(int(PhaseF+PhaseFreqBandWidth))+']', '[Amp', str(int(AmpF))+'-'+str(int(AmpF+AmpFreqBandWidth))+']')
                AmpFiltered = FilterSignal(Data, Rate, [AmpF, AmpF+AmpFreqBandWidth], FilterOrder)
                Amp = abs(ssig.hilbert(AmpFiltered))

                for P,Ph in enumerate(Phases):
                    I = (np.rad2deg(Phase)>Ph)*(np.rad2deg(Phase)<(Ph+20))
                    MeanAmp[P] = np.mean(Amp[I])

                p = MeanAmp/sum(MeanAmp)
                MI = (np.log(len(p))+sum(p[p>0]*np.log(p[p>0])))/np.log(len(p))
                Comodulogram[AF,PhF] = MI

    if Save:
        if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
        IO.Bin.Write(Comodulogram, File+'.dat')
        IO.Bin.Write(AmpFreq, File+'_AmpFreq.dat')
        IO.Bin.Write(PhaseFreq, File+'_PhaseFreq.dat')

    return(Comodulogram, AmpFreq, PhaseFreq)



def CWT(Signal, t, Rate, Freqs, tPsi, Wavelet=Morlet, WaveletArgs=dict(CyclesNo=5, FreqScalingFactor=1, Offset=0)):
    Signal_Psi = np.zeros((Freqs.shape[0], t.shape[0]), dtype='complex')
    if 't' not in WaveletArgs: WaveletArgs['t'] = tPsi

    for F,Freq in enumerate(Freqs):
        WaveletArgs['Freq'] = Freq
        Psi = Wavelet(WaveletArgs)
        Signal_Psi[F,:] = np.convolve(Signal, Psi, 'same')

    return(Signal_Psi)


def GetInstFreq(Signal, Rate):
    if len(Signal.shape) == 2:

        InstFreq = np.zeros((Signal.shape[0]-1, Signal.shape[1]))
        for C in range(Signal.shape[1]):
            InstFreq[:,C] = GetInstFreq(Signal[:,C], Rate)

    else:
        SignalAnalytic = ssig.hilbert(Signal)
        SignalPhase = np.angle(SignalAnalytic)

        # Instantaneous frequency can be achieved by
        # InstFreq = np.angle(np.exp(1j*np.diff(SignalPhase)))/(2*np.pi/Rate)
        # Or by unwrapping the signal
        SignalPhaseUnwrapped = np.unwrap(SignalPhase)
        InstFreq = np.diff(SignalPhaseUnwrapped)/(2*np.pi/Rate)

    return(InstFreq)


def GetPLVs(SignalRef, SignalResult, Rate, Freqs, FreqBand):
    if len(SignalRef.shape) > 1:
        if SignalRef.shape[1] != SignalResult.shape[1]:
            print('Ref and Result dimensions have to be the same.')
            return(None)

        PLVs = np.zeros((Freqs.shape[0], SignalRef.shape[1]), dtype='float32')
        for C in range(PLVs.shape[1]):
            PLVs[:,C] = GetPLVs(SignalRef[:,C], SignalResult[:,C], Rate, Freqs, FreqBand)

    else:
        PLVs = np.zeros((Freqs.shape[0]), dtype='float32')
        for F,Freq in enumerate(Freqs):
            SignalFilteredRef = FilterSignal(SignalRef, Rate, [Freq, Freq+FreqBand])
            SignalFilteredResult = FilterSignal(SignalResult, Rate, [Freq, Freq+FreqBand])
            DP = GetDeltaPhase(SignalFilteredRef, SignalFilteredResult)
            PLVs[F] = np.abs(np.mean(np.exp(1j * DP)))

    return(PLVs)


def PSD(Signal, Rate, Scaling='density', WindowSize=None, NPerSeg=None, NOverlap=None):
    if not WindowSize: WindowSize = len(Signal)
    if not NPerSeg: NPerSeg = WindowSize//2
    if not NOverlap: NOverlap = WindowSize//4

    F, PxxSp = [], []

    if len(Signal.shape) == 2:
        for C in range(Signal.shape[1]):
            print('PSD of channel', C+1, '...')
            f, pxx = PSD(Signal[:,C], Rate, Scaling, WindowSize, NPerSeg, NOverlap)

            if not len(F):
                F = np.zeros((len(pxx), Signal.shape[1]), dtype='float')
                PxxSp = F.copy()

            F[:,C], PxxSp[:,C] = f, pxx

    else:
        NFFT = GetPowerOf2(WindowSize)
        F, PxxSp = ssig.welch(Signal, Rate, nperseg=WindowSize//2,
                                noverlap=WindowSize//4, nfft=NFFT, detrend=False)

    return(F, PxxSp)


def QuantifyTTLsPerRec(AnalogTTLs, Data=[], StdNo=2.5, EventsDict={}, TTLCh=None,
                       Proc=None, Rec=None, Edge='rise'):
    print('Get TTL timestamps... ', end='')
    if AnalogTTLs:
        Threshold = GetTTLThreshold(Data, StdNo); print('TTL threshold:', Threshold)

        if Edge == 'rise':
            TTLs = np.where((Data[:-1] < Threshold)*(Data[1:] > Threshold))[0]
        elif Edge == 'fall':
            TTLs = np.where((Data[:-1] > Threshold)*(Data[1:] < Threshold))[0]
        else:
            print('"Edge" should be "rise" of "fall".')
            return(None)

    else:
        TTLs = GetEventEdges(EventsDict, TTLCh, Proc, Rec, Edge='rise')

    print('Done.')
    return(TTLs)


def RemapCh(Probe, Adaptor):
    Probes = {
        'A16': {'Tip': [9, 8, 10, 7, 13, 4, 12, 5, 15, 2, 16, 1, 14, 3, 11, 6],
                'Head': [8, 7, 6, 5, 4, 3, 2, 1, 9, 10, 11, 12, 13, 14, 15, 16]},
        'CM16': {'Tip': [9, 8, 10, 7, 11, 6, 12, 15, 13, 4, 14, 3, 15, 2, 16, 1],
                 'Head': [12, 11, 10, 9, 8, 7, 6, 5, 16, 15, 14, 13, 4, 3, 2, 1]},
        'Ciralli': {'Tip': [12, 11, 10, 9, 8, 7, 6, 5, 13, 14, 15, 16, 1, 2, 3, 4],
                    'Head': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]}
    }

    Adaptors = {
        'CustomAdaptor': [5, 6, 7, 8, 9, 10 ,11, 12, 13, 14, 15, 16, 1, 2, 3, 4],
        'RHAHeadstage': [16, 15, 14, 13, 12, 11, 10, 9, 1, 2, 3, 4, 5, 6, 7, 8],
        'RHAOM': [12, 11, 10, 9, 8, 7, 6, 5, 13, 14, 15, 16, 1, 2, 3, 4],
        'A16OM16': [13, 12, 14, 11, 15, 10, 16, 9, 5, 4, 6, 3, 7, 2, 8, 1],
        'None16': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    }

    if Probe not in Probes or Adaptor not in Adaptors:
        print('Unknown probe and/or adaptor.')
        print('Known probes:')
        for P in Probes.keys(): print('    ' + P)
        print('Known adaptors:')
        for A in Adaptors.keys(): print('    ' + A)
        return(None)

    Map = RemapChannels(Probes[Probe]['Tip'], Probes[Probe]['Head'], Adaptors[Adaptor])
    return(Map)


def SignalIntensity(Signal, Rate, FreqBand, Ref, NoiseRMS=None, WindowSize=None):
    Intensity = {}; IntensityPSD = {}

    F, PxxSp = PSD(Signal, Rate, WindowSize=WindowSize)
    Range = (F > FreqBand[0])*(F < FreqBand[1])
    BinSize = F[1] - F[0]

    RMS = (sum(PxxSp[Range]) * BinSize)**0.5
    if NoiseRMS: RMS = RMS - NoiseRMS

    dB = 20*(np.log10((RMS/Ref)/2e-5))

    IntensityPSD['F'] = F
    IntensityPSD['PxxSp'] = PxxSp
    Intensity['RMS'] = RMS
    Intensity['dB'] = dB

    return(Intensity, IntensityPSD)


def SliceData(Signal, TTLs, NoOfSamplesBefore, NoOfSamplesAfter,
              AnalogTTLs, RawTime=[]):
    NoOfSamples = NoOfSamplesBefore+NoOfSamplesAfter
    Array = np.zeros((NoOfSamples, len(TTLs)))

    for T, TTL in enumerate(TTLs):
        if AnalogTTLs: TTLLoc = int(TTL)
        else: TTLLoc = int(RawTime.index(TTL))#)/Rate)

        Start = TTLLoc-NoOfSamplesBefore
        End = TTLLoc+NoOfSamplesAfter

        if Start < 0 or End > len(Signal):
            print('TTL too close to the edge. Skipping...')
            continue

        Array[:,T] = Signal[Start:End]

    # print('Done.')
    return(Array)


def Spectrogram(Signal, Rate, WindowSize=None, Overlap=None, NFFT=None, Window='hann'):
    if not WindowSize: WindowSize = len(Signal)//2
    if not Overlap: Overlap = WindowSize//2

    if not NFFT: NFFT = GetPowerOf2(WindowSize)
    F, T, Sxx = ssig.spectrogram(Signal, Rate, axis=0,
                                   nperseg=WindowSize, noverlap=Overlap,
                                   nfft=NFFT, detrend=False, window=Window
    )

    return(F, T, Sxx)

