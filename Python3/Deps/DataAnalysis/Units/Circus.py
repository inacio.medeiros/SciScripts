#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2019-12-11
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
## Imports
import os
import numpy as np

from glob import glob
from imp import load_source

from DataAnalysis.Units import Units
from IO import IO

from circus.shared.parser import CircusParser
from circus.shared.files import load_data


def GetAllClusters(AnalysisPath, Exp='Data_000', OnDisk=False):
    print('Getting parameters...')
    Here = os.getcwd(); os.chdir(AnalysisPath)
    Params = CircusParser(Exp+'.dat'); Params.get_data_file()
    os.chdir(Here)

    SpkWidth = Params.getint('detection', 'N_t')
    SpkWindow = [-(SpkWidth//2)-(SpkWidth%2), SpkWidth//2]
    Results = load_data(Params, 'results')
    RawFiles = sorted(Params.get_data_file().get_file_names())

    PrbFile = AnalysisPath+'/'+Params.get('data', 'mapping')
    Prb = load_source('', PrbFile)
    ChNoTotal = Prb.total_nb_channels
    ChSpacing = Prb.channel_groups['0']['geometry']
    ChSpacing = abs(ChSpacing[0][1] - ChSpacing[1][1])

    Rate = Params.get_data_file().sampling_rate
    Channels = Prb.channel_groups['0']['channels']

    print('Getting info...')
    InfoFile = glob(AnalysisPath+'/*dict')[0]
    DataInfo = IO.Txt.Read(InfoFile)
    Clusters = sorted(Results['spiketimes'].keys(), key=lambda x: int(x.split('_')[-1]))

    DataInfo['Analysis'] = {}
    DataInfo['Analysis']['Channels'] = Channels
    DataInfo['Analysis']['ChNoTotal'] = ChNoTotal
    DataInfo['Analysis']['ChSpacing'] = ChSpacing
    DataInfo['Analysis']['ClusterRecs'] = np.arange(len(RawFiles)).tolist()
    DataInfo['Analysis']['ClusterLabels'] = {Cl: Cluster for Cl, Cluster in enumerate(Clusters)}
    DataInfo['Analysis']['Rate'] = Rate
    DataInfo['Analysis']['RawData'] = RawFiles

    Recs = Units.GetRecsNested(DataInfo)
    ClusterRecs = [b for a in Recs for b in a]

    if ClusterRecs != DataInfo['Analysis']['ClusterRecs']:
        print('Wrong number of recordings! Cannot extract TTLs!')
        DataInfo['Analysis']['RecNoMatch'] = False
    else:
        DataInfo['Analysis']['RecNoMatch'] = True

    DataInfo['Analysis']['RecsNested'] = Recs


    print('Getting rec lengths and offsets...')
    RawLengths = []
    for F,File in enumerate(RawFiles):
        Data = np.memmap(File, 'int16')
        Data = Data.reshape((Data.shape[0]//ChNoTotal, ChNoTotal))
        RawLengths.append(Data.shape[0])
        Data = 0; del(Data)

    RawOffsets = np.concatenate([[0], np.cumsum(RawLengths)[:-1]])

    DataInfo['Analysis']['RecLengths'] = RawLengths
    DataInfo['Analysis']['RecOffsets'] = RawOffsets

    GetSpkRec = np.vectorize(lambda Spk: np.where(RawOffsets <= Spk)[0][-1])
    # from numba import jit
    # FindRecs = jit(nopython=True)(np.vectorize(lambda Spk: np.where(RawOffsets <= Spk)[0][-1]))

    print('Getting spk times and clusters...')
    SpkTimes, SpkClusters = np.array([], 'int16'), np.array([], 'int16')
    for Cl,Cluster in enumerate(Clusters):
        SpkTimes = np.concatenate((SpkTimes, Results['spiketimes'][Cluster]))
        SpkClusters = np.concatenate((SpkClusters, np.array([Cl]*Results['spiketimes'][Cluster].shape[0])))

    AnalysisFile = '/'.join(AnalysisPath.split('/')[:-1]) + '/Units/' + AnalysisPath.split('/')[-2]
    IO.Bin.Write(SpkClusters, AnalysisFile + '_' + Exp + '_AllClusters/SpkClusters.dat')
    SpkClusters = np.memmap(AnalysisFile + '_' + Exp + '_AllClusters/SpkClusters.dat', dtype=SpkClusters.dtype).reshape(SpkClusters.shape)
    IO.Bin.Write(SpkTimes, AnalysisFile + '_' + Exp + '_AllClusters/SpkTimes.dat')
    SpkTimes = np.memmap(AnalysisFile + '_' + Exp + '_AllClusters/SpkTimes.dat', dtype=SpkTimes.dtype, mode='c').reshape(SpkTimes.shape)

    print('Getting spk recs...')
    SpkRecs = GetSpkRec(SpkTimes)
    IO.Bin.Write(SpkRecs, AnalysisFile + '_' + Exp + '_AllClusters/SpkRecs.dat')
    SpkRecs = np.memmap(AnalysisFile + '_' + Exp + '_AllClusters/SpkRecs.dat', dtype=SpkRecs.dtype, mode='c').reshape(SpkRecs.shape)

    print(f'Getting all waveforms {(SpkTimes.shape[0], SpkWidth, len(Channels))}...')
    print(SpkTimes.shape[0] > 1000000)
    if SpkTimes.shape[0] > 1e6:
        Waveforms = [np.zeros((1000000, SpkWidth, len(Channels))) for _ in range(SpkTimes.shape[0]//1000000)]
        if SpkTimes.shape[0]%1000000:
            Waveforms.append(np.zeros((SpkTimes.shape[0]%1000000, SpkWidth, len(Channels))))
    else:
        Waveforms = [np.zeros((SpkTimes.shape[0], SpkWidth, len(Channels)), dtype='int16')]

    if OnDisk:
        IO.Bin.Write(Waveforms, AnalysisFile + '_' + Exp + '_AllClusters/Waveforms')
        Waveforms = [np.memmap(W, dtype=Waveforms[w].dtype).reshape(Waveforms[w].shape) for w,W in enumerate(sorted(glob(AnalysisFile + '_' + Exp + '_AllClusters/Waveforms/*.dat')))]

    for R,Rec in enumerate(RawFiles):
        print(f'    Loading rec {R} of {len(RawFiles)}...')
        Data = np.memmap(Rec, 'int16', mode='c')
        Data = Data.reshape((Data.shape[0]//ChNoTotal, ChNoTotal))

        SpkMask = SpkRecs == R
        SpkIndex = np.where(SpkMask)[0]

        for S,Spk in enumerate(SpkTimes[SpkMask]):
            ListIndex = SpkIndex[S]//1000000
            Window = [Spk+_ for _ in SpkWindow]
            Window = [W-RawOffsets[R] for W in Window]
            if Window[0] < 0: Window = [_-Window[0] for _ in Window]
            Waveforms[ListIndex][SpkIndex[S],:,:] = Data[Window[0]:Window[1],Channels]

        Data = 0; del(Data)
        SpkMask = 0; del(SpkMask)

    if not OnDisk:
        IO.Bin.Write(Waveforms, AnalysisFile + '_' + Exp + '_AllClusters/Waveforms')
        Waveforms = 0; del(Waveforms)

    IO.Txt.Write(DataInfo, AnalysisFile + '_' + Exp + '_AllClusters/Info.dict')
    print('Done. Results written to', AnalysisFile+'.')

    return(None)

