#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 08:35:50 2017

@author: cerebro

Timestamp-based notebook
"""
#%%
from datetime import datetime
from glob import glob

from argparse import ArgumentParser
Parser = ArgumentParser()

Parser.add_argument('-f', help='Name to append to filename', default='Notes')
Args = Parser.parse_args()

Date = datetime.now().strftime("%Y%m%d%H%M%S")
File = Date+'-'+Args.f

Close = False
while not Close:
    if File in glob('*'):
        with open(File, 'r') as F: Lines = F.readlines()
    else:
        Lines = []

    print('\n\n'); print(''.join(Lines)); print('--- EOF ---\n\n')
    Line = input(': ')
    Time = datetime.now().strftime("%H%M%S")

    if Line.lower() == 'exit':
        Line = '\n'
        Close = True
    elif not Line:
        Line = '\n'
    elif Line[0] == '/':
        Line = Line[1:] + '\n'
    else:
        Line = Time + ' ' + Line + '\n'

    Lines.append(Line)
    with open(File, 'w') as F: F.writelines(Lines)
