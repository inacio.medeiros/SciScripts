#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 09:06:55 2018

@author: cerebro
"""
import math
from IO import Txt
from datetime import datetime

def GetDirections():
    Op = ['', '', '']; Axis = ['X', 'Y', 'Z']; Name = ['left', 'posterior', 'ventral']
    for n in range(3):
        while Op[n] not in ['+', '-']:
            Op[n] = input('When going '+Name[n]+', the '+Axis[n]+' coordinate increase(+) or decrease(-) [+/-]? ')
            if Op[n] not in ['+', '-']: print('Accepted answers are "+" or "-".')

    return(Op)

def GetAtlasCoords(Axis=['X', 'Y', 'Z']):
    AtlasCoord = [0]
    while len(AtlasCoord) % len(Axis):
        AtlasCoord = input('Atlas Coordinates in µm [ML1 AP1 DV1 ML2 AP2 DV2 ...]: ')
        AtlasCoord = [int(_) for _ in AtlasCoord.split()]

        if len(AtlasCoord) % len(Axis):
            print('\n'+'There should be an AP, ML and DV for each coordinate. For example,')
            print('to inject at AP = -4.0mm; ML = 2.0mm; and DV = 3.0mm and 1.5mm:')
            print('    2000 -4000 -3000 2000 -4000 -1500')
            # print('Remember that DV is always negative (unless you wanna place something outside the brain :) )\n')

    return(AtlasCoord)


def GetBregmaAndLambdaCoords():
    Bregma = {}; Lambda = {}
    Bregma['X'], Bregma['Y'], Bregma['Z'] = 0, 0, 0
    Lambda['X'], Lambda['Y'], Lambda['Z'] = 1, 1, 1
    Break = False

    while (Bregma['X'] != Lambda['X'] or Bregma['Z'] != Lambda['Z']) and not Break:
        BregmaCoord = input('Bregma coordinate in µm [X Y Z]: ')
        while len(BregmaCoord.split()) != 3:
            print('\n'+'Bregma coordinates should have 3 values.')
            BregmaCoord = input('Bregma coordinate in µm [X Y Z]: ')

        LambdaCoord = input('Lambda coordinate in µm [X Y Z]: ')
        while len(LambdaCoord.split()) != 3:
            print('\n'+'Lambda coordinates should have 3 values.')
            LambdaCoord = input('Lambda coordinate in µm [X Y Z]: ')

        Bregma['X'], Bregma['Y'], Bregma['Z'] = [int(_) for _ in BregmaCoord.split()]
        Lambda['X'], Lambda['Y'], Lambda['Z'] = [int(_) for _ in LambdaCoord.split()]

        Break = True; Misaligned = False
        for Axis in ['X', 'Z']:
            if Bregma[Axis] != Lambda[Axis]:
                Misaligned = True
                print('Head is not aligned in the', Axis, 'axis.')
                print('Lambda', Axis, 'position needs to be corrected by', -(Lambda[Axis]-Bregma[Axis]))

        if Misaligned:
            Ans = input('Do you wish to reinsert the coordinates [Y/n] ? ')
            if Ans.lower() in ['', 'y', 'yes']: Break = False

    return(Bregma, Lambda)


def RemapCoords(Coords, Axis=['X', 'Y', 'Z']):
    Coords = [Coords[n*len(Axis):(n*len(Axis))+len(Axis)]
                  for n in range(0, len(Coords)//len(Axis))]
    Coords = [{Axis[n]: C[n] for n in range(len(C))} for C in Coords]

    return(Coords)


def NormalizeCoords(Bregma, Lambda, Coords, Op, AtlasBregmaLambda=4200):
    BregmaLambda = abs(Lambda['Y'] - Bregma['Y'])
    CoordNorm = BregmaLambda/AtlasBregmaLambda
    Coords = [int(round(_*CoordNorm)) for _ in Coords]
    Coords = RemapCoords(Coords)
    print('\n' + 'Bregma-lambda distance:', BregmaLambda)
    print('\n' + 'Normalized coordinates are:')
    for Coord in Coords: print(Coord)
    print('')

    for C in range(len(Coords)):
        if Bregma['Z'] != Lambda['Z']:
            # print('Z coord of bregma and lambda are different. Adjusting Y and Z...')
            Sig = '-' if Coords[C]['Y'] < 0 else '+'
            ZDiff = abs(Bregma['Z']-Lambda['Z'])
            Hyp = (Coords[C]['Z']**2 + Coords[C]['Y']**2)**0.5
            OrigAng = math.degrees(math.atan(Coords[C]['Z']/Coords[C]['Y']))

            # if Bregma['Z'] > Lambda['Z']: Ang = OrigAng + DiffAng
            # elif Bregma['Z'] < Lambda['Z']: Ang = OrigAng - DiffAng

            # Coords[C]['Y'] = int(round(math.cos(Ang)*Hyp))
            # Coords[C]['Z'] = int(round(math.sin(Ang)*Hyp))

        if Op[0] == '-': Coords[C]['X'] += Bregma['X']
        else: Coords[C]['X'] = -(Coords[C]['X']) + Bregma['X']

        if Op[1] == '-': Coords[C]['Y'] += Bregma['Y']
        else: Coords[C]['Y'] = -(Coords[C]['Y']) + Bregma['Y']

        if Op[2] == '-': Coords[C]['Z'] += Bregma['Z']
        else: Coords[C]['Z'] = -(Coords[C]['Z']) + Bregma['Z']

    print('\n' + 'Stereotaxic coordinates are:')
    for Coord in Coords: print(Coord)
    print('')
    return(Coords)


def AdjustZCoords(Bregma, Coords, Op):
    ZCoords = []
    while len(ZCoords) != len(Coords):

        ZCoords = input('Z position of each coordinate in µm [Z1, Z2, ...]: ')
        ZCoords = [int(_) for _ in ZCoords.split()]

        if len(ZCoords) != len(Coords):
            print('There should be a Z position for each inserted coordinate. For Example,')
            print('if coordinates are "2000 -4000 -3000 2000 -4000 -1500", there should be 2')
            print('Z positions.')

    ZDiff = [Z - Bregma['Z'] for Z in ZCoords]
    print('Z difference:', ZDiff)
    if Op[2] == '+':
        for C in range(len(Coords)): Coords[C]['Z'] += ZDiff[C]
    else:
        for C in range(len(Coords)): Coords[C]['Z'] -= ZDiff[C]

    return(Coords, ZDiff)


if __name__ == "__main__":
    Op = GetDirections()
    AtlasCoords = GetAtlasCoords()
    Bregma, Lambda = GetBregmaAndLambdaCoords()
    Coords = NormalizeCoords(Bregma, Lambda, AtlasCoords, Op)

    Coords, ZDiff = AdjustZCoords(Bregma, Coords, Op)
    print('\n' + 'Final coordinates:')
    for Coord in Coords: print(Coord)
    print('')
    File = datetime.now().strftime("%Y%m%d%H%M%S") + '-Coordinates'
    Txt.Write({'AtlasCoords': AtlasCoords, 'Bregma': Bregma, 'Lambda': Lambda, 'Coords': Coords, 'ZDiff': ZDiff}, File)

