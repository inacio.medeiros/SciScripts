#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 08:35:50 2017

@author: cerebro

Final volume will be animal's weight x VolAmp
For example, suppose a 20g animal
- if VolAmp is 10, the injection volume will be 200µl
- if VolAmp is 1, the injection volumewill be 20µl
"""
#%%
from argparse import ArgumentParser
Parser = ArgumentParser()

Parser.add_argument('--dose-mgkg', help='Drug dose in mg/kg')
Parser.add_argument('--stock-mgml', help='Drug stock concentration in mg/ml')
Parser.add_argument('--stock-ml', help='Final solution volume in ml')
Parser.add_argument('--vol-amp', help='Volume amplification (dose to be applied will be "animal weight x VolAmp"', default='10')

Args = Parser.parse_args()

Dose_MgKg = float(Args.dose_mgkg)
Stock_MgMl = float(Args.stock_mgml)
Stock_Ml = float(Args.stock_ml)
VolAmp = float(Args.vol_amp)

Dose_1g_g = Dose_MgKg/1000
Dose_1g_ml = Dose_1g_g/Stock_MgMl

Dose_Final = round((Dose_1g_ml*1000*1000*Stock_Ml)/VolAmp,3)

Solution = {'Drug'+str(Stock_Ml)+'ml': Dose_Final,
            'Vehicle'+str(Stock_Ml)+'ml': (Stock_Ml*1000)-Dose_Final}
print(Solution)
print('')
