#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2019-12-09
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

Quick 'n' dirty script to play sound stimulation without any calibration.
"""

#%%
from IO import SoundCard, SigGen
import numpy as np

## Set soundcard
Rate, BlockSize, Channels = 48000, 512, 1
SoundCard.AudioSet(Rate, BlockSize, Channels)


## Set stimulus
Freq, Dur = [5000, 15000], 0.01
Pulse = SigGen.Noise(Rate, Dur)
Pulse = np.concatenate([Pulse, np.zeros(int(0.5*Rate)), Pulse, np.zeros(int(4*Rate))])

## Play
for i in range(50):  print(i+1); SoundCard.Write(Pulse, [1,2])

# for i in range(50):
#     print('Pair',i+1)
#     Pi = np.concatenate([Pulse, np.zeros(int(np.random.randint(2,8)*Rate))])
#     SoundCard.Write(Pi, [1,2])
