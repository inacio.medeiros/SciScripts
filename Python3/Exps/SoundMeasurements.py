#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@dat: 2018-09-03
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
#%%
from Exps import SoundMeasurements
from DataAnalysis import SoundMeasurements as Analysis
from DataAnalysis.Plot import SoundMeasurements as Plot

Parameters = dict(
    ## Calibration
    Freq = 10000,
    WaveDur = 10,
    Repetitions = 4,


    ## Measurements
    Setup = 'GPIAS',
    SoundPulseDur = 2,
    NoiseFrequency = [[8000, 10000], [9000, 11000], [10000, 12000], [12000, 14000],
                      [14000, 16000], [16000, 18000], [8000, 18000]],

    # Just if testing, otherwise comment out
    # NoiseFrequency = [[8000, 10000], [14000, 16000]], # Override
    # SoundAmpF = [1.7, 0.0001, 0.0],

    # Mic sensitivity, from mic datasheet, in dB re V/Pa or in V/Pa
    MicSens_dB = -47.46,


    ## General
    Rate = 192000,
    #BlockSize = 384,
    System = 'Jack-IntelOut-Marantz-IntelIn',
    # System = 'Jack-TestOut-TestIn',
)

# SoundMeasurements.RunCalibration(**Parameters)
SoundMeasurements.RunMeasurement(**Parameters)
Analysis.Run(**Parameters)
Plot.All(Parameters['System'], Parameters['Setup'])
