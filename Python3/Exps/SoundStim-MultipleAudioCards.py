#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2017-08-18
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts

Script made to answer
https://github.com/spatialaudio/python-sounddevice/issues/98
"""
import numpy as np
import sounddevice as SD

from threading import Thread

## Set your things
Rate = 192000       # in Hz
BlockSize = 384     # in samples
ChsPerDevice = 1    # in number
Duration = 3        # in seconds
DeviceIndex = [0, 1, 2]


## Apply settings
SD.default.samplerate = Rate
SD.default.blocksize = BlockSize
SD.default.channels = ChsPerDevice


## Create streams
Streams = [SD.OutputStream(dtype='float32', device=Index) for Index in DeviceIndex]


## Define thread class
class Play(Thread):
    def __init__(self, Stream, Sound):
       Thread.__init__(self)
       self.Stream = Stream
       self.Sound = Sound

    def run(self):
        self.Stream.write(self.Sound)


## Generate your sound arrays
BG = np.random.uniform(-0.3, 0.3, size=(Rate*Duration, ChsPerDevice))
Pulse = np.random.uniform(-1, 1, size=(Rate*Duration, ChsPerDevice))
Gap = np.zeros((Rate*Duration, ChsPerDevice))

Stim = [BG, Pulse, Gap] # Order according to what you want each card to output


## Create threads
Threads = [Play(Stream, Stim[S]) for S, Stream in enumerate(Streams)]


## Play
for S in Streams: S.start()
for T in Threads: T.start()

# Do whatever other thing while stimuli are played
# and whenever you want to wait for them all to finish, just:
for T in Threads: T.join()
for S in Streams: S.stop()

