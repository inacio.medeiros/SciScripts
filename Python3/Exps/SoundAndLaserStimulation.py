#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@year: 2015
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
#%% Sound and Laser stimulation
from Exps import SoundAndLaserStimulation


Parameters = dict(
    AnimalName          = 'Control_09', # sempre um string
    StimType            = ['Sound'],    # 'Sound', 'Laser' or 'SoundLaser' - Checkbox - Não precisa

    ## === Sound === ##
    SoundType           = 'Noise', # Comment all possibilities
    Intensities         = [80, 75, 70, 65, 60, 55, 50, 45], # lista de intensidades
    NoiseFrequency      = [   # Cada sublista é um intervalo de frequencias
        [8000, 10000],
        [9000, 11000],
        [10000, 12000],
        [12000, 14000],
        [14000, 16000]
    ],

    # Fill all durations in SECONDS!
    SoundPulseNo                = 25**2,
    SoundPauseBeforePulseDur    = 0.004,
    SoundPulseDur               = 0.003,
    SoundPauseAfterPulseDur     = 0.093,
    PauseBetweenIntensities     = 10,

    # Intensities         = [80]*5,
    # NoiseFrequency      = [[5000, 15000]],
    # SoundPulseNo                = 200,


    ## === Laser === ##
    # # 'Sq' for square pulses, 'Sin' for sin wave
    # LaserType                       = 'Sq',

    # # if LaserType == 'Sq'
    # LaserPauseBeforePulseDur        = 0,
    # LaserPulseDur                   = 0.01,
    # LaserPauseAfterPulseDur         = 0.09,
    # LaserPulseNo                    = 200,

    # # if LaserType == 'Sin'
    # LaserDur                        = 0.1*529,
    # LaserFreq                       = 10,      # in Hz

    # LaserStimBlockNo                = 5,
    # LaserPauseBetweenStimBlocksDur  = 10,


    ## === Probe === ##
    # Remapped    = True,
    # Probe       = 'A16',        # None | 'A16'
    # ProbeNo     = '8B95',
    # Adaptor     = 'A16OM16',    # None | 'CustomAdaptor' | 'RHAHeadstage' | 'A16OM16'
    # ChSpacing   = 50,


    ## === Hardware === ##
    TTLAmpF         = 1.7,
    RecCh           = [1],
    StimCh          = 2,
    TTLCh           = 3,
    # RecCh           = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
    # StimCh          = 17,
    # TTLCh           = 18,
    # AnalogTTLs      = True,

    # System = 'Jack-TestOut-TestIn',
    System          = 'Jack-IntelOut-Marantz-IntelIn',
    Setup           = 'UnitRec',
)

Stimulation, InfoFile = SoundAndLaserStimulation.Prepare(**Parameters)


#%% Run
SoundAndLaserStimulation.Play(Stimulation, InfoFile, ['Sound'], DV='Out')

