# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@year: 2015
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
#%%
from time import time
from IO import Arduino

PulseNo = 5000

ArduinoObj = Arduino.CreateObj(115200)

for Pulse in range(PulseNo):
    ArduinoObj.write(b'A')
    time.sleep(0.1)
