#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 11:27:52 2018

@author: malfatti
"""

import os
# from glob import glob
from argparse import ArgumentParser
Parser = ArgumentParser()

Parser.add_argument('--files', nargs='*', help='Files to rename')
Args = Parser.parse_args()

# Photos = glob('????-??-?? ??.??.??.jpg')
Photos = Args.files
for Photo in Photos:
    Path = '/'.join(Photo.split('/')[:-1])
    File = Photo.split('/')[-1]
    
    if Photo[-3:].lower() == 'jpg':
        New = Path+ '/IMG-' + File.replace('-','').replace(' ','-').replace('.','')[:-3]+'.jpg' 
    elif Photo[-3:].lower() == 'mp4':
        File = ''.join(File.split('-')[1:4])+'-'+''.join(File.split('-')[4:7]) + '-' + ''.join(File.split('-')[7:])
        if File[-1] == '-': File = File[:-1]
        New = Path + '/VID-' + File
    else:
        continue
    
    os.rename(Photo, New)


# New = ['/'.join(Photo.split('/')[:-1]) + '/IMG-'+Photo.split('/')[-1].replace('-','').replace(' ','-').replace('.','')[:-3]+'.jpg' 
#        for Photo in Photos]

# print('The files')
# print(Photos)
# print('will be renamed to')
# print(New)
# print('')
# Ans = input('Proceed [y/N]? ')

# if Ans in ['y', 'yes']:
#     for F in range(len(Photos)): os.rename(Photos[F], New[F])
# else:
#     print('Aborted.')
    
