import sys
from PyQt5.QtWidgets import QApplication
from PyQt5 import QtSvg

App = QApplication(sys.argv) 
Widget = QtSvg.QSvgWidget(sys.argv[1])
Widget.show()

sys.exit(App.exec_())
