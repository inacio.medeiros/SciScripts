#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20200214
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser

#%%
Parser = ArgumentParser()
Parser.add_argument('--files', nargs='*', help='Files to process')
Parser.add_argument('--min', nargs='?', type=float, default=5, help='Minimum percentage')
Parser.add_argument('--max', nargs='?', type=float, default=85, help='Maximum percentage')
Args = Parser.parse_args()

Files = Args.files

for File in Files:
    IMG = plt.imread(File)

    IMG2 = IMG/IMG.max()
    Min = np.percentile(IMG2.ravel(), 5)
    Max = np.percentile(IMG2.ravel(), 85)
    IMG2[IMG2<Min] = Min
    IMG2[IMG2>Max] = Max
    IMG2 -= Min
    IMG2 /= IMG2.max()

    plt.imsave(File, IMG2)
    #IMG2 *= 255

# Fig, Axes = plt.subplots(1,2,figsize=(15,10))
# Axes[0].imshow(np.flipud(IMG))
# Axes[1].imshow(np.flipud(IMG2))
# plt.show()
