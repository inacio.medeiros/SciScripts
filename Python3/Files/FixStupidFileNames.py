#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 11:27:52 2018

@author: malfatti
"""

import os
import unicodedata
from argparse import ArgumentParser

Parser = ArgumentParser()
Parser.add_argument('--files', nargs='*', help='Files to rename')
Args = Parser.parse_args()

Files = Args.files
NewFiles = []
for File in Files:
    if '/' in File: Path = '/'.join(File.split('/')[:-1])
    else: Path = '.'

    New = File.split('/')[-1]

    for C in ['"',"'",'!','@','#','$','%','&','*','+','=',';',':','?',',']:
        if C in New: New = New.replace(C,'')

    for C in ['(', ')', '[', ']', '{', '}', '<', '>', '|']:
        if C in New: New = New.replace(C,'_')

    if ' ' in New:
        New = New.title()
        New = New.replace(' ', '')

    New = ''.join((c for c in unicodedata.normalize('NFD', New) if unicodedata.category(c) != 'Mn'))

    if New[0] == New[0].lower():
        New = list(New)
        New[0] = New[0].upper()
        New = ''.join(New)

    if '.' in New:
        New = '.'.join(New.split('.')[:-1]) + '.' + New.split('.')[-1].lower()

    New = Path+'/'+New
    NewFiles.append(New)

print('Files will be renamed as:')
for F,File in enumerate(Files):
    print('    '+File, '|', NewFiles[F])


#for F,File in enumerate(Files): os.rename(File, NewFiles[F])
Ans = input('Proceed? [y/N] ')
if Ans.lower() in ['y', 'yes']:
    for F,File in enumerate(Files):
        if File != NewFiles[F]: os.rename(File, NewFiles[F])
else:
    print('Aborted.')


