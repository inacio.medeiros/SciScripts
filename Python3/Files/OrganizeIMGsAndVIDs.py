#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 12:43:27 2018

@author: malfatti
"""

import os
from glob import glob

Files = glob('IMG*.jpg') + glob('VID*.mp4')
# print(Files)

for File in Files:
    Sep = File[3]
    Date = File.split(Sep)[1]
    os.makedirs(Date, exist_ok=True)
    os.rename(File, Date+'/'+File)
    #print(File, 'goes to', Date+'/'+File)

