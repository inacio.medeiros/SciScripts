#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20180911
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
#%% Imports
import neo
import numpy as np
import os
import quantities as Qt
# from glob import glob

from DataAnalysis.DataAnalysis import QuantifyTTLsPerRec
from DataAnalysis.Plot import Plot


#%% Plot functions
def AllSegments(Data, Time,
                Ax=None, AxArgs={}, File='AllSegments', Ext=['svg'], Save=False, Show=True):
    ReturnAx = True
    if not Ax:
        ReturnAx = False
        Plt = Plot.Return('Plt')
        Fig, Ax = Plt.subplots(figsize=(5,3))

    Colormap = Plot.GetColors('colormaps')[-1]
    # Colormap = Colormap[0]

    for S,Seg in enumerate(Data):
        Color = Colormap(255-((len(Data)-S)*10))
        Ax = Plot.General(Time, Seg.magnitude, Color, Ax=Ax, AxArgs=AxArgs)

    if ReturnAx:
        Plot.Set(Ax=Ax, AxArgs=AxArgs)
        return(Ax)
    else:
        Plot.Set(Ax=Ax, Fig=Fig, AxArgs=AxArgs)
        if Save:
            if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
            for E in Ext: Fig.savefig(File+'.'+E, format=E, dpi=300)#, bbox_extra_artists=(Ax.get_legend(),), bbox_inches='tight')

        if Show: Plt.show()
        else: Plt.close()
        return(None)


def CurrentVoltageAllSegs(Current, Voltage, t=[],
                          AxArgs={}, File='CurrentVoltageAllSegs', Ext=['svg'], Save=False, Show=True):
    if not len(t):
        dt = Voltage[0].sampling_period.magnitude
        t = np.arange(0, Voltage[0].size, dt)

    Plt = Plot.Return('Plt')
    Fig, Axes = Plt.subplots(2, 1, figsize=(5,3), sharex=True)
    # Fig = Plt.figure(figsize=(5,3))
    # Axes = [
    #         Plt.subplot2grid((3, 1), (0, 0), rowspan=2),
    #         Plt.subplot2grid((3, 1), (2, 0))
    # ]
    AUnit = Current[0].units.dimensionality.string
    VUnit = Voltage[0].units.dimensionality.string
    ALabel = 'Current ['+AUnit+']'
    VLabel = 'Voltage ['+VUnit+']'

    AxArgs['ylabel'] = ALabel
    Axes[0] = AllSegments(Current, t, Ax=Axes[0], AxArgs=AxArgs)
    AxArgs['xlabel'] = 'Time [s]'
    AxArgs['ylabel'] = VLabel
    Axes[1] = AllSegments(Voltage, t, Ax=Axes[1], AxArgs=AxArgs)

    Plot.Set(Fig=Fig)
    if Save:
        if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
        for E in Ext: Fig.savefig(File+'.'+E, format=E, dpi=300)#, bbox_extra_artists=(Ax.get_legend(),), bbox_inches='tight')

    if Show: Plt.show()
    else: Plt.close()
    return(None)


def VoltagesCurrents(Voltages, Currents, Labels=[],
            Ax=None, AxArgs={}, File='VoltagesPerCurrents', Ext=['svg'], Save=False, Show=True):
    ReturnAx = True
    if not Ax:
        ReturnAx = False
        Plt = Plot.Return('Plt')
        Fig, Ax = Plt.subplots(figsize=(5,3))

    if not len(Labels): Labels = [None for _ in Voltages]

    for l,L in enumerate(Labels):
        Ax.plot(Voltages[l], Currents[l], label=L, marker='.')
    Ax.legend(loc='upper left')

    if ReturnAx:
        Plot.Set(Ax=Ax, AxArgs=AxArgs)
        return(Ax)
    else:
        Plot.Set(Ax=Ax, Fig=Fig, AxArgs=AxArgs)
        Ax.spines['bottom'].set_bounds(Ax.get_xticks()[0], Ax.get_xticks()[-1])
        if Save:
            if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
            for E in Ext: Fig.savefig(File+'.'+E, format=E, dpi=300)#, bbox_extra_artists=(Ax.get_legend(),), bbox_inches='tight')

        if Show: Plt.show()
        else: Plt.close()
        return(None)


def IRev(Voltages, Currents, IRevs, Labels, Title,
         File='IRev', Ext=['svg'], Save=False, Show=True):
    Plt = Plot.Return('Plt')
    Fig, Ax = Plt.subplots(figsize=(5,3))
    AxArgs = {'title': Title, 'xlabel': 'Voltage [mV]', 'ylabel': 'Current [µA]'}
    Ax = VoltagesCurrents(Voltages, Currents, Labels, Ax=Ax, AxArgs=AxArgs)

    Ax.plot([np.min(Vs), np.max(Vs)], [0,0], 'k--')
    for r,R in enumerate(IRevs):
        Ri = R#+np.random.uniform(-0.5,0.5)
        Ax.plot(Ri, [0], 'k', marker='v', zorder=1)
        Ax.plot([Ri]*2, [np.min(Is), 0], Ax.lines[r].get_color(), linestyle='--', zorder=1)
    Ax.legend()

    Plot.Set(Fig=Fig)
    if Save:
        if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
        for E in Ext: Fig.savefig(File+'.'+E, format=E, dpi=300)#, bbox_extra_artists=(Ax.get_legend(),), bbox_inches='tight')

    if Show: Plt.show()
    else: Plt.close()
    return(None)


def TxtData(DataA, DataB,
            File='TxtData', Ext=['svg'], Save=False, Show=True):
    Plt = Plot.Return('Plt')
    Fig, Ax = Plt.subplots(1,2,figsize=(5,3), sharey=True)

    Cc = ['0mM Ca', '0.5mM Ca', '1mM Ca']
    for c,C in enumerate(Cc):
        Ax[0].plot(DataA[c][:,0], DataA[c][:,1]/DataA[c][:,1].max(), label=C)
        Ax[1].plot(DataB[c][:,0], DataB[c][:,1]/DataB[c][:,1].max(), label=C)

    Ax[0].legend()
    Ax[1].legend()

    XLabel = 'Applied voltage [V]'
    YLabel = '$I_{tail}/I_{max}$ [norm.]'
    AxArgs = {'title': 'WT', 'xlabel': XLabel, 'ylabel': YLabel}
    Plot.Set(Ax=Ax[0], AxArgs=AxArgs)

    AxArgs = {'title': 'Mutant', 'xlabel': XLabel}
    Plot.Set(Ax=Ax[1], AxArgs=AxArgs)

    Plot.Set(Fig=Fig)
    if Save:
        if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
        for E in Ext: Fig.savefig(File+'.'+E, format=E, dpi=300)#, bbox_extra_artists=(Ax.get_legend(),), bbox_inches='tight')

    if Show: Plt.show()
    else: Plt.close()
    return(None)


def AllCaCurrents(Voltage, Currents, t=[], Labels=None,
                  Axes=[], AxArgs={}, File='AllCaCurrents', Ext=['svg'], Save=False, Show=True):
    SignalNo = len(Currents)+1
    if not len(t):
        dt = Voltage[0].sampling_period.magnitude
        t = np.arange(0, Voltage[0].size, dt)

    if not len(Labels): Labels = [None for _ in Currents]

    ReturnAx = True
    if not len(Axes):
        ReturnAx = False
        Plt = Plot.Return('Plt')
        Fig, Axes = Plt.subplots(SignalNo, 1, figsize=(4, 10), dpi=50)

    # YLim = [0, 0]
    for C, Current in enumerate(Currents):
        AUnit = Current[0].units.dimensionality.string
        VUnit = Voltage[0].units.dimensionality.string
        ALabel = 'Current ['+AUnit+']'
        VLabel = 'Voltage ['+VUnit+']'

        AxArgs['ylabel'] = ALabel
        AxArgs['xlabel'] = ''
        # AxArgs['title'] = Labels[C]
        
        ## Override
        Axes[C].set_ylim([-15,15])
        for c,cc in enumerate(Current):
            Current[c][cc<-15] = -15*Qt.uA
            Current[c][cc>15] = 15*Qt.uA
        
        Axes[C] = AllSegments(Current[:-1], t, Ax=Axes[C], AxArgs=AxArgs)
        Axes[C].text(0, -14, Labels[C])

        # y = Axes[C].get_ylim()
        # if y[0] < YLim[0]: YLim[0] = y[0]
        # if y[1] > YLim[1]: YLim[1] = y[1]

    # for C in range(len(Currents)):
    #     YBound = [Axes[C].get_yticks()[Axes[C].get_yticks() >= YLim[0]][0],
    #               Axes[C].get_yticks()[Axes[C].get_yticks() <= YLim[1]][-1]]
    #     Axes[C].spines['bottom'].set_bounds(YBound[0], YBound[1])
    #     Axes[C].set_ylim(YLim)
    #     # Axes[C].set_ylim(YLim)

    # AxArgs['title'] = 'Voltage protocol'
    AxArgs['xlabel'] = 'Time [s]'
    AxArgs['ylabel'] = VLabel
    Axes[-1].set_ylim([-100,100])
    Axes[-1] = AllSegments(Voltage[:-1], t, Ax=Axes[-1], AxArgs=AxArgs)
    Axes[C].text(0, -80, 'Voltage protocol')
    
    ## Override
    # for Ax in Axes: Ax.set_ylim([-15,15])
    # Axes[-1].set_ylim([-100,100])
    
    if ReturnAx:
        # for Ax in Axes: Plot.Set(Ax=Ax, AxArgs=AxArgs)
        return(Axes)
    else:
        # for Ax in Axes: Plot.Set(Ax=Ax, AxArgs=AxArgs)
        Plot.Set(Fig=Fig)
        if Save:
            if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
            for E in Ext: Fig.savefig(File+'.'+E, format=E, dpi=300)
    
        if Show: Plt.show()
        else: Plt.close()
        return(None)


# def RawTraces(Block,
#               File='RawTraces', Ext=['svg'], Save=False, Show=True, ReturnData=False):
#     SignalNo = len(Block.segments[0].analogsignals)
#     Plt = Plot.Return('Plt')
#     Fig, Axes = Plt.subplots(((SignalNo+2)+1)//2, 2,
#                              figsize=(7.086614, 4), dpi=100)

#     for Si in range(SignalNo):
#         for Se, Seg in enumerate(Block.segments):
#             Signal = Seg.analogsignals[Si]
#             t = Signal.times.rescale('s').magnitude
#             Axes[Si,0].plot(t, Signal.magnitude, 'k')

#         Unit = Signal.units.dimensionality.string
#         if Unit == 'uA': YLabel = 'Current [uA]'
#         elif Unit == 'mV': YLabel = 'Voltage [mV]'
#         else: print(Unit); YLabel = None

#         AxArgs = {'ylabel': YLabel, 'xtickspacing': 50}#,'xlim': [-20,270]}
#         if Si == SignalNo-1: AxArgs['xlabel']= 'Time [s]'
#         Plot.Set(Ax=Axes[Si,0], AxArgs=AxArgs)

#     t = Axes[0,0].lines[0].get_xdata()
#     X = Axes[1,0].lines[0].get_ydata()
#     XLabel = Axes[1,0].get_ylabel()
#     Y = Axes[0,0].lines[0].get_ydata()
#     YLabel = Axes[0,0].get_ylabel()
#     if YLabel[0] == 'V':
#         X, Y, XLabel, YLabel = Y, X, YLabel, XLabel

#     Sort = np.argsort(X, axis=0)
#     G = ((Y*1e-6)/(X*1e-3))*1e3

#     Axes[0,1].plot(X[Sort], Y[Sort], 'k.--')
#     AxArgs = {'ylabel': YLabel}
#     Plot.Set(Ax=Axes[0,1], AxArgs=AxArgs)

#     Axes[1,1].plot(X[Sort], G[Sort], 'k.--')
#     AxArgs = {'xlabel': XLabel, 'ylabel': 'Conductance [mS]'}
#     Plot.Set(Ax=Axes[1,1], AxArgs=AxArgs)

#     Plot.Set(Fig=Fig)
#     Fig.subplots_adjust(wspace=0.3, hspace=0.3)

#     if Save:
#         if '/' in File: os.makedirs('/'.join(File.split('/')[:-1]), exist_ok=True)
#         for E in Ext: Fig.savefig(File+'.'+E, format=E, dpi=300)

#     if Show: Plt.show()
#     else: Plt.close()

#     if ReturnData: return(t,X,Y,XLabel,YLabel)
#     else: return(None)



#%% Recs info
DataPath = os.environ['HOME']+'/Barbara/ChileData/Data'
Recs = {
    'ABFFiles': [
        # DataPath+'/2018_09_03_0000.abf',
        DataPath+'/2018_09_03_0001.abf',
        DataPath+'/2018_09_03_0002.abf',
        DataPath+'/2018_09_03_0003.abf',
        # DataPath+'/2018_09_05_0000.abf',
        # DataPath+'/2018_09_05_0001.abf',
        DataPath+'/2018_09_05_0002.abf',
        DataPath+'/2018_09_05_0003.abf',
        # DataPath+'/2018_09_05_0004.abf',
        DataPath+'/2018_09_05_0005.abf',
        DataPath+'/2018_09_05_0006.abf',
        DataPath+'/2018_09_05_0007.abf',
        # DataPath+'/2018_09_05_0008.abf',
        DataPath+'/2018_09_05_0009.abf',
        # DataPath+'/2018_09_05_0010.abf',
        DataPath+'/2018_09_06_0000.abf',
        DataPath+'/2018_09_06_0001.abf',
        DataPath+'/2018_09_06_0002.abf',
        DataPath+'/2018_09_06_0003.abf',
        DataPath+'/2018_09_06_0008.abf',
        DataPath+'/2018_09_06_0009.abf',
        DataPath+'/2018_09_06_0010.abf',
        DataPath+'/2018_09_06_0011.abf',
        DataPath+'/2018_09_06_0012.abf',
        DataPath+'/2018_09_06_0013.abf',
        DataPath+'/2018_09_06_0014.abf',
        DataPath+'/2018_09_06_0015.abf',
        DataPath+'/2018_09_06_0016.abf',
        DataPath+'/2018_09_06_0017.abf',
        DataPath+'/2018_09_06_0018.abf',
        DataPath+'/2018_09_06_0019.abf',
        DataPath+'/2018_09_06_0020.abf',
        DataPath+'/2018_09_06_0021.abf',
        DataPath+'/2018_09_06_0022.abf',
        DataPath+'/2018_09_06_0023.abf',
        DataPath+'/2018_09_07_0000.abf',
        DataPath+'/2018_09_07_0005.abf',
        DataPath+'/2018_09_07_0006.abf',
        DataPath+'/2018_09_07_0007.abf',
        DataPath+'/2018_09_07_0008.abf',
        DataPath+'/2018_09_07_0009.abf',
        DataPath+'/2018_09_07_0010.abf',
        DataPath+'/2018_09_07_0011.abf',
        DataPath+'/2018_09_11_0000.abf',
        DataPath+'/2018_09_11_0001.abf',
        DataPath+'/2018_09_11_0002.abf',
        DataPath+'/2018_09_11_0003.abf',
        DataPath+'/2018_09_11_0004.abf',
        # DataPath+'/2018_09_11_0005.abf',
        DataPath+'/2018_09_11_0006.abf',
        DataPath+'/2018_09_11_0007.abf',
        DataPath+'/2018_09_11_0008.abf',
        DataPath+'/2018_09_11_0009.abf',
        DataPath+'/2018_09_11_0010.abf',
        DataPath+'/2018_09_11_0011.abf',
        DataPath+'/2018_09_11_0012.abf',
        DataPath+'/2018_09_11_0013.abf',
        DataPath+'/2018_09_11_0014.abf',
        DataPath+'/2018_09_11_0015.abf',
        DataPath+'/2018_09_11_0016.abf',
        DataPath+'/2018_09_11_0017.abf'
    ],

    'Treatment': [
        # '',
        '',
        '',
        '',
        # '',
        # '',
        '1.0mMCa',
        '1.0mMCa',
        # '0.5mMCa',
        '0.5mMCa',
        '0.5mMCa',
        '',
        # '',
        '',
        # '',
        '',
        '',
        '',
        '',
        '0.0mMCa',
        '0.5mMCa',
        '0.5mMCa',
        '1.0mMCa',
        '0.0mMCa',
        '0.0mMCa',
        '0.5mMCa',
        '0.5mMCa',
        '1.0mMCa',
        '1.0mMCa',
        '0.0mMCa',
        '0.0mMCa',
        '0.5mMCa',
        '0.5mMCa',
        '1.0mMCa',
        '1.0mMCa',
        '',
        '120mMKCl',
        '120mMKCl',
        '040mMKCl',
        '012mMKCl',
        '120mMKCl',
        '040mMKCl',
        '012mMKCl',
        '0.0mMCa',
        '0.5mMCa',
        '0.5mMCa',
        '1.0mMCa',
        '1.0mMCa',
        # '1.0mMCa',
        '0.0mMCa',
        '0.0mMCa',
        '0.0mMCa',
        '0.0mMCa',
        '0.5mMCa',
        '1.0mMCa',
        '0.0mMCa',
        '0.0mMCa',
        '0.5mMCa',
        '0.5mMCa',
        '1.0mMCa',
        '1.0mMCa'
    ],

    'Type': [
        # ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        # ['Cx46', 'WT', ''],
        # ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        # ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'D51N-1', ''],
        # ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        # ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'WT', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-2', ''],
        ['Cx46', 'D51N-2', ''],
        ['Cx46', 'D51N-2', ''],
        ['Cx46', 'D51N-2', ''],
        ['Cx46', 'D51N-2', ''],
        ['Cx46', 'D51N-2', ''],
        ['Cx46', 'D51N-1', ''],
        ['Cx46', 'D51N-1', 'Rev'],
        ['Cx46', 'D51N-1', 'Rev'],
        ['Cx46', 'D51N-1', 'Rev'],
        ['Cx46', 'D51N-1', 'Rev'],
        ['Cx46', 'D51N-2', 'Rev'],
        ['Cx46', 'D51N-2', 'Rev'],
        ['Cx46', 'D51N-2', 'Rev'],
        ['Cx26', 'WT', ''],
        ['Cx26', 'WT', ''],
        ['Cx26', 'WT', ''],
        ['Cx26', 'WT', ''],
        ['Cx26', 'WT', ''],
        # ['Cx26', 'WT', ''],
        ['Cx26', 'WT', ''],
        ['Cx26', 'WT', ''],
        ['Cx26', 'D50A-1', ''],
        ['Cx26', 'D50A-1', ''],
        ['Cx26', 'D50A-1', ''],
        ['Cx26', 'D50A-1', ''],
        ['Cx26', 'D50A-2', ''],
        ['Cx26', 'D50A-2', ''],
        ['Cx26', 'D50A-2', ''],
        ['Cx26', 'D50A-2', ''],
        ['Cx26', 'D50A-2', ''],
        ['Cx26', 'D50A-2', ''],
    ]
}

Recs = {K: np.array(V) for K,V in Recs.items()}
Valid = Recs['Treatment'] != ''
Cxs = np.unique(Recs['Type'][Valid,0])
Genotype = np.unique(Recs['Type'][Valid,1])
RawTreatments = np.unique(Recs['Treatment'][Valid])
Ions = np.unique([_[5:] for _ in RawTreatments])
RecNo = len(Recs['ABFFiles'])

Treatments = {I: [] for I in Ions}
for T in RawTreatments: Treatments[T[5:]].append(float(T[:3]))

#%% Extract data from files
Analysis = {Key: [[] for _ in range(RecNo)]
            for Key in ['Voltage', 'Current', 'Time', 'Conductance',
                        'Treatment', 'Cx', 'Genotype', 'CurrentTail',
                        'ConductanceTail', 'VoltageProtocol', 'IRev', 'CurrentTailNotNorm']}

for F, ABFFile in enumerate(Recs['ABFFiles']):
    if ABFFile.split('/')[-1] in [
        '2018_09_03_0000.abf', '2018_09_05_0000.abf', '2018_09_05_0001.abf',
        '2018_09_05_0004.abf', '2018_09_05_0008.abf', '2018_09_05_0010.abf',
        '2018_09_11_0005.abf']:
        print(F)
        continue

    print(ABFFile)

    Reader = neo.io.AxonIO(ABFFile)
    Data = Reader.read()[0]

    ## Quick look
    # RawTraces(Data)
    for K in Analysis.keys():
        Analysis[K][F] = [[]for _ in range(len(Data.segments))]

    for Se, Seg in enumerate(Data.segments):
        Units = [_.units.dimensionality.string for _ in Seg.analogsignals]
        V, A = Units.index('mV'), Units.index('uA')
        Analysis['Time'][F][Se] = Seg.analogsignals[0].times.rescale('s')

        Analysis['Voltage'][F][Se] = Seg.analogsignals[V]
        Analysis['Current'][F][Se] = Seg.analogsignals[A]
        Analysis['Conductance'][F][Se] = Seg.analogsignals[A].rescale('A')/\
                                            Seg.analogsignals[V].rescale('V')
        Analysis['Conductance'][F][Se] = Analysis['Conductance'][F][Se].rescale('millisiemens')

        ## Single plots
        # Sort = np.argsort(Analysis['Voltage'][F][Se].magnitude, axis=0)[:,0]
        # AxArgs = {'xlabel': 'Time [s]', 'ylabel': Signal['VLabel']}
        # Plot.General(t, Signal['Voltage'], 'k', AxArgs=AxArgs)
        # AxArgs = {'xlabel': 'Time [s]', 'ylabel': Signal['ALabel']}
        # Plot.General(t, Signal['Current'], 'k', AxArgs=AxArgs)
        # AxArgs = {'xlabel': Signal['VLabel'], 'ylabel': Signal['ALabel']}
        # Plot.General(Signal['Voltage'][Sort], Signal['Current'][Sort], 'k.--', AxArgs=AxArgs)
        # AxArgs = {'xlabel': Signal['VLabel'], 'ylabel': Signal['SLabel']}
        # Plot.General(Signal['Voltage'][Sort], Signal['Conductance'][Sort], 'k.--', AxArgs=AxArgs)

    Rate = Analysis['Voltage'][F][-1].sampling_rate.magnitude
    Start = QuantifyTTLsPerRec(True, Analysis['Voltage'][F][-1], StdNo=0.5, Edge='fall')[0]

    ## Overrides
    if ABFFile.split('/')[-1] in [
        '2018_09_03_0003.abf', '2018_09_05_0009.abf', '2018_09_11_0007.abf']:
        Start = int(Rate*10.78)
    elif ABFFile.split('/')[-1] in [
        '2018_09_07_0000.abf', '2018_09_07_0005.abf', '2018_09_07_0006.abf',
        '2018_09_07_0007.abf', '2018_09_07_0008.abf', '2018_09_07_0009.abf',
        '2018_09_07_0010.abf', '2018_09_07_0011.abf']:
        Start = int(Rate*7.733)

    Start += int(Rate*0.005)
    End = int(Start+(Rate*0.004))

    ## Plot all segments
    # t = Analysis['Time'][F][0]
    # CurrentVoltageAllSegs(Analysis['Current'][F], Analysis['Voltage'][F], t)
    # Plt = Plot.Return('Plt')

    # Fig, Axes = Plt.subplots(2, 1, figsize=(5,3), sharex=True, dpi=150)
    # AxArgs = {}
    # AUnit = Analysis['Current'][F][0].units.dimensionality.string
    # VUnit = Analysis['Voltage'][F][0].units.dimensionality.string
    # ALabel = 'Current ['+AUnit+']'
    # VLabel = 'Voltage ['+VUnit+']'

    # AxArgs['ylabel'] = ALabel
    # Axes[0] = AllSegments(Analysis['Current'][F], t, Ax=Axes[0], AxArgs=AxArgs)
    # AxArgs['xlabel'] = 'Time [s]'
    # AxArgs['ylabel'] = VLabel
    # Axes[1] = AllSegments(Analysis['Voltage'][F], t, Ax=Axes[1], AxArgs=AxArgs)

    # Y = np.mean(Analysis['Current'][F])
    # Axes[0].plot([t[Start],t[End]], [Y,Y], 'r.-')
    # Y = np.mean(Analysis['Voltage'][F])
    # Axes[1].plot([t[Start],t[End]], [Y,Y], 'r.-')
    # Plt.show()


    Is = [_[Start:End].mean() for _ in Analysis['Current'][F]]
    Gs = [_[Start:End].mean() for _ in Analysis['Conductance'][F]]

    if Recs['Type'][F][2] == 'Rev':
        Analysis['IRev'][F] = [_[int(Rate*5):int(Rate*6)].mean() for _ in Analysis['Current'][F]]
    else:
        Analysis['IRev'][F] = ''

    Analysis['CurrentTail'][F] = [_/max(Is) for _ in Is]
    Analysis['CurrentTailNotNorm'][F] = Is
    Analysis['ConductanceTail'][F] = [_/max(Gs) for _ in Gs]
    Analysis['VoltageProtocol'][F] = [_[int(Rate*5):int(Rate*6)].mean() for _ in Analysis['Voltage'][F]]
    # Plt.plot(Vs, Is); Plt.show()
    # Plt.plot(Vs, Gs); Plt.show()


#%% Plots
Cx26 = Recs['Type'][:,0] == 'Cx26'
Cx46 = Recs['Type'][:,0] == 'Cx46'
WT = Recs['Type'][:,1] == 'WT'
Ca0 = Recs['Treatment'] == '0.0mMCa'

Cx26WT = Cx26*WT
Cx26Ca0 = Cx26*Ca0
Cx46WT = Cx46*WT
Cx46Ca0 = Cx46*Ca0
WTCa0 = WT*Ca0
Cx46RevM2 = (Recs['Type'][:,2] == 'Rev') * (Recs['Type'][:,1] == 'D51N-2')

XLabel = 'Applied voltage [mV]'
YLabel = '$I_{tail}/I_{max}$ [norm.]'


## Fig
Cx26WT = ((Recs['ABFFiles'] == DataPath+'/2018_09_11_0000.abf') +
         (Recs['ABFFiles'] == DataPath+'/2018_09_11_0001.abf')) +\
         (Recs['ABFFiles'] == DataPath+'/2018_09_11_0003.abf')
Cx26Mut = ((Recs['ABFFiles'] == DataPath+'/2018_09_11_0012.abf') +
         (Recs['ABFFiles'] == DataPath+'/2018_09_11_0014.abf')) +\
         (Recs['ABFFiles'] == DataPath+'/2018_09_11_0016.abf')
Cx46WT = ((Recs['ABFFiles'] == DataPath+'/2018_09_06_0008.abf') +
         (Recs['ABFFiles'] == DataPath+'/2018_09_06_0010.abf')) +\
         (Recs['ABFFiles'] == DataPath+'/2018_09_06_0011.abf')
Cx46Mut = (Recs['ABFFiles'] == DataPath+'/2018_09_06_0018.abf') + \
          (Recs['ABFFiles'] == DataPath+'/2018_09_06_0020.abf') +\
          (Recs['ABFFiles'] == DataPath+'/2018_09_06_0022.abf')

Vs =[Analysis['VoltageProtocol'][_] for _ in np.where((Cx26WT))[0]]
Is = [Analysis['CurrentTail'][_] for _ in np.where((Cx26WT))[0]]
Cx26WTA = [np.array([C, Is[c]]).T for c,C in enumerate(Vs)]
Vs =[Analysis['VoltageProtocol'][_] for _ in np.where((Cx26Mut))[0]]
Is = [Analysis['CurrentTail'][_] for _ in np.where((Cx26Mut))[0]]
Cx26MutA = [np.array([C, Is[c]]).T for c,C in enumerate(Vs)]
Vs =[Analysis['VoltageProtocol'][_] for _ in np.where((Cx46WT))[0]]
Is = [Analysis['CurrentTail'][_] for _ in np.where((Cx46WT))[0]]
Cx46WTA = [np.array([C, Is[c]]).T for c,C in enumerate(Vs)]
Vs =[Analysis['VoltageProtocol'][_] for _ in np.where((Cx46Mut))[0]]
Is = [Analysis['CurrentTail'][_] for _ in np.where((Cx46Mut))[0]]
Cx46MutA = [np.array([C, Is[c]]).T for c,C in enumerate(Vs)]

TxtData(Cx26WTA, Cx26MutA, File='Cx26', Save=True)
TxtData(Cx46WTA, Cx46MutA, File='Cx46', Save=True)


## All Ca[] for Cx26WT
# Override
Cx26WT = ((Recs['ABFFiles'] == DataPath+'/2018_09_11_0000.abf') +
         (Recs['ABFFiles'] == DataPath+'/2018_09_11_0002.abf')) +\
         (Recs['ABFFiles'] == DataPath+'/2018_09_11_0004.abf')
Labels = Recs['Treatment'][Cx26WT]
Vs = [Analysis['VoltageProtocol'][_] for _ in np.where((Cx26WT))[0]]
Is = [Analysis['CurrentTail'][_] for _ in np.where((Cx26WT))[0]]
VoltagesCurrents(Vs, Is, Labels, AxArgs={'title': 'Cx26 WT', 'xlabel': XLabel, 'ylabel': YLabel, 'ylim': [0,1]})#, File='Cx26WT-AllCa', Save=True)


## All Ca[] for Cx46WT
# Override
Cx46WT = (Recs['ABFFiles'] == DataPath+'/2018_09_06_0018.abf') + \
          (Recs['ABFFiles'] == DataPath+'/2018_09_06_0020.abf') +\
          (Recs['ABFFiles'] == DataPath+'/2018_09_06_0022.abf')
Labels = Recs['Treatment'][Cx46WT]
Vs = [Analysis['VoltageProtocol'][_] for _ in np.where((Cx46WT))[0]]
Is = [Analysis['CurrentTail'][_] for _ in np.where((Cx46WT))[0]]
Voltage = [Analysis['Voltage'][_] for _ in np.where((Cx46WT))[0]]
Currents = [Analysis['Current'][_] for _ in np.where((Cx46WT))[0]]
# VoltagesCurrents(Vs, Is, Labels, AxArgs={'title': 'Cx46 D51N', 'xlabel': XLabel, 'ylabel': YLabel, 'ylim': [0,1]}, File='Cx46D51N-AllCa', Save=True)
AllCaCurrents(Voltage[0], Currents, Analysis['Time'][np.where((Cx46WT))[0][0]][0], Labels, File='Cx46D51N-AllCaCurrents', Save=True, Ext=['png'])


## Cx26 WTxMut
Cx46WTMut = (Recs['ABFFiles'] == DataPath+'/2018_09_06_0008.abf') + \
            (Recs['ABFFiles'] == DataPath+'/2018_09_06_0012.abf') #+ \
            # (Recs['ABFFiles'] == DataPath+'/2018_09_06_0013.abf') + \
            # (Recs['ABFFiles'] == DataPath+'/2018_09_06_0018.abf') + \
            # (Recs['ABFFiles'] == DataPath+'/2018_09_06_0019.abf')
Labels = Recs['Type'][Cx46WTMut,1]
Vs = [Analysis['VoltageProtocol'][_] for _ in np.where((Cx46WTMut))[0]]
Is = [Analysis['CurrentTail'][_] for _ in np.where((Cx46WTMut))[0]]
VoltagesCurrents(Vs, Is, Labels, AxArgs={'title': 'Cx46 0mM $Ca^{2+}$', 'xlabel': XLabel, 'ylabel': YLabel, 'ylim': [0,1]})#, File='Cx46-WTxMut0Ca', Save=True)

## Rev
Labels = Recs['Treatment'][Cx46RevM2]
Vs = [Analysis['VoltageProtocol'][_] for _ in np.where((Cx46RevM2))[0]]
Is = [Analysis['IRev'][_] for _ in np.where((Cx46RevM2))[0]]
## Needs data fitting
IRevs = []
for i,I in enumerate(Is):
    I = np.array(I)
    R = [np.where((I <= 0))[0][-1],
          np.where((I > 0))[0][0]]
    R = [np.linspace(Vs[i][R[0]], Vs[i][R[1]], Rate),
         np.linspace(I[R[0]], I[R[1]], Rate)]

    Ri = [np.where((np.abs(R[1]) == sorted(np.abs(R[1]))[0]))[0][0],
         np.where((np.abs(R[1]) == sorted(np.abs(R[1]))[1]))[0][0]]
    Ri = np.mean([R[0][_] for _ in Ri])
    IRevs.append(Ri)

IRev(Vs, Is, IRevs, Labels, 'Cx46 D51N')


# ## All Cxs for WTCa0
# Labels = Recs['Type'][WTCa0,0]
# Vs = [Analysis['VoltageProtocol'][_] for _ in np.where((WTCa0))[0]]
# Is = [Analysis['CurrentTail'][_] for _ in np.where((WTCa0))[0]]
# VoltagesCurrents(Vs, Is, Labels, AxArgs={'title': 'WT 0mM $Ca^{2+}$', 'xlabel': XLabel, 'ylabel': YLabel, 'ylim': [0,1]})#, File='WT0Ca-AllCxs', Save=True)



## Raw signal per Ca[]
Cx26WTRaw = [-18, -17, -15]
Cx46WTRaw = [-18, -17, -15]

Plt = Plot.Return('Plt')

Fig, Axes = Plt.subplots(4,2, figsize=(8, 10), dpi=100, sharex=True)
Voltage = [Analysis['Voltage'][_] for _ in np.where((Cx26WT))[0]]
Currents = [Analysis['Current'][_] for _ in np.where((Cx26WT))[0]]
Axes[:,0] = AllCaCurrents(Voltage[0], Currents, Analysis['Time'][np.where((Cx26WT))[0][0]][0], Labels, Axes[:,0])
Voltage = [Analysis['Voltage'][_] for _ in np.where((Cx26Mut))[0]]
Currents = [Analysis['Current'][_] for _ in np.where((Cx26Mut))[0]]
Axes[:,1] = AllCaCurrents(Voltage[0], Currents, Analysis['Time'][np.where((Cx26Mut))[0][0]][0], Labels, Axes[:,1])
Axes[0,0].set_title('Cx26 WT')
Axes[0,1].set_title('Cx26 D50A')
Plot.Set(Fig=Fig)
E = 'png'
Fig.savefig('AllCa.'+E, format=E, dpi=300)
Plt.show()

#%% Data from text file (from pclamp)
TextFile = DataPath+'/Data3.txt'
with open(TextFile) as F:
    TextData = F.readlines()

TextData = [[float(s) for s in _.split()] if '\t' in _ else _ for _ in TextData]

## Data.txt
# Cx46WT00Ca = np.array(TextData[2:16])
# Cx46WT05Ca = np.array(TextData[20:34])
# Cx46WT10Ca = np.array(TextData[39:53])
# # Cx46Mut00Ca = np.array(TextData[58:72])
# # Cx46Mut05Ca = np.array(TextData[77:91])
# # Cx46Mut10Ca = np.array(TextData[112:124])
# Cx46Mut00Ca = np.array(TextData[129:143])
# Cx46Mut05Ca = np.array(TextData[147:161])
# Cx46Mut10Ca = np.array(TextData[165:179])

# Cx26WT00Ca = np.array(TextData[184:198])
# Cx26WT05Ca = np.array(TextData[203:217])
# Cx26WT10Ca = np.array(TextData[221:235])
# Cx26Mut00Ca = np.array(TextData[243:257])
# Cx26Mut05Ca = np.array(TextData[261:275])
# Cx26Mut10Ca = np.array(TextData[281:295])

## Data2.txt
Order = ['Cx26WT00Ca', 'Cx26WT05Ca', 'Cx26WT10Ca',
         'Cx26Mut00Ca', 'Cx26Mut05Ca', 'Cx26Mut10Ca',
         'Cx46WT00Ca', 'Cx46WT05Ca', 'Cx46WT10Ca',
         'Cx46Mut00Ca', 'Cx46Mut05Ca', 'Cx46Mut10Ca',
         'Cx46Mut200Ca', 'Cx46Mut205Ca', 'Cx46Mut210Ca']

## Data3.txt
Order = ['Cx26WT00Ca', 'Cx26WT05Ca', 'Cx26WT10Ca',
         'Cx26Mut00Ca', 'Cx26Mut05Ca', 'Cx26Mut10Ca',
         'Cx46WT00Ca', 'Cx46WT05Ca', 'Cx46WT10Ca',
         'Cx46Mut00Ca', 'Cx46Mut05Ca', 'Cx46Mut10Ca']

tData = [np.array(TextData[_*15+1:_*15+15]) for _ in range(len(Order))]
TxtData(tData[:3], tData[3:6], File='Cx26', Save=True)
TxtData(tData[6:9], tData[9:12], File='Cx46-1', Save=True)
TxtData(tData[6:9], tData[12:], File='Cx46-2', Save=True)
