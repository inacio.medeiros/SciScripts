#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2018-02-26
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import os
import numpy as np
from glob import glob

# from DataAnalysis import DataAnalysis
from DataAnalysis.Stats import RTTest
from DataAnalysis.Units import Klusta, Units, Circus
from IO import IO

# from DataAnalysis.Plot import Klusta as KlPlot, Units as UnitsPlot, Plot
from DataAnalysis.Plot import Plot, Units as UnitsPlot
plt = Plot.Return('plt')

Parameters = dict(
    # Clustering
    Folders = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*UnitRec')),
    Probe = 'A16',
    ProbeSpacing = 50,

    # Units
    TimeWindow = [-50, 50],
    ISISize = 0.01,
    BinSize = 1,

    # Plots
    Show = False,
    Save = True,
    Ext = ['svg'],
)

# Klusta.ClusterizeSpks(**Parameters)
# Klusta.Units(**Parameters)
AnalysisFolder = os.environ['ANALYSISPATH']
Group = 'Recovery'


#%% Clusterize
Folders = sorted(glob(os.environ['DATAPATH']+'/'+Group+'/*UnitRec'))
Folders = Folders[2:]
Probe, ProbeSpacing = 'A16', 50
Klusta.ClusterizeSpks(Folders, ProbeSpacing, Log=None)


#%% Get and plot features
ClustersFolders = sorted(glob(AnalysisFolder+'/'+Group+'/**/*_AllUnits', recursive=True))
ClustersFolders = [ClustersFolders[1]]
Save, Show = True, False

for Folder in ClustersFolders:
    AnalysisPath = '_'.join(Folder.split('_')[:-1])
    ## Run once
    # UnitRec = Units.GetAllUnits(AnalysisPath+'_AllClusters', **Parameters)
    # CellsResponse = Units.GetCellsResponse(UnitRec.copy(), AnalysisPath+'_AllUnits', Save)
    # CellsParameters = Units.GetUnitsParameters(UnitRec.copy(), CellsResponse.copy(), AnalysisPath+'_CellsParameters', Save)

    ## Then just load
    UnitRec = Units.UnitRecLoad(AnalysisPath+'_AllUnits')
    if not len(UnitRec['Raster']): continue
    if 'StimType' not in UnitRec.keys(): continue
    if not True in [bool(len(_)) for _ in UnitRec['StimType']]: continue

    CellsResponse = {_: UnitRec[_].copy() for _ in ['SpkResp', 'SpkCount']}
    CellsParameters = IO.Bin.Read(AnalysisPath+'_CellsParameters')[0]

    ## and plot
    Exp = AnalysisPath.split(AnalysisPath.split('/')[-3])[-1][1:]
    ExtraInfo = {**CellsResponse}
    FigPath = '/'.join(AnalysisPath.split('/')[:-2]) + '/Plots/Units'
    UnitsPlot.Features(UnitRec.copy(), None, ExtraInfo, Exp, FigPath, ['pdf'], Save, Show)


#%% Make examples
AnalysisPath = AnalysisFolder+'/Recovery/20160703-Recovery_03-UnitRec/Units/20160703-Recovery_03-UnitRec_Recovery_Exp00_AllUnits'
Save, Show = False, True

Examples = [
    # Unit, FreqIndex, dBIndex
    # [4, 4, 2],
    # [113, 0, 3],
    # [125, 3, 1],
    # [55, 0, 3],
    [13, 0, 3],
    # [41, 3, 1]
]

ThisGroup = AnalysisPath.split('/')[-1].split('-')[1].split('_')[0]
UnitRec = Units.UnitRecLoad(AnalysisPath)
Rate = round(1/(UnitRec['RasterX'][1,0]-UnitRec['RasterX'][0,0]))*1000
Intensities = np.unique(UnitRec['dB'])
Freqs = sorted(np.unique(UnitRec['Freq']), key=lambda x: int(x.split('-')[0]))
Freqs = np.array(Freqs)

for E,Example in enumerate(Examples):
    Unit, Freq, dB = Example
    Ind = np.where((UnitRec['UnitId'] == Unit) * (UnitRec['Freq'] == Freqs[Freq]) * (UnitRec['dB'] == Intensities[dB]))[0]

    ExtraInfo = {}
    if 'SpkResp' in UnitRec.keys():
        ExtraInfo['SpkResp'] = [UnitRec['SpkResp'][_] for _ in Ind]
        ExtraInfo['FiringRate'] = [UnitRec['FiringRate'][_] for _ in Ind]

    Spks = UnitRec['Spks'][UnitRec['UnitId'] == Unit]
    if 'str' in str(type(Spks[0])): Spks = [IO.Bin.Read(_)[0] for _ in Spks]
    Spks = np.concatenate(Spks, axis=0)
    SpkX = np.arange(Spks.shape[1])*1000/Rate

    DataExample = {
        'UnitId': UnitRec['UnitId'][Ind],
        'Spks': UnitRec['Spks'][Ind],
        'Raster': UnitRec['Raster'][Ind],
        'PSTH': UnitRec['PSTH'][Ind],
        'SpkResp': UnitRec['SpkResp'][Ind],
        'StimType': UnitRec['StimType'][Ind],
        'FiringRate': UnitRec['FiringRate'][Ind],
        'PSTHX': UnitRec['PSTHX'][:,Ind],
        'RasterX': UnitRec['RasterX'][:,Ind],
        'Exp': Exp
    }

    IO.Asdf.Write(DataExample, os.environ['ANALYSISPATH']+'/MalfattiEtAl2020/Units/'+ThisGroup+'-Example.asdf')
    UnitsPlot.Features(DataExample, None, ExtraInfo, Exp, '.', ['pdf'], Save=Save, Show=Show)

    # Fig, Ax = plt.subplots(figsize=[4.3,4.3])
    # Ax = UnitsPlot.WF_BestCh(Spks, SpkX, StimType=['Sound'], Ax=Ax)
    # if Save: Fig.savefig('WF_BestCh-Unit'+"{0:04d}".format(Unit)+'.pdf')
    # if Show: plt.show()
    # else: plt.close()


#%% Unit features
AnalysisPath = AnalysisFolder+'/Recovery/20160703-Recovery_03-UnitRec/Units/20160703-Recovery_03-UnitRec_Recovery_Exp00_AllUnits'
ThisGroup = AnalysisPath.split('/')[-1].split('-')[1].split('_')[0]
Save, Show = True, False
dB = 80

CellsParameters = IO.Bin.Read('_'.join(AnalysisPath.split('_')[:-1])+'_CellsParameters')[0]

# UnitRec = Units.UnitRecLoad(AnalysisPath)
# Resp = (UnitRec['SpkResp'] < 0.05)*(UnitRec['dB'] == dB)
# UnitsId = np.unique(UnitRec['UnitId'][Resp])

FR, Sharpness, FreqBest = Units.FRFreqSharp(CellsParameters, dB)

Sets = [FR, Sharpness, FreqBest]
FigNames = ['UnitsFR', 'UnitsSharpness', 'UnitsBestFreq']
YLabel = ['Firing rate [Hz]', 'Response broadness [a.u.]', 'Frequency [kHz]']

for D,Data in enumerate(Sets):
    Sep = [[], []]
    for P in range(Data.shape[0]):
        if Data[P,0] > Data[P,1]: Sep[0].append(Data[P,:])
        else: Sep[1].append(Data[P,:])
    Sep = [np.array(_) for _ in Sep]

    SepSets = [Data, Sep[0], Sep[1]]
    IO.Bin.Write(SepSets, AnalysisFolder+'/MalfattiEtAl2020/Units/'+ThisGroup+'-UnitsFeatures/'+FigNames[D])

    FigSize = Plot.FigSize.copy()
    FigSize[1] *= 0.7
    Fig, Axes = plt.subplots(1,3,sharey=True, figsize=FigSize)

    YLim = 0
    for S,SepSet in enumerate(SepSets):
        Axes[S] = Plot.ScatterMeanPaired(SepSet, Alpha=0.3, AxArgs={'xticklabels': ['NaCl', 'CNO']}, Ax=Axes[S])
        # Axes[S] = Plot.ScatterMeanPaired(SepSet, ['NaCl', 'CNO'], Ax=Axes[S])

        Axes[S].text(1.5, min(Axes[S].get_ylim()), 'n = '+str(len(Axes[S].get_lines())-6), ha='center')

        p = RTTest(SepSet[:,0], SepSet[:,1], Alt='two.sided')['p.value']
        # p = str(round(p,4))
        p = str(p)

        if not YLim: YLim = max(Axes[S].get_ylim())
        Plot.SignificanceBar([0,1], [YLim]*2, 'p = '+p, Axes[S])

    Axes[0].set_ylabel(YLabel[D])
    Plot.Set(Fig=Fig)
    # Fig.savefig(Exp+'_'+FigNames[D]+'.pdf')
    plt.show()



#%% Firing rate for each animal
AnalysisPath = sorted(glob(AnalysisFolder+'/'+Group+'/**/*_AllUnits', recursive=True))

Animals = []
FRs = []
for F,Folder in enumerate(AnalysisPath):
    CellsParameters = IO.Bin.Read('_'.join(Folder.split('_')[:-1])+'_CellsParameters')[0]
    FR = Units.FRFreqSharp(CellsParameters, dB=80)[0]

    Sep = [[], []]
    for P in range(FR.shape[0]):
        if FR[P,0] > FR[P,1]: Sep[0].append(FR[P,:])
        else: Sep[1].append(FR[P,:])
    Sep = [np.array(_) for _ in Sep]

    FR = [FR, Sep[0], Sep[1]]

    Animals.append(Folder.split('/')[-1].split('-')[1])
    FRs.append(FR)

IO.Bin.Write(np.array(Animals), AnalysisFolder+'/MalfattiEtAl2020/Units/'+Group+'-FiringRateAllAnimals/Animals.dat')
IO.Bin.Write(FRs, AnalysisFolder+'/MalfattiEtAl2020/Units/'+Group+'-FiringRateAllAnimals/FRs')

# Unit, Freq, dB = Examples[0]
# Freqs = sorted(np.unique(UnitRec['Freq']), key=lambda x: int(x.split('-')[0]))
# Intensities = sorted(np.unique(UnitRec['dB']), reverse=True)
# Ind = np.where((UnitRec['UnitId'] == Unit) * (UnitRec['Freq'] == Freqs[Freq]) * (UnitRec['dB'] == Intensities[dB]))[0]

# FR = [IO.Bin.Read(_)[0] for _ in UnitRec['FiringRate'][Ind]]
# for I in range(2): plt.plot(FR[I])
# plt.show()



#%% Firing rate
AnalysisPath = sorted(glob(AnalysisFolder+'/'+Group+'/**/*_AllUnits', recursive=True))

Jitter = 5
FigSize = list(Plot.FigSize)
FigSize = [FigSize[0]*0.70, FigSize[1]*0.70]

Fig, Axes = plt.subplots(1,2, figsize=FigSize)
Label = 'True'
for Folder in AnalysisPath:
    UnitRec = Units.UnitRecLoad(Folder)

    if not len(UnitRec['Raster']): continue

    if 'StimType' not in UnitRec.keys(): continue
    if not True in [bool(len(_)) for _ in UnitRec['StimType']]:
        continue

    Freqs = np.unique(UnitRec['Freq'][UnitRec['Freq'] != '634THz']).tolist()
    if '' in Freqs: del(Freqs[Freqs.index('')])
    Freqs = sorted(Freqs, key=lambda x: int(x.split('-')[0]))

    ShortFreqs = ['-'.join([_[:-3] for _ in Freq.split('-')]) for Freq in Freqs]
    UnitsId = np.unique(UnitRec['UnitId'])
    Colors = plt.get_cmap()(np.linspace(0,0.8,len(Freqs)))

    for U,Unit in enumerate(UnitsId):
        Ind = (UnitRec['UnitId'] == Unit) * (UnitRec['dB'] == 80) * (UnitRec['StimType'] == 'Sound_NaCl')
        # Ind = (UnitRec['UnitId'] == Unit) * (UnitRec['dB'] == 80) * (UnitRec['StimType'] == 'Sound')
        # UnitFreqs = UnitRec['Freq'][Ind]

        FR = [IO.Bin.Read(_)[0] for _ in UnitRec['FiringRate'][Ind]]
        FR = [_.mean() for _ in FR]
        FR = [_ if _ else np.nan for _ in FR]
        BF = FR.index(np.nanmax(FR))
        BestFreq = UnitRec['Freq'][Ind][BF]
        Jit = np.random.randint(-Jitter,Jitter)
        DV = UnitRec['DV'][Ind][BF]
        if DV < 0: DV = -DV+3500

        DVJit = (DV+Jit)/1000
        DVJit = -DVJit if DVJit > 0 else DVJit

        Axes[1].plot(Freqs.index(BestFreq), DVJit, 'k.', alpha=0.3)

        for F,Freq in enumerate(Freqs):
            Jit = np.random.randint(-Jitter,Jitter)
            DVJit = (DV+Jit)/1000
            DVJit = -DVJit if DVJit > 0 else DVJit

            Color = Colors[F]
            if len(Label):
                Label = '' if U else '-'.join([_[:-3] for _ in Freq.split('-')])

            Axes[0].plot(FR[F], DVJit, color=Color, marker='.', linestyle='', alpha=0.5, label=Label)

    Label = ''


Axes[1].set_xticks(range(len(ShortFreqs)))
Axes[1].set_xticklabels(ShortFreqs)
Axes[0].set_ylabel('DV coord. [mm]')
Axes[0].set_xlabel('Firing rate [Hz]')
Axes[1].set_xlabel('Frequency [kHz]')
Axes[0].set_title('All frequencies')
Axes[1].set_title('Best frequency')
Axes[0].set_xlim([-1,61])
Axes[0].legend(loc='lower right', frameon=False)

for Ax in Axes:
    Ax.set_ylim([-4.3, -3.5])
    Plot.Set(Ax=Ax)

Plot.Set(Fig=Fig)
Fig.savefig(AnalysisPath.split('/')[-1]+'-BestFreqPerDepth.pdf')
plt.show()


#%% Unit audiograms
UpSample = 3
AnalysisPath = sorted(glob(AnalysisFolder+'/'+Group+'/**/*_AllUnits', recursive=True))

for Fo,Folder in enumerate(AnalysisPath):
    ThisGroup = AnalysisPath.split('/')[-1].split('-')[1].split('_')[0]

    UnitRec = Units.UnitRecLoad(Folder)
    if not len(UnitRec['Raster']): continue

    if 'StimType' not in UnitRec.keys(): continue
    if not True in [bool(len(_)) for _ in UnitRec['StimType']]:
        continue

    UnitsId = np.unique(UnitRec['UnitId'])

    Freqs = np.unique(UnitRec['Freq'][UnitRec['Freq'] != '634THz']).tolist()
    if '' in Freqs: del(Freqs[Freqs.index('')])
    Freqs = sorted(Freqs, key=lambda x: int(x.split('-')[0]))
    StrFreqs = ['-'.join([_[:-3] for _ in Freq.split('-')]) for Freq in Freqs]
    ShortFreqs = [sum([int(_) for _ in F.split('-')])//(2*1000) for F in Freqs]

    Intensities = sorted(np.unique(UnitRec['dB']), reverse=True)
    if 0 in Intensities: del(Intensities[Intensities.index(0)])

    for U,Unit in enumerate(UnitsId):
        FreqdB, FreqdBI = [[], []], [[], []]

        for S,Stim in enumerate(['Sound_NaCl', 'Sound_CNO']):
            print('Folder', Fo, 'Unit', U, 'Stim', S)
            FreqdB[S] = np.zeros((len(Intensities),len(Freqs)), dtype='float32')

            for I,Intensity in enumerate(Intensities):
                for F,Freq in enumerate(Freqs):
                    Ind = (
                        UnitRec['UnitId'] == Unit) * (
                        UnitRec['dB'] == Intensity) * (
                        UnitRec['Freq'] == Freq) * (
                        UnitRec['StimType'] == Stim
                    )

                    if not True in Ind:
                        FreqdB[S][I,F] = 0
                        continue

                    while UnitRec['FiringRate'][Ind].shape[0] > 1:
                        Ind[np.where((UnitRec['FiringRate'] == UnitRec['FiringRate'][Ind][1]))[0]] = False

                    FR = IO.Bin.Read(UnitRec['FiringRate'][Ind][0])[0]
                    FreqdB[S][I,F] = FR.mean()

            IntensitiesI = np.interp(np.linspace(0,1,len(Intensities)*UpSample), np.linspace(0,1,len(Intensities)), Intensities)
            FreqsI = np.interp(np.linspace(0,1,len(ShortFreqs)*UpSample), np.linspace(0,1,len(ShortFreqs)), ShortFreqs)

            # interpolate intensities
            FreqdBI1 = np.zeros((len(Intensities),len(FreqsI)), dtype='float32')
            for I in range(FreqdB[S].shape[0]):
                FreqdBI1[I,:] = np.interp(np.linspace(0,1,len(FreqsI)), np.linspace(0,1,FreqdB[S].shape[1]), FreqdB[S][I,:])

            # interpolate freqs
            FreqdBI[S] = np.zeros((len(IntensitiesI),len(FreqsI)), dtype='float32')
            for F in range(FreqdBI1.shape[1]):
                FreqdBI[S][:,F] = np.interp(np.linspace(0,1,len(IntensitiesI)), np.linspace(0,1,FreqdBI1.shape[0]), FreqdBI1[:,F])

        IO.Bin.Write({
            'FreqsI': FreqsI, 'IntensitiesI': IntensitiesI,
            'FreqdBI': FreqdBI, 'FreqdB': FreqdB
        }, os.environ['ANALYSISPATH']+'/MalfattiEtAl2020/Units/'+ThisGroup+'-Audiogram')

        IO.Txt.Write({'ShortFreqs': ShortFreqs, 'Intensities': Intensities},
                     os.environ['ANALYSISPATH']+'/MalfattiEtAl2020/Units/'+ThisGroup+'-Audiogram/Info.dict')

        Title = 'Unit'+"{0:04d}".format(Unit)+'_Audiogram'
        FigFile = '/'.join(Folder.split('/')[:-2])+'/Plots/Units'
        os.makedirs(FigFile, exist_ok=True)
        FigFile = FigFile+'/'+Title+'.pdf'

        FigSize = Plot.FigSize.copy()
        FigSize[0] *= 0.7
        Fig, Axes = plt.subplots(2,2,figsize=FigSize)
        for S in range(2):
            Cf1 = Axes[S][0].pcolormesh(FreqsI, IntensitiesI, FreqdBI[S]/abs(FreqdBI[S]).max())

            Axes[S][-1].plot(ShortFreqs, FreqdB[S].T)

            for Ax in Axes[S]:
                Ax.set_xticks(ShortFreqs)
                Plot.Set(Ax=Ax)


            Fig.colorbar(Cf1, ax=Axes[S][0], ticks=np.linspace(0,1,5), label='Firing rate [norm.]')

        for Ax in Axes[-1]: Ax.set_xlabel('Frequency [kHz]')
        for Ax in Axes:
            Ax[0].set_ylabel('Intensity [dBSPL]')
            Ax[-1].set_ylabel('Firing rate [Hz]')

        for Ax in Axes:
            for L,Line in enumerate(Ax[-1].get_lines()):
                Line.set_label(str(int(Intensities[L]))+'dB')

            Ax[-1].legend(frameon=False)
            Plot.ApplyColorMapToCycle(Ax[-1])

        Fig.suptitle(Title)
        Plot.Set(Fig=Fig)
        # Fig.savefig(FigFile, dpi=300)
        plt.show()
        # plt.close()


#%% for MSc - new dataset
AnalysisPath = AnalysisFolder+'/OptogeneticExcitation'
TimeWindow = [-50, 50]
BinSize = 1
Show = False
Save = True
Ext = ['svg']
TTLCh = None
AnalogTTLs = True
PrmFiles = sorted(glob(AnalysisPath+'/**/*.prm', recursive=True))
for PrmFile in PrmFiles:
    UnitRec = Klusta.GetAllUnits(PrmFile, TimeWindow, TTLCh, BinSize, AnalogTTLs, Save)
    if not UnitRec['UnitId'].size:
        print('No good units in', PrmFile.split('/')[-1])
        continue

    UnitsFile = '/'.join(PrmFile.split('/')[:-2]) + '/'+ PrmFile.split('/')[-3]
    UnitsFile +=  '_' + PrmFile.split('/')[-1][:-4] + '_AllUnits.asdf'
    UnitRec = Klusta.MergeUnits(UnitRec, UnitsFile, Save)


    ## Plot
    # CellsResponse = Klusta.GetCellsResponse(UnitRec)
    # ExtraInfo = {**CellsResponse, **{'FiringRate': UnitRec['FiringRate']}}
    # KlPlot.Features(UnitRec, '/'.join(PrmFile.split('/')[:-2]) + '/Plots', PrmFile.split('/')[-3]+'-AllBlocks_'+PrmFile.split('_')[-1].split('.')[0], BinSize=None, ExtraInfo=ExtraInfo, Ext=['pdf'], Save=True, Show=False)

    print('')

## Then
# AsdfFiles = dict(CaMKIIaChR2 = sorted(glob(AnalysisPath+'/**/*AllUnits.asdf', recursive=True)))
# Qnt = Klusta.QuantifyPerOpsinPerStim(AsdfFiles, Save=False, Return=True)


#%% Circus
Save = True; Show = False
Folder = '20180814-Recovery_15-UnitRec'

ClustersPath = '/'.join([AnalysisFolder, Group, Folder, 'CircusFiles'])
Exp = glob(ClustersPath+'/*params')[0].split('/')[-1].split('.')[0]
Circus.GetAllClusters(ClustersPath, Exp=Exp)

ClustersPath = '/'.join([AnalysisFolder, Group, Folder, 'Units', Folder+'_'+Exp+'_AllClusters'])
Units.GetAllUnits(ClustersPath, TimeWindow=[-50,50], ISISize=0.01, TTLCh=None, BinSize=1, AnalogTTLs=True, Return=False)

AnalysisPath = '_'.join(ClustersPath.split('_')[:-1])
UnitRec = Units.UnitRecLoad(AnalysisPath+'_AllUnits')
CellsResponse = Units.GetCellsResponse(UnitRec.copy(), AnalysisPath+'_AllUnits')
CellsParameters = Units.GetUnitsParameters(UnitRec.copy(), CellsResponse.copy(), AnalysisPath+'_CellsParameters')

Exp = AnalysisPath.split(AnalysisPath.split('/')[-3])[-1][1:]
ExtraInfo = {**CellsResponse}
FigPath = '/'.join(AnalysisPath.split('/')[:-2]) + '/Plots'
UnitsPlot.Features(UnitRec.copy(), None, ExtraInfo, Exp, FigPath, ['pdf'], Save, Show)

