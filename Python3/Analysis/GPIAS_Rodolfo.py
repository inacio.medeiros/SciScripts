# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@year: 2016
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import sys
sys.path.insert(0, '../Deps/')

from IO import IO
from glob import glob
from DataAnalysis.Plot import GPIAS as GPIASPlot
from DataAnalysis import DataAnalysis, GPIAS



TimeWindow = [-0.2, 0.2]
FilterFreq = [50]    # frequency for filter
FilterOrder = 3       # butter order
Filter = 'butter'

Ext = ['svg']
Save = False
Show = True

# Folder = sorted(glob(os.environ['DATAPATH']+'/20??-*'))[-1]
# InfoFile = sorted(glob('/home/ciratti/Data/*.dict'))[-1]
# Folder = sorted(glob(os.environ['DATAPATH']+'/TBA/20190223-TBA-GPIAS/20??-*'))[-2]
# InfoFile = sorted(glob(os.environ['DATAPATH']+'/TBA/20190223-TBA-GPIAS/*.dict'))[-2]
Folder = sorted(glob('../../Data/20??-*'))[0]
InfoFile = sorted(glob('../../Data/*.dict'))[0]
AnalysisFolder = 'ToDelete'
AnalysisKey = 'ToDelete'
FigName = 'ToDelete'

Data, Rate = IO.DataLoader(Folder, AnalogTTLs=True, Unit='mV')
if len(Data.keys()) == 1:
    Proc = list(Data.keys())[0]

DataInfo = IO.Txt.Read(InfoFile)
# DataInfo['DAqs']['RecCh'] = [5]
# DataInfo['DAqs']['RecCh'] = [4,5,6]
# if 'ExpInfo' not in DataInfo:
#     DataInfo = IO.Txt.Dict_OldToNew(DataInfo)
#     IO.Txt.Write(DataInfo, InfoFile)

ExpStim = '_'.join(DataInfo['Animal']['StimType'])

GPIASRec, XValues = GPIAS.Analysis(
    Data[Proc], DataInfo, Rate[Proc], AnalysisFolder,
    AnalysisKey, TimeWindow,
    FilterFreq, FilterOrder, 'lowpass', Filter, SliceSize=100, Return=True, Save=Save, Overwrite=True)

GPIASPlot.Traces(GPIASRec, XValues, DataInfo['Audio']['SoundLoudPulseDur'],
                 FigName, False, 'Trace', Ext, {}, Save, Show)

# Peaks
Std = 2
Peaks = {F: {Trial: DataAnalysis.GetPeaks(Freq[Trial], Std=Std)['Pos'] for Trial in [
    'Gap', 'NoGap']} for F, Freq in GPIASRec['Trace'].items()}
