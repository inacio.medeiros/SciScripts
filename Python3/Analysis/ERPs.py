#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  1 19:46:01 2018

@author: ciralli
"""
#%%
import numpy as np
import os
from glob import glob

from DataAnalysis.Plot import ERPs as ERPsPlot
from DataAnalysis import ERPs, DataAnalysis


#%% Single

Folder = '/home/ciralli/Documents/PhD/Data/Nicotine/20190214-Nicotine_01-ERPs/2019-02-14_13-20-12_0515'
# Folder = '/home/malfatti/Barbara/Data/2018-12-22_13-36-19_S'
# Folder = sorted(glob('/home/Data/Ciralli/Data/20??-*'))[-1]
FilterFreq = [60]
ERPWindow = [-0.5, 1.5]


Data, Rate = ERPs.Load(Folder)
TTLs = ERPs.GetTTLs(Data, Folder)
ERP = ERPs.PairedERP(Data[:,:-2], Rate, TTLs, FilterFreq, 'lowpass', ERPWindow)
ERPMean = ERP.mean(axis=1)
Norm = DataAnalysis.Normalize(ERPMean)

ERPsPlot.Paired8Ch(Norm, np.linspace(-ERPWindow, ERPWindow+1, ERPMean.shape[0]))


#%% Batch

## One group
Group = 'Control'
Folders = sorted(glob(os.environ['DATAPATH']+'/'+Group+'/*ERPs/20??-*'))

## All ERPs ever
Folders = sorted(glob(os.environ['DATAPATH']+'/*/*ERPs/20??-*'))

# Remove Impedance folders
Folders = [_ for _ in Folders if 'Impedance' not in _]

# ERPs parameters
FilterFreq = [60]
ERPWindow = 2

# Comodulation parameters
PhaseFreqBand = [1, 20]
PhaseFreqBandWidth = 4
AmpFreqBand = [20, 200]
AmpFreqBandWidth = 10
FilterOrder = 3


Save = True
Show= False

for Folder in Folders:
    AnalysisFolder = os.environ['ANALYSISPATH']+'/'+'/'.join(Folder.split('/')[-3:-1])

    Data, Rate = ERPs.Load(Folder)

    if 'Baseline' in Folder:
        # Downsample
        Data, Rate = Data[::Rate//1000,:16], 1000

        if Data.shape[0] > 300*Rate: Data = Data[:300*Rate,:]
        Data = Data[:DataAnalysis.GetPowerOf2(Data.shape[0], False),:]
        Data = DataAnalysis.FilterSignal(Data, Rate, [2, Rate//2-1], FilterOrder)

        Cmdlgrm, AmpFreq, PhaseFreq = DataAnalysis.Comodulation(
            Data, Rate, PhaseFreqBand, PhaseFreqBandWidth, AmpFreqBand,
            AmpFreqBandWidth, FilterOrder, AnalysisFolder+'/Comodulogram', Save=Save
        )

        ERPsPlot.Plot.Comodulogram(Cmdlgrm, AmpFreq+PhaseFreqBandWidth//2, PhaseFreq+PhaseFreqBandWidth//2,
                                   File=AnalysisFolder+'/Plots/'+'_'.join(Folder.split('/')[-2:]+'_Cmdlgrm'),
                                   Save=Save, Show=Show)

    else:
        TTLs = ERPs.GetTTLs(Data, Folder)
        ERP, X = ERPs.PairedERP(Data[:,:-2], Rate, TTLs, FilterFreq, ERPWindow, AnalysisFolder+'/ERPs', Save)
        Norm = DataAnalysis.Normalize(ERP.mean(axis=1))

        ERPsPlot.Paired8Ch(Norm, X,
                           File=AnalysisFolder+'/Plots/'+'_'.join(Folder.split('/')[-2:])+'_ERPs',
                           Save=Save, Show=Show)

    del(Data, Cmdlgrm, ERP)




#%% Old way of recording

# def AllCh(Data, X=[], AxArgs={}, TimeMarks=[], NormalizeChannels=False):
#     Plt = Plot.Return('plt')
#     if not len(X): X = np.arange(Data.shape[0])

#     if NormalizeChannels:
#         for Ch in range(Data.shape[1]):
#             Data[:,Ch] -= Data[:,Ch].min()
#             Data[:,Ch] /= Data[:,Ch].max()

#     Fig, Ax = Plt.subplots()

#     for T in TimeMarks:
#         if len(T) > 2: Color = T[2]
#         else: Color = 'k'

#         if len(T) > 3: Alpha = T[3]
#         else: Alpha = 1

#         if len(T) > 4: Label = T[4]
#         else: Label = ''

#         Ax.axvspan(T[0], T[1], color=Color, alpha=Alpha, label=Label)

#     Spaces = Plot.GetSpaces({str(Ch): Data[:,Ch] for Ch in range(Data.shape[1])})

#     YTicks = []
#     for Ch in range(Data.shape[1]):
#         Ax.plot(X, Data[:,Ch]+Spaces[Ch])
#         YTicks.append((Data[:,Ch]+Spaces[Ch]).mean())

#     Ax.set_yticks(YTicks)
#     Ax.set_yticklabels([_+1 for _ in range(Data.shape[1])])
#     Ax.legend(loc='best')

#     Ax.tick_params(left=False)
#     Ax.spines['left'].set_visible(False)
#     Plot.Set(Ax=Ax, AxArgs=AxArgs, Fig=Fig)
#     Plt.show()



# # Path = '/home/ciralli/Documents/PhD/Data/20180803-DCXIT_01-ERPs/2018-08-03_08-21-08_12-14'
# Path = '/home/malfatti/Barbara/Data/PhD/20180731-DCXIT_01-ERPs/2018-07-31_15-41-26_1214'
# FilterFreq = [60]
# # InvalidChs = [13,14]
# ChMap = DataAnalysis.RemapCh('Ciralli', 'None16')
# ChNo = len(ChMap)

# Data, Rate = IO.DataLoader(Path, ChannelMap=ChMap)
# Data, Rate = Data['100'], Rate['100']

# Recs = sorted(Data.keys(), key=lambda x: int(x))

# # Put all recs in one 3D array (Samples x Channels x Recs)
# All = np.zeros((Rate, ChNo, len(Recs)), dtype='float32')
# for R, Rec in enumerate(Recs):
#     All[:Data[Rec].shape[0],:,R] = Data[Rec][:Rate,:ChNo]

# # Filtered mean of recs for all channels
# Mean = All.mean(axis=2)
# for Ch in range(Mean.shape[1]):
#     Mean[:,Ch] = FilterSignal(Mean[:,Ch], Rate, FilterFreq, Type='lowpass')

# # Mean PSD of recs for all channels
# F = np.zeros(((All.shape[0]//2)+1, All.shape[1], All.shape[2]), dtype=All.dtype)
# PSDs = np.zeros(((All.shape[0]//2)+1, All.shape[1], All.shape[2]), dtype=All.dtype)
# for R in range(All.shape[2]):
#     for Ch in range(All.shape[1]):
#         F[:,Ch,R], PSDs[:,Ch,R] = PSD(All[:,Ch,R], Rate, WindowSize=All.shape[0])

# F = F.mean(axis=2).mean(axis=1)
# PSDs = PSDs.mean(axis=2)

# # Spectrogram of the mean of recs for each channel
# Fxx, Txx, Sxx = Spectrogram(Mean, Rate, 3)


# ## Plots
# AllCh(
#     Mean, np.arange(Rate)/Rate,
#     AxArgs={'xlim': [0, 1],
#             'xlabel': 'Time [s]',
#             'ylabel': 'Channels [norm.]'},
#     TimeMarks=[[0, 0.05, 'r', 0.4, 'Sound click'],
#                [0.5, 0.55, 'r', 0.4],
#                [0.02, 0.021, 'b', 0.4, 'P20'],
#                [0.52, 0.521, 'b', 0.4]],
#     NormalizeChannels=True
# )

# AllCh(
#     PSDs[F<max(FilterFreq)], F[F<max(FilterFreq)],
#     AxArgs={'xlim': [F[0], F[F<max(FilterFreq)][-1]],
#             'xlabel': 'Frequency [Hz]',
#             'ylabel': 'PSD [V²/Hz, norm.]'},
#     TimeMarks=[[4,12,'g',0.4, 'Theta range']],
#     NormalizeChannels=True
# )

# Fig, Axes = Plt.subplots(Mean.shape[1],1, figsize=(5,10), sharex=True)
# for Ch in range(Mean.shape[1]):
#     Axes[Ch] = Plot.Spectrogram(Txx, Fxx, Sxx[:,Ch,:], Ax=Axes[Ch], AxArgs={'ylim': [0,20]})
# # Plot.Set(Fig=Fig)
# Plt.show()
