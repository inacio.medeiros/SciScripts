#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@year: 2016
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

#import GPIAZon
import numpy as np
import os

from DataAnalysis import DataAnalysis, GPIAS
from DataAnalysis.Plot import GPIAS as GPIASPlot
from glob import glob
from IO import IO


# %% Single
TimeWindow = [-0.2, 0.2]
FilterFreq = [50]    # frequency for filter
FilterOrder = 3       # butter order
Filter = 'butter'

Ext = ['svg']
Save = False
Show = True

# Folder = sorted(glob(os.environ['DATAPATH']+'/20??-*'))[-1]
# InfoFile = sorted(glob('/home/ciratti/Data/*.dict'))[-1]
# Folder = sorted(glob(os.environ['DATAPATH']+'/TBA/20190223-TBA-GPIAS/20??-*'))[-2]
# InfoFile = sorted(glob(os.environ['DATAPATH']+'/TBA/20190223-TBA-GPIAS/*.dict'))[-2]
Folder = sorted(
    glob('/home/malfatti/Documents/PhD/Data/Recovery/20*GPIAS/20??-*'))[-2]
InfoFile = sorted(
    glob('/home/malfatti/Documents/PhD/Data/Recovery/20*GPIAS/*.dict'))[-2]
AnalysisFolder = 'ToDelete'
AnalysisKey = 'ToDelete'
FigName = 'ToDelete'

Data, Rate = IO.DataLoader(Folder, AnalogTTLs=True, Unit='mV')
if len(Data.keys()) == 1:
    Proc = list(Data.keys())[0]

DataInfo = IO.Txt.Read(InfoFile)
# DataInfo['DAqs']['RecCh'] = [5]
# DataInfo['DAqs']['RecCh'] = [4,5,6]
# if 'ExpInfo' not in DataInfo:
#     DataInfo = IO.Txt.Dict_OldToNew(DataInfo)
#     IO.Txt.Write(DataInfo, InfoFile)

ExpStim = '_'.join(DataInfo['Animal']['StimType'])

GPIASRec, XValues = GPIAS.Analysis(
    Data[Proc], DataInfo, Rate[Proc], AnalysisFolder,
    AnalysisKey, TimeWindow,
    FilterFreq, FilterOrder, 'lowpass', Filter, SliceSize=100, Return=True, Save=Save, Overwrite=True)

GPIASPlot.Traces(GPIASRec, XValues, DataInfo['Audio']['SoundLoudPulseDur'],
                 FigName, False, 'Trace', Ext, {}, Save, Show)

# Peaks
Std = 2
Peaks = {F: {Trial: DataAnalysis.GetPeaks(Freq[Trial], Std=Std)['Pos'] for Trial in [
    'Gap', 'NoGap']} for F, Freq in GPIASRec['Trace'].items()}


# %% Batch
# DataFolder = os.environ['DATAPATH']
# AnalysisFolder = os.environ['ANALYSISPATH']
# DataFolder = '/home/Data/Ciralli/Data'
# AnalysisFolder = '/home/Data/Ciralli/Analysis'
DataFolder = '/home/malfatti/Barbara/Data'
AnalysisFolder = os.environ['ANALYSISPATH']
Group = 'Nicotine'
AnalysisFolder = AnalysisFolder+'/' + Group

TimeWindow = [-0.2, 0.2]    # in s
FilterFreq = [60]     # frequency for filter
FilterOrder = 3             # butter order
Filter = 'butter'
Stim = 'Sound'

Ext = ['svg']
Save = True
Show = False

Exps = sorted(glob(DataFolder + '/' + Group+'/2*IAS'))
# Exps = [Exps[-1]] # Just the last folder

GPIASIndexes = {}

for E, Exp in enumerate(Exps):
    # if E < 5: continue

    Folders = sorted(glob(Exp + '/' + Exp.split('/')[-1][:4] + '-*'))
    Files = sorted(glob(Exp + '/' + Exp.split('/')[-1][:4] + '*dict'))

    StimExps = []
    if Files:
        for F, Folder in enumerate(Folders):
            DataInfo = IO.Txt.Read(Files[F])

            if 'ExpInfo' not in DataInfo:
                DataInfo = IO.Txt.Dict_OldToNew(DataInfo)
                IO.Txt.Write(DataInfo, Files[F])

            if not DataInfo['ExpInfo']:
                DataInfo = IO.Txt.Dict_OldToNew(DataInfo)
                IO.Txt.Write(DataInfo, Files[F])

            if Stim in DataInfo['Animal']['StimType']:
                StimExps.append(Folder)
    else:
        StimExps = Folders

    # print(Exp)
    # print()
    # print(Folders)
    # print()
    # print(StimExps)
    # print('=====')

    StimExps.sort()
    for F, Folder in enumerate(StimExps):
        if '2016-07-02_13-05-52_Recovery_04' in Folder:
            continue

        print(Folder)

        if len(Folder.split('_')[-2]) > 2 and len(Folder.split('_')[-2].split('-')) == 1:
            Animal = '_'.join(Folder.split('_')[-2:])
        else:
            Animal = Folder.split('_')[-1]
            if '.' in Animal:
                Animal = Animal.split('.')[0]

        if Animal not in GPIASIndexes:
            GPIASIndexes[Animal] = {}

        RecFolder = Folder.split('/')[-1]

        Data, Rate = IO.DataLoader(Folder, AnalogTTLs=True, Unit='uV')

        if Folder[-5:] == '.asdf':
            del(Data['history'])
            Rate = {'100': Data['100']['Rate']}

        if len(Data.keys()) == 1:
            Proc = list(Data.keys())[0]

        if Folder[-5:] == '.asdf':
            ExpStim = 'Sound'
            DataInfo = None
            AnalysisKey = '-'.join([Folder[:-5].split('/')[-1].split('_')[0].replace(
                '-', ''), Animal]) + '-' + ExpStim + '-' + Group + '_' + 'GPIAS'
        else:
            DataInfo = IO.Txt.Read(Files[F])
            ExpStim = '_'.join(DataInfo['Animal']['StimType'])
            AnalysisKey = Files[F][:-5].split('/')[-1] + \
                '-' + ExpStim + '-' + Group + '_' + 'GPIAS'

        # if Save: os.makedirs(AnalysisFolder, exist_ok=True)

        # if DataInfo:
        #     FilterFreq = [50] if (DataInfo['DAqs']['RecCh']) == 3 else [70, 300]
        # else:
        #     FilterFreq = [70,300]

        # Specific overrides
        if RecFolder == '2017-05-21_10-34-34_Prevention_06':
            del(Data['100']['90'])

        if DataInfo['DAqs']['RecCh'] == [3, 4, 5, 6]:
            DataInfo['DAqs']['RecCh'] = [4, 5, 6]

        SavePath = Exp.replace(DataFolder+'/'+Group, AnalysisFolder)
        GPIASRec, XValues = GPIAS.Analysis(
            Data[Proc], DataInfo, Rate[Proc], SavePath,
            AnalysisKey, TimeWindow, FilterFreq, FilterOrder,
            '', Filter, Return=True, Save=Save, Overwrite=True)

        GPIASIndexes[Animal].update({F: Freq['GPIASIndex']
                                     for F, Freq in GPIASRec['Index'].items()})

        # FigPrefix = AnalysisKey.replace('/', '_')
        # FigName = '/'.join([AnalysisFolder, 'Plots', FigPrefix+'_Traces'])
        # if Save: os.makedirs('/'.join(FigName.split('/')[:-1]), exist_ok=True)
        # AxArgs = {'xlim': [-50, 150]}
        # PlotGPIAS.Traces(GPIASRec, XValues, DataInfo['Audio']['SoundLoudPulseDur'],
        #                   FigName, Ext, AxArgs, False, True)

        del(GPIASRec, XValues)


# %% DataSet
ExpNames = ['BeforeANT', 'AfterANT', 'AfterANTCNO']
DataFolder = os.environ['DATAPATH']
AnalysisFolder = os.environ['ANALYSISPATH']
Group = 'Prevention'

if Group == 'Recovery':
    Exps = dict(
        BeforeANT=[
            # Recovery
            AnalysisFolder + '/Recovery/20160217000000-Recovery_01-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160217000000-Recovery_02-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20160217000001-Recovery_01-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160217000001-Recovery_02-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160218000000-Recovery_02-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160218000000-Recovery_03-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160218000000-Recovery_04-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160218000001-Recovery_02-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20160218000001-Recovery_03-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160218000001-Recovery_04-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180313093904-Recovery_11-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180314094456-Recovery_10-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180314102807-Recovery_09-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180314110941-Recovery_07-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180315095103-Recovery_15-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180315103130-Recovery_14-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180315111223-Recovery_16-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180315115213-Recovery_08-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180315140536-Recovery_12-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180316082935-Recovery_13-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180316094733-Recovery_06-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180316102437-Recovery_05-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180320145813-Recovery_13-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180322093744-Recovery_06-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180322102502-Recovery_06-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180322104556-Recovery_05-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180322125959-Recovery_12-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180323130832-Recovery_15-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180323134406-Recovery_15-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180323140732-Recovery_16-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180323142044-Recovery_16-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180323153635-Recovery_14-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180323161049-Recovery_14-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180323162935-Recovery_08-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180323164351-Recovery_08-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180323170723-Recovery_08-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180326092113-Recovery_09-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180326103141-Recovery_10-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180326111346-Recovery_07-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180326134604-Recovery_11-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180403102322-Recovery_12-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180403105744-Recovery_12-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180403132902-Recovery_14-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180403140413-Recovery_14-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180404105109-Recovery_10-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180404112049-Recovery_10-GPIAS-Sound-Recovery_GPIAS.asdf',
        ],


        AfterANT=[
            # Recovery
            AnalysisFolder + '/Recovery/20160623135627-Recovery_01-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20160623172515-Recovery_01-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20160624102402-Recovery_03-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160624142416-Recovery_04-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160629110701-Recovery_01-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160629142020-Recovery_03-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160629151424-Recovery_04-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160630102618-Recovery_01-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20160630111225-Recovery_03-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160630115233-Recovery_04-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160630152405-Recovery_01-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160630160407-Recovery_03-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160630163909-Recovery_04-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160701093505-Recovery_01-GPIAS-Sound_NaCl-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160701103441-Recovery_03-GPIAS-Sound_NaCl-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20160701113343-Recovery_04-GPIAS-Sound_NaCl-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180718094837-Recovery_15-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180718105907-Recovery_09-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180718133820-Recovery_08-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180718141902-Recovery_10-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180719100437-Recovery_13-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180719104905-Recovery_16-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180719113356-Recovery_11-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180719132845-Recovery_14-GPIAS-Sound-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180719141140-Recovery_06-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180719145440-Recovery_07-GPIAS-Sound-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180720131952-Recovery_05-GPIAS-Sound-Recovery_GPIAS.asdf',
        ],


        AfterANTCNO=[
            # Recovery
            AnalysisFolder + '/Recovery/20160702093819-Recovery_01-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20160702113546-Recovery_03-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180727134008-Recovery_09-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180727142735-Recovery_05-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180727160904-Recovery_13-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180728101732-Recovery_07-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180728112811-Recovery_15-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180728120820-Recovery_14-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180730090841-Recovery_11-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            # AnalysisFolder + '/Recovery/20180730095227-Recovery_08-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180730103711-Recovery_16-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180730115815-Recovery_06-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
            AnalysisFolder + '/Recovery/20180730125041-Recovery_10-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
        ]
    )

elif Group == 'Nicotine':
    Exps = dict(
        BeforeANT=[
            # AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127104152-Nicotine_04-GPIAS-Sound-Nicotine_GPIAS.asdf',
            # AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127120555-Nicotine_02-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127123844-Nicotine_02-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127132447-Nicotine_01-GPIAS-Sound-Nicotine_GPIAS.asdf',
            # AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127135913-Nicotine_01-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127141943-Nicotine_03-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127145309-Nicotine_03-GPIAS-Sound-Nicotine_GPIAS.asdf',
            # AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127152457-Nicotine_05-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190127155737-Nicotine_05-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190128131121-Nicotine_01-GPIAS-Sound-Nicotine_GPIAS.asdf',
            # AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190128132423-Nicotine_05-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190128133543-Nicotine_05-GPIAS-Sound-Nicotine_GPIAS.asdf',
            # AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190128135148-Nicotine_02-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190128140258-Nicotine_02-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190127-Nicotine-GPIAS/20190128142141-Nicotine_03-GPIAS-Sound-Nicotine_GPIAS.asdf'
        ],
        AfterANT=[
            AnalysisFolder + '/Nicotine/20190203-Nicotine-GPIAS/20190203075628-Nicotine_03-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190203-Nicotine-GPIAS/20190203083940-Nicotine_01-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190203-Nicotine-GPIAS/20190203093314-Nicotine_05-GPIAS-Sound-Nicotine_GPIAS.asdf',
            AnalysisFolder + '/Nicotine/20190203-Nicotine-GPIAS/20190203101707-Nicotine_02-GPIAS-Sound-Nicotine_GPIAS.asdf',
        ]
    )

elif Group == 'RecoveryControl':
    Exps = dict(
        BeforeANT=[
            # RecoveryControl
            AnalysisFolder + '/RecoveryControl/20171020135215-RecoveryControl_04-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20171020144636-RecoveryControl_01-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20171020153418-RecoveryControl_03-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20171020161851-RecoveryControl_05-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20171020170459-RecoveryControl_02-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20171031102345-RecoveryControl_03-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            # AnalysisFolder + '/RecoveryControl/20171031111438-RecoveryControl_02-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20171031131237-RecoveryControl_01-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20171031135939-RecoveryControl_05-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            # AnalysisFolder + '/RecoveryControl/20171031145222-RecoveryControl_04-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180313082918-RecoveryControl_06-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180315132514-RecoveryControl_08-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            # AnalysisFolder + '/RecoveryControl/20180315152443-RecoveryControl_07-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180321093315-RecoveryControl_08-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180321100852-RecoveryControl_08-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            # AnalysisFolder + '/RecoveryControl/20180322112958-RecoveryControl_07-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            # AnalysisFolder + '/RecoveryControl/20180322120530-RecoveryControl_07-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180326130302-RecoveryControl_06-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            # AnalysisFolder + '/RecoveryControl/20180403124649-RecoveryControl_07-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
        ],


        AfterANT=[
            # RecoveryControl
            AnalysisFolder + '/RecoveryControl/20180117151148-RecoveryControl_04-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180117154538-RecoveryControl_01-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            # AnalysisFolder + '/RecoveryControl/20180117161858-RecoveryControl_01-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180117165545-RecoveryControl_03-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180118104340-RecoveryControl_02-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180118112014-RecoveryControl_05-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180718114321-RecoveryControl_06-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180720123729-RecoveryControl_08-GPIAS-Sound-RecoveryControl_GPIAS.asdf',
        ],


        AfterANTCNO=[
            # RecoveryControl
            AnalysisFolder + '/RecoveryControl/20180119091720-RecoveryControl_02-GPIAS-Sound_CNO-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180119095211-RecoveryControl_04-GPIAS-Sound_CNO-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180119102551-RecoveryControl_01-GPIAS-Sound_CNO-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180119110101-RecoveryControl_03-GPIAS-Sound_CNO-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180120103329-RecoveryControl_05-GPIAS-Sound_CNO-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180727125815-RecoveryControl_06-GPIAS-Sound_CNO-RecoveryControl_GPIAS.asdf',
            AnalysisFolder + '/RecoveryControl/20180727152255-RecoveryControl_08-GPIAS-Sound_CNO-RecoveryControl_GPIAS.asdf',
        ]
    )

elif Group == 'Prevention':
    Exps = dict(
        BeforeANT=[
            # Prevention
            # AnalysisFolder + '/Prevention/20170521102604-Prevention_06-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170521111251-Prevention_07-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170521133509-Prevention_08-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170521141321-Prevention_09-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170521145459-Prevention_10-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170522144047-Prevention_01-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170522152001-Prevention_02-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170522155831-Prevention_03-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170522163944-Prevention_04-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170522171635-Prevention_05-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170712105754-Prevention_01-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170712123951-Prevention_05-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170712150018-Prevention_03-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170712154233-Prevention_04-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170712162501-Prevention_02-GPIAS-Sound-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170712170940-Prevention_09-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190223154308-Prevention_14-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190223161624-Prevention_14-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190223165224-Prevention_11-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190223171946-Prevention_11-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190224100153-Prevention_13-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190224103431-Prevention_13-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190224133622-Prevention_12-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190224141201-Prevention_12-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190224165738-Prevention_15-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190224173047-Prevention_15-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225085208-Prevention_15-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225090847-Prevention_15-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225093459-Prevention_13-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225095657-Prevention_13-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225102509-Prevention_11-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225103810-Prevention_11-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225104941-Prevention_14-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225110716-Prevention_14-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225111810-Prevention_12-GPIAS-Sound-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190225114451-Prevention_12-GPIAS-Sound-Prevention_GPIAS.asdf',
        ],


        AfterANT=[
            # Prevention
            # AnalysisFolder + '/Prevention/20170720152352-Prevention_05-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170720161047-Prevention_03-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170720165432-Prevention_04-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170803105421-Prevention_05-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190412105332-Prevention_12-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190412112525-Prevention_12-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190412135848-Prevention_14-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190412143133-Prevention_14-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190415155418-Prevention_15-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190415163218-Prevention_15-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190416161817-Prevention_11-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190416165012-Prevention_11-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190418092858-Prevention_13-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190418100153-Prevention_13-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190419091952-Prevention_12-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190419095220-Prevention_12-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190419102831-Prevention_14-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190419110242-Prevention_14-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190419145723-Prevention_15-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190419153008-Prevention_15-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190430090326-Prevention_11-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190501085255-Prevention_13-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190501092927-Prevention_13-GPIAS-Sound_NaCl-Prevention_GPIAS.asdf',
        ],


        AfterANTCNO=[
            # Prevention
            # AnalysisFolder + '/Prevention/20170721105421-Prevention_05-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170721122749-Prevention_04-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            # AnalysisFolder + '/Prevention/20170721132420-Prevention_03-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190501103259-Prevention_14-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190501110106-Prevention_14-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190502161349-Prevention_11-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190502164633-Prevention_11-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190503093232-Prevention_13-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190503101110-Prevention_13-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190504090055-Prevention_15-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190504093340-Prevention_15-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190504131339-Prevention_12-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
            AnalysisFolder + '/Prevention/20190504134713-Prevention_12-GPIAS-Sound_CNO-Prevention_GPIAS.asdf',
        ]
    )

elif Group == 'PreventionControl':
    Exps = dict(
        BeforeANT=[
            # PreventionControl
            # AnalysisFolder + '/PreventionControl/20171017104136-PreventionControl_02-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            # AnalysisFolder + '/PreventionControl/20171017112858-PreventionControl_01-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20171017132258-PreventionControl_03-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            # AnalysisFolder + '/PreventionControl/20171017145730-PreventionControl_04-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20171025085051-PreventionControl_03-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            # AnalysisFolder + '/PreventionControl/20171025095719-PreventionControl_04-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            # AnalysisFolder + '/PreventionControl/20171025104825-PreventionControl_01-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            # AnalysisFolder + '/PreventionControl/20171025160536-PreventionControl_02-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190223132711-PreventionControl_08-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190223140032-PreventionControl_08-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190223143425-PreventionControl_05-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190223150731-PreventionControl_05-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190224111544-PreventionControl_07-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190224114832-PreventionControl_07-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190224144236-PreventionControl_09-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190224151758-PreventionControl_09-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190224154337-PreventionControl_06-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190224161507-PreventionControl_06-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190225065951-PreventionControl_08-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190225074503-PreventionControl_07-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190225082123-PreventionControl_05-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190225083318-PreventionControl_05-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190226093901-PreventionControl_09-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190226125027-PreventionControl_06-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190226131907-PreventionControl_06-GPIAS-Sound-PreventionControl_GPIAS.asdf',
        ],


        AfterANT=[
            # PreventionControl
            AnalysisFolder + '/PreventionControl/20180118091103-PreventionControl_03-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            # AnalysisFolder + '/PreventionControl/20180118095710-PreventionControl_04-GPIAS-Sound-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190412094650-PreventionControl_06-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190412102029-PreventionControl_06-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190413132302-PreventionControl_09-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190416093602-PreventionControl_07-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190416103805-PreventionControl_07-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190416123402-PreventionControl_05-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190416125728-PreventionControl_05-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190416133254-PreventionControl_05-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190416144544-PreventionControl_08-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190416152547-PreventionControl_08-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190418105415-PreventionControl_06-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190418112739-PreventionControl_06-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190419130129-PreventionControl_09-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190429085213-PreventionControl_07-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190429100319-PreventionControl_05-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190429103814-PreventionControl_05-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190429121439-PreventionControl_08-GPIAS-Sound_NaCl-PreventionControl_GPIAS.asdf',
        ],


        AfterANTCNO=[
            # PreventionControl
            AnalysisFolder + '/PreventionControl/20180120091759-PreventionControl_03-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            # AnalysisFolder + '/PreventionControl/20180120095557-PreventionControl_04-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190502145216-PreventionControl_05-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190503112139-PreventionControl_09-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190503115801-PreventionControl_09-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190503151835-PreventionControl_08-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190503155149-PreventionControl_08-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190503162011-PreventionControl_08-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190504105425-PreventionControl_06-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190504112859-PreventionControl_06-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190504145436-PreventionControl_07-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
            AnalysisFolder + '/PreventionControl/20190504152838-PreventionControl_07-GPIAS-Sound_CNO-PreventionControl_GPIAS.asdf',
        ]
    )


# %%
Indexes_IFAE, Traces_IFAE, X = GPIAS.GetExpsIndexesDict(Exps)
Animals = np.unique(Indexes_IFAE['Animals'])
Freqs = sorted(np.unique(Indexes_IFAE['Freqs']),
               key=lambda x: int(x.split('-')[1]))
MAF = GPIAS.GetMAF(Indexes_IFAE, Animals, ExpNames)
Indexes = GPIAS.GetMAFIndexes(Indexes_IFAE, MAF, Animals, ExpNames)


# Example
if Group == 'Recovery':
    GPIASData = [
        AnalysisFolder + '/Recovery/20180326134604-Recovery_11-GPIAS-Sound-Recovery_GPIAS.asdf',
        AnalysisFolder + '/Recovery/20180728120820-Recovery_14-GPIAS-Sound_CNO-Recovery_GPIAS.asdf',
        AnalysisFolder + '/Recovery/20180730090841-Recovery_11-GPIAS-Sound_CNO-Recovery_GPIAS.asdf'
    ]
    GPIASData = [IO.Asdf.Read(_) for _ in GPIASData]
    GPIASData = {ExpNames[G]: {'Trace': {K: V for K, V in GD['GPIAS']['Trace']['9000-11000'].items(
    ) if K not in ['Post', 'Pre']}, 'X': GD['X']} for G, GD in enumerate(GPIASData)}
    GPIASData['ExpIndexes'] = Indexes_IFAE['Index'][(
        Indexes_IFAE['Animals'] == 'Recovery_11') * (Indexes_IFAE['Freqs'] == '9000-11000')]
    GPIASData['ExpIndexes'][1] = Indexes_IFAE['Index'][(
        Indexes_IFAE['Animals'] == 'Recovery_14') * (Indexes_IFAE['Freqs'] == '9000-11000')][-1]
    IO.Bin.Write(GPIASData, AnalysisFolder +
                 '/MalfattiEtAl2020/GPIAS/Recovery-Example')

if Group == 'Prevention':
    X = np.arange(-200, 200, 400/Traces_IFAE['Traces'][16].shape[0])
    GPIASData = {
        Traces_IFAE['Exps'][_]: {'Trace': {Col: Traces_IFAE['Traces'][_][:, C]
                                           for C, Col in enumerate(['NoGap', 'Gap'])}, 'X': X}
        for _ in np.where((Traces_IFAE['Animals'] == 'Prevention_13')*(Traces_IFAE['Freqs'] == '8000-18000'))[0]
    }
    GPIASData['ExpIndexes'] = Indexes_IFAE['Index'][(
        Indexes_IFAE['Animals'] == 'Prevention_13') * (Indexes_IFAE['Freqs'] == '8000-18000')]
    IO.Bin.Write(GPIASData, AnalysisFolder +
                 '/MalfattiEtAl2020/GPIAS/Prevention-Example')


# All Freqs
DataPerFreq = np.zeros((len(Animals), len(Freqs), len(Exps)),
                       dtype=Indexes_IFAE['Index'].dtype)
for E, Exp in enumerate(Exps):
    for F, Freq in enumerate(Freqs):
        for A, Animal in enumerate(Animals):
            EFA = (Indexes_IFAE['Freqs'] == Freq) * (Indexes_IFAE['Exps']
                                                     == Exp) * (Indexes_IFAE['Animals'] == Animal)
            DataPerFreq[A, F,
                        E] = Indexes_IFAE['Index'][EFA] if True in EFA else np.nan

# Ignore NaNs
for E in range(DataPerFreq.shape[2]):
    for F in range(DataPerFreq.shape[1]):
        NaNs = np.isnan(DataPerFreq[:, F, E])
        DataPerFreq[NaNs, F, E] = np.nanmean(DataPerFreq[:, F, E])

Sec = 1
if Group == 'Recovery':
    DataPerFreq = np.concatenate(
        (DataPerFreq, DataPerFreq.mean(axis=0)[np.newaxis, :, :]), axis=0)
    Sec = 2

All = [[DataPerFreq[:, _, 0], DataPerFreq[:, _, Sec]]
       for _ in range(DataPerFreq.shape[1])]
All = abs(np.array([_ for b in All for _ in b]))

IO.Txt.Write(MAF, AnalysisFolder+'/MalfattiEtAl2020/GPIAS/'+Group+'-MAF.txt')
IO.Txt.Write(Freqs, AnalysisFolder +
             '/MalfattiEtAl2020/GPIAS/'+Group+'-Freqs.txt')
IO.Bin.Write(Indexes_IFAE, AnalysisFolder +
             '/MalfattiEtAl2020/GPIAS/'+Group+'-Indexes_IFAE')
IO.Bin.Write(Indexes, AnalysisFolder +
             '/MalfattiEtAl2020/GPIAS/'+Group+'-Indexes')
IO.Bin.Write(All, AnalysisFolder+'/MalfattiEtAl2020/GPIAS/'+Group+'-All.dat')
IO.Bin.Write({K: V for K, V in Traces_IFAE.items() if K != 'Traces'},
             AnalysisFolder+'/MalfattiEtAl2020/GPIAS/'+Group+'-Traces_IFAE')
for T, Trace in enumerate(Traces_IFAE['Traces']):
    IO.Bin.Write(Trace, AnalysisFolder+'/MalfattiEtAl2020/GPIAS/' +
                 Group+'-Traces_IFAE/Traces/Traces_'+str(T)+'.dat')

# GPIASPlot.IndexPerExp(Indexes, ExpNames, Ax=None, AxArgs={}, File='GPIAS-IndexPerExp-'+Group, Ext=['pdf'], Save=False, Show=True)
# [Stats.RTTest(Indexes[0], Indexes[1], Alt='two.sided')['p.value']*len(Freqs), Stats.RTTest(Indexes[1], Indexes[2], Alt='two.sided')['p.value']*len(Freqs)]

# {'PreventionControl': 7 [0.026987292871467606, 1.2319478371916839],
#  'RecoveryControl': 7 [0.01869875743050367, 4.873884856780353],
#  'Prevention': 8 [0.005795720277505062, 2.8914111241013374],
#  'Recovery': 11 [9.441212506970777e-09, 0.03802184488025859]}


# np.array([3.3208695046025083,1.5042228100929367,3.613213886305233,0.1396302197954058,1.9078242336316942,2.0591030098897845])-np.array([83.57719374779897,27.80068722250457,73.78770958155222,110.17114005084561,23.860298478324893,39.36260992660456])


# %% Check all animals, all freqs, all exps
GPIASPlot.AllAnimalsAllExpAllFreq(Traces_IFAE, Indexes_IFAE, ExpNames)


# %%
GroupNo = 2
AnimalsIndex = Indexes_IFAE['BeforeANT'].copy()

for A, Animal in AnimalsIndex.items():
    for F, Freq in Animal.items():
        if Freq > 0:
            AnimalsIndex[A][F] = -(100-(100/((Freq+100)/100)))

AnimalsMean = {A: np.mean([_ for _ in Animal.values()])
               for A, Animal in AnimalsIndex.items()}
Animals = list(AnimalsMean.keys())
AnimalsMean = [AnimalsMean[A] for A in Animals]
AnimalsMeanSorted = sorted(AnimalsMean)
AnimalsSorted = [Animals[AnimalsMean.index(_)] for _ in AnimalsMeanSorted]

Groups = [[AnimalsSorted[_]
           for _ in range(G, len(Animals), GroupNo)] for G in range(GroupNo)]


# %%#%% convert hdf5 to dict
# Files = glob('Prevention/*GPIAS/2*.hdf5'); Files.sort()

# for File in Files:
#     StimInfo, DataInfo = IO.Hdf5.DataLoad('/DataInfo', File)
#     DataInfo.update(StimInfo)

#     Folder = sorted(glob('/'.join(File.split('/')[:-1])+'/2*'+DataInfo['AnimalName']))[0]
#     Data, Rate = IO.DataLoader(Folder, AnalogTTLs=True, Unit='Bits')
#     if len(Data.keys()) == 1: Proc = list(Data.keys())[0]

#     GPIASPlot.Plot.AllCh(Data[Proc]['0'])

#     PiezoCh = input('PiezoCh: '); PiezoCh = [int(PiezoCh)]
#     TTLCh = input('TTLCh: '); TTLCh = int(TTLCh)

# #    DataDict = Txt.DictRead(File[:-4]+'dict')
# #    PiezoCh, TTLCh = DataDict['PiezoCh'], DataDict['TTLCh']

#     DataInfo.update({'PiezoCh': PiezoCh, 'TTLCh': TTLCh, 'FileName': File[:-4]+'dict'})
#     IO.Txt.DictWrite(File[:-4]+'dict', DataInfo)

# %% Double-check
plt = GPIASPlot.Plot.Return('plt')

TimeWindow = [-0.2, 0.2]
FilterFreq = [50]    # frequency for filter
FilterOrder = 3       # butter order
Filter = 'butter'
AnalogTTLs = True
SliceSize = 100

Ext = ['svg']
Save = False
Show = True

Folder = sorted(glob('/home/malfatti/Barbara/PhD/Data/JW/20*/20??-*'))[-1]
InfoFile = sorted(glob('/home/malfatti/Barbara/PhD/Data/JW/20*/*.dict'))[-1]
DataInfo = IO.Txt.Read(InfoFile)

Data, Rate = IO.DataLoader(Folder, AnalogTTLs=True, Unit='mV')
Data, Rate = Data['100'], Rate['100']


PrePostFreq = DataInfo['ExpInfo']['FreqOrder'][0][0]
PrePostFreq = '-'.join([str(DataInfo['Audio']['NoiseFrequency'][PrePostFreq][0]),
                        str(DataInfo['Audio']['NoiseFrequency'][PrePostFreq][1])])

GPIASData = GPIAS.PreallocateDict(
    DataInfo['Audio']['NoiseFrequency'], PrePostFreq)
GPIASData = GPIAS.OrganizeRecs(GPIASData, Data, Rate, DataInfo, AnalogTTLs,
                               TimeWindow, None, None,
                               FilterFreq, FilterOrder, Filter)

X = np.arange(int(TimeWindow[0]*Rate), int(TimeWindow[1]*Rate))*1000/Rate


for A, Array in enumerate(GPIASData['IndexTrace']['8000-10000']['Gap']):
    plt.plot(X, Array, 'b', alpha=1 /
             len(GPIASData['IndexTrace']['8000-10000']['Gap']))
    plt.plot(X, GPIASData['IndexTrace']['8000-10000']['NoGap'][A],
             'r', alpha=1/len(GPIASData['IndexTrace']['8000-10000']['Gap']))

plt.plot(X, np.mean(GPIASData['IndexTrace']['8000-10000']['Gap'], axis=0), 'b')
plt.plot(X, np.mean(GPIASData['IndexTrace']
                    ['8000-10000']['NoGap'], axis=0), 'r')
plt.show()


SliceSize = int(SliceSize * (Rate/1000))

for Freq in GPIASData['IndexTrace'].keys():
    for Key in GPIASData['IndexTrace'][Freq].keys():
        # Average trials for traces
        GPIASData['Trace'][Freq][Key] = np.mean(
            GPIASData['Trace'][Freq][Key], axis=0)
        if GPIASData['Trace'][Freq][Key].shape == ():
            print('Freq', Freq, 'trial', Key, 'is empty. Skipping...')
            continue

        for Tr in range(len(GPIASData['IndexTrace'][Freq][Key])):
            GPIASData['IndexTrace'][Freq][Key][Tr] = abs(
                GPIAS.signal.hilbert(GPIASData['IndexTrace'][Freq][Key][Tr])
            )

        GPIASData['IndexTrace'][Freq][Key] = np.mean(
            GPIASData['IndexTrace'][Freq][Key], axis=0)

    # RMS
    if Freq == PrePostFreq:
        Keys = [['Gap', 'NoGap', 'GPIASIndex'],
                ['Post', 'Pre', 'PrePost']]
    else:
        Keys = [['Gap', 'NoGap', 'GPIASIndex']]

    GPIASData['Index'][Freq] = GPIAS.IndexCalc(
        GPIASData['IndexTrace'][Freq], Keys,
        -int(TimeWindow[0]*Rate), SliceSize, False)

    print(GPIASData['Index'][Freq]['GPIASIndex'])
    plt.plot(X, GPIASData['IndexTrace'][Freq]['Gap'])
    plt.plot(X, GPIASData['IndexTrace'][Freq]['NoGap'])
    plt.show()
