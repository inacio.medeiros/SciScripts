#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20170708
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
#%% ABRs
import os
import numpy as np

from glob import glob

from DataAnalysis import ABRs
from DataAnalysis.Plot import ABRs as ABRPlot
from IO import IO

AnalysisFolder = os.environ['ANALYSISPATH']
DataFolder = os.environ['DATAPATH']
# AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'


#%% Before vs after trauma
ANTDates = {
    'Prevention': [[[20170521,20170815], 20170714], [[20190223,20190514], 20190403]],
    'PreventionControl': [[[20171017,20180124], 20180104], [[20190223,20190516], 20190406]],
    'Recovery': [[[20160217,20160703], 20160616], [[20180313,20180825], 20180524]],
    'RecoveryControl': [[[20171020,20180202], 20180103], [[20180313,20180825], 20180524]],
}

Folders = [_ for _ in glob(AnalysisFolder+'/Rec*/*-*-*')+glob(AnalysisFolder+'/Prev*/*-*-*') if '.' not in _]
# Folders = [[F for F in Folders if G+'_' in F] for G,Group in ANTDates.items()]

ExpEpochs = {'Folders':[], 'Exps':[]}
for G,Group in ANTDates.items():
    for F in Folders:
        # if F == '/home/malfatti/Documents/PhD/Analysis/Prevention/20170815-Prevention_05-UnitRec': lkjsdf
        FGroup = F.split('/')[-1].split('-')[1].split('_')[0]
        if 'CaMKIIa' in FGroup: FGroup = 'Recovery'
        if G != FGroup: continue

        Date = int(F.split('/')[-1].split('-')[0])
        Epoch = 0 if Date in range(Group[0][0][0], Group[0][0][1]+1) else 1

        ExpEpochs['Folders'].append(F)
        if Date > Group[Epoch][1]: ExpEpochs['Exps'].append('AfterANT')
        else: ExpEpochs['Exps'].append('BeforeANT')

ExpEpochs['Folders'] = ['/'.join(_.split('/')[6:]) for _ in ExpEpochs['Folders']]
ExpEpochs['Animals'] = [_.split('/')[-1].split('-')[-2] for _ in ExpEpochs['Folders']]

IO.Txt.Write(ExpEpochs, AnalysisFolder+'/ExpEpochs.dict')
ExpEpochs = {K: np.array(V) for K,V in ExpEpochs.items()}

#%% Batch
Folders = sorted(glob(DataFolder+'/[Rec,Prev]*/*-ABRs') + glob(DataFolder+'/[Rec,Prev]*/*-UnitRec'))
Log = {_: [] for _ in ['Done', 'Error', 'Exception']}

for F,Folder in enumerate(Folders):
    # if F < 103: continue

    SubFolders = sorted(glob(Folder+'/*20??-*'))
    SubFolders = [_ for _ in SubFolders if 'impedance' not in _.lower() and _[-4:] != 'dict']


    InfoFile = glob('/'.join(SubFolders[0].split('/')[:-1])+'/*.dict')[-1]
    Info = IO.Txt.Read(InfoFile)
    if 'Audio' not in Info.keys():
        Info = IO.Txt.Dict_OldToNew(Info)
        IO.Txt.Write(Info, InfoFile)

    Freqs = [Info['ExpInfo'][K]['Hz'] for K in sorted(list(Info['ExpInfo'].keys()))]
    Intensities = [str(_) for _ in Info['Audio']['Intensities']]
    Exps = [int(k) for k,K in Info['ExpInfo'].items()
            if K['Hz'].lower() not in ['bl', 'baseline']
            and 'Sound' in K['StimType']]

    SubFolders = [SubFolders[_] for _ in Exps]
    Freqs = [Freqs[_] for _ in Exps]


    Parameters = dict(
        ABRCh = Info['DAqs']['RecCh'],
        TTLCh = Info['DAqs']['TTLCh'],
        TimeWindow = [-0.003, 0.012],
        FilterFreq = [600, 1500]
    )

    ## Fix stupid TTLs
    TTLFiles = [sorted(glob(_ + '/*TTLs.TTLs')) for _ in SubFolders]
    if True in [bool(_) for _ in TTLFiles]:
        TTLFiles = [
            [
                [_ for _ in Freq if 'Rec_'+"{0:01d}".format(I)+'_TTLs.TTLs' in _][0]
                if len([_ for _ in Freq if 'Rec_'+"{0:01d}".format(I)+'_TTLs.TTLs' in _])
                else Info['DAqs']['TTLCh']
            for I,Intensity in enumerate(Info['Audio']['Intensities'])
            ]
        for Freq in TTLFiles]

        Parameters['TTLCh'] = TTLFiles

    ## Fix stupid wrong ABR channels
    NoTTLsRec = os.environ['NEBULAPATH']+'/Documents/PhD/Notes/Experiments/NoTTLRecs.dict'
    NoTTLRecs = IO.Txt.Read(NoTTLsRec)
    if InfoFile.split('/')[-1].split('.')[0] in NoTTLRecs.keys():
        ABRCh = [[Parameters['ABRCh'] for _ in a] for a in TTLFiles]
        Changed = False

        for Fr, Freq in NoTTLRecs[InfoFile.split('/')[-1].split('.')[0]].items():
            for R,Rec in Freq.items():
                if 'DataCh' in Rec.keys():
                    if type(Rec['DataCh']) == list:
                        ABRCh[int(Fr.split('_')[-1])][int(R)] = Rec['DataCh']
                        Changed = True

        if Changed: Parameters['ABRCh'] = ABRCh

    ABRs.Session(SubFolders, Freqs, Intensities, **Parameters)
    Log['Done'].append((F,Folder))

#Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-51-30_Recovery_11-0911

#%% Single rec - Single ABR intensity
# Folder = '/home/Data/Malfatti/Data/2019-03-30_10-58-18_0810'
Folder = '/home/malfatti/Documents/PhD/Data/Recovery/20180705-Recovery_08-ABRs/2018-07-05_09-50-16_Recovery_08-1012'
Rec = '0'
Data, Rate = IO.DataLoader(Folder, Unit='uV', ChannelMap=[])

Parameters = dict(
    ABRCh = [1],
    TTLCh = 0,
    TimeWindow = [-0.003, 0.012],
    FilterFreq = [600, 1500]
)

Proc = list(Data.keys())[0]
ABR, X = ABRs.Single(Data[Proc][Rec], Rate[Proc], **Parameters)

XLim = [_*1000 for _ in Parameters['TimeWindow']]
ABRPlot.Single(ABR, X, AxArgs={'xlim': XLim})#, AllCh=True)


#%% Multiple recs - Single ABR frequency
Folder = sorted(glob(DataFolder+'/20??-*'))[-2]
InfoFile = sorted(glob('/home/ciratti/Data/*.dict'))[-1]
# Folder = '/home/Data/Malfatti/Data/2019-03-30_13-39-21_1416'
# InfoFile = '/home/ciratti/Data/20190330105750-PreventionControl_06-Sound.dict'

Info = IO.Txt.Read(InfoFile)
Intensities = [str(_) for _ in Info['Audio']['Intensities']]

Parameters = dict(
    Recs = 'all',
    # ABRCh = [-1],
    # TTLCh = 21,
    ABRCh = Info['DAqs']['RecCh'],
    TTLCh = Info['DAqs']['TTLCh'],
    TimeWindow = [-0.003, 0.012],
    FilterFreq = [600, 1500]
)

Data, Rate = IO.DataLoader(Folder, Unit='uV', ChannelMap=[])
Proc = list(Data.keys())[0]
ABR, X = ABRs.Multiple(Data[Proc], Rate[Proc], Intensities=Intensities, **Parameters)

XLim = [_*1000 for _ in Parameters['TimeWindow']]
Peaks = ABRs.LatencyToPeaks(ABR, RefKey=Intensities[0], Std=1)
ABRPlot.Multiple(ABR, X, SpaceAmpF=0.75, StimDur=0.003, Peaks=Peaks, AxArgs={'xlim': XLim}, Save=False, Show=True)

## mean of all ch
# import numpy as np
# ABRPlot.Plot.AllCh(np.array([_.mean(axis=1) for _ in ABR.values()]).T, X)


#%% Multiple Freqs - Single ABR session
Folders = DataFolder+'/TBA/20190410-Prevention_13-ABRs/20??-*'
# Animal = '20181029-Pilot_03-ABRs'
# Folders = '/home/Data/Ciralli/Data/Pilot/' + Animal + '/2018-*'
Folders = sorted(glob(Folders))
Folders = [_ for _ in Folders if 'impedance' not in _.lower()]
InfoFile = glob('/'.join(Folders[0].split('/')[:-1])+'/*.dict')[0]

Info = IO.Txt.Read(InfoFile)
Freqs = [Info['ExpInfo'][K]['Hz'] for K in sorted(list(Info['ExpInfo'].keys()))]
Intensities = [str(_) for _ in Info['Audio']['Intensities']]
Exps = [int(k) for k,K in Info['ExpInfo'].items()
        if K['Hz'].lower() not in ['bl', 'baseline']
        and K['StimType'] == ['Sound']]
Folders = [Folders[_] for _ in Exps]

Parameters = dict(
    ABRCh = Info['DAqs']['RecCh'],
    TTLCh = Info['DAqs']['TTLCh'],
    # TTLCh = 21,
    TimeWindow = [-0.003, 0.012],
    FilterFreq = [600, 1500]
)

ABRs.Session(Folders, Freqs, Intensities, **Parameters)


#%% Multiple Freqs - Single ABR session from saved ABR files
Folder = AnalysisFolder + '/Recovery/20180705-Recovery_08-ABRs/ABRs'
InfoFile = glob(DataFolder + '/' + '/'.join(Folder.split('/')[-3:-1]) + '/*.dict')[0]

Info = IO.Txt.Read(InfoFile)
Freqs = [Info['ExpInfo'][K]['Hz'] for K in sorted(list(Info['ExpInfo'].keys()))]
Folders = [glob(Folder + '/' + _)[0] + '/Trial0' for _ in Freqs]
Intensities = [str(_) for _ in Info['Audio']['Intensities']]

for Folder in Folders:
    print(Folder)
    ABR = IO.Bin.Read(Folder)[0]
    X = '/'.join(Folder.split('/')[:-1])+'/X.dat'
    X = IO.Bin.Read(X)[0]

    RefKey = str(max([int(_) for _ in ABR.keys()]))
    Std = 0.85
    Peaks = ABRs.LatencyToPeaks(ABR, X, RefKey=RefKey, Std=Std)
    Thr = ABRs.GetThreshold(ABR, X, Std=Std)
    XLim = [round(X[0], 2), round(X[-1], 2)]
    ABRPlot.Multiple(ABR, X, SpaceAmpF=0.75, StimDur=0.003, Peaks=Peaks, AxArgs={'xlim': XLim, 'title': Folder.split('/')[-2]+' '+Intensities[Thr]}, File='ABRs-'+Folder.split('/')[-2], Ext=['pdf'], Save=False, Show=True)


#%% Make Figure
Folder = AnalysisFolder + '/RecoveryControl/20180105-RecoveryControl_03-ABRs/ABRs/8000-10000/Trial0/'
InfoFile = DataFolder+'/RecoveryControl/20180105-RecoveryControl_03-ABRs/20180105104441-RecoveryControl_03-Sound.dict'
Folder = AnalysisFolder+'/20180618-D2-ABRs/ABRs/14000-16000/Trial0'
InfoFile = DataFolder+'/ToBeAssigned/20180618-D2-ABRs/20180618144043-D2-Sound.dict'

Info = IO.Txt.Read(InfoFile)
Intensities = [str(_) for _ in Info['Audio']['Intensities']]
Freqs = [Info['ExpInfo'][K]['Hz'] for K in sorted(list(Info['ExpInfo'].keys()))]
Folders = [glob('/'.join(Folder.split('/')[:-2])+'/'+_)[0] for _ in Freqs]
Folders = [_+'/Trial0' for _ in Folders]

ABR = IO.Bin.Read(Folder)[0]
X = '/'.join(Folder.split('/')[:-2])+'/X.dat'
X = IO.Bin.Read(X)[0]

# import numpy as np
# WireFolder = 'Data/Recovery/20160703-CaMKIIahM4Dn08-UnitRec/2016-07-03_19-03-56_NaCl'
# WireInfo = 'Data/Recovery/20160703-CaMKIIahM4Dn08-UnitRec/20160703185210-CaMKIIahM4Dn08-SoundStim.dict'
# WireInfo = IO.Txt.Read(WireInfo)
# Data, Rate = IO.DataLoader(WireFolder, Unit='uV', ChannelMap=None)
# Procs = sorted(list(Data.keys()))
# ProbeProc, WireProc = Procs
# TTLs = Data[ProbeProc]['0'][:,WireInfo['DAqs']['TTLCh']-1]
# Wire = Data[WireProc]['0'][:,:]
# Probe = Data[ProbeProc]['0'][:,:16]
# Min = min([TTLs.shape[0], Wire.shape[0], Probe.shape[0]])
# Wire, Probe, TTLs = Wire[:Min], Probe[:Min], TTLs[:Min]
# Wire = np.hstack((Wire, TTLs.reshape((TTLs.shape[0], 1))))
# Probe = np.hstack((Probe, TTLs.reshape((TTLs.shape[0], 1))))
# WireABR, WireX = ABRs.Single(Wire, Rate[WireProc], range(1,Wire.shape[1]), Wire.shape[1], [-0.003, 0.012], [600, 1500])
# ProbeABR, ProbeX = ABRs.Single(Probe, Rate[ProbeProc], range(1,Probe.shape[1]), Probe.shape[1], [-0.003, 0.012], [600, 1500])
# WireABR, ProbeABR = WireABR[:,0], ProbeABR[:,2]
# WireABR /= WireABR.max()
# ProbeABR /= ProbeABR.max()
# plt.plot(WireX, WireABR, label='Wire'); plt.plot(ProbeX, ProbeABR, label='Probe'); plt.legend(); plt.show()

# ProbeFolder = AnalysisFolder+'/RecoveryControl/20180130-RecoveryControl_03-UnitRec/ABRs/8000-10000/Trial0/'
# ProbeABR = IO.Bin.Read(ProbeFolder)[0]
# Ch = DataAnalysis.GetStrongestCh(ProbeABR['80'])
# ProbeABR = ProbeABR['80'][:,Ch]
# ProbeABR /= ProbeABR.max()
# ProbeX = '/'.join(Folder.split('/')[:-2])+'/X.dat'
# ProbeX = IO.Bin.Read(ProbeX)[0]
# WireFolder = AnalysisFolder+'/RecoveryControl/20180105-RecoveryControl_03-ABRs/ABRs/8000-10000/Trial0/'
# WireABR = IO.Bin.Read(WireFolder)[0]
# WireABR = WireABR['80'][:,0]
# WireABR /= WireABR.max()
# WireX = '/'.join(Folder.split('/')[:-2])+'/X.dat'
# WireX = IO.Bin.Read(WireX)[0]
# plt.plot(WireX, WireABR); plt.plot(ProbeX, ProbeABR); plt.show()


Peaks = ABRs.LatencyToPeaks(ABR, Std=1)
Latencies = ABRs.LatencyPerFreq(Folders, Freqs)

Thresholds = IO.Bin.Read(AnalysisFolder+'/ThresholdsPerFreq-1stSet')[0]
ThrFreqs = sorted(Thresholds.keys(), key=lambda x: [int(y) for y in x.split('-')])
Thresholds = [Thresholds[_] for _ in ThrFreqs]
ThrFreqs = [_.split('-')[0][:-3]+'-'+_.split('-')[1][:-3] for _ in ThrFreqs]

ABRPlot.Traces_LatencyPerFreq_AllFreqs(ABR, X, Latencies, Intensities, Thresholds, ThrFreqs, StimDur=0.003, Peaks=[], Save=True, Show=True)



#%% Malfatti et al, 2020
## Example
Folder = AnalysisFolder + '/Prevention/20190410-Prevention_14-ABRs/ABRs/8000-10000/Trial0'
Std = 0.85

ABR = IO.Bin.Read(Folder, Verbose=False)[0]
X = '/'.join(Folder.split('/')[:-1])+'/X.dat'
X = IO.Bin.Read(X)[0]

RefKey = str(max([int(_) for _ in ABR.keys()]))
Peaks = ABRs.LatencyToPeaks(ABR, X, RefKey=RefKey, Std=Std)

## Group data
Group = 'Recovery'
Exps = ['BeforeANT', 'AfterANT', 'AfterANTCNO']

ExpEpochs = IO.Txt.Read(AnalysisFolder+'/ExpEpochs.dict')
ExpEpochs = {K: np.array(V) for K,V in ExpEpochs.items()}

FolderExps = Exps[:2]
# Get only ABR folders from specified group
ExpMask = np.array([
        _.split('-')[-1] == 'ABRs'
        and _.split('/')[-1].split('-')[1].split('_')[0] == Group
    for _ in ExpEpochs['Folders']
])
ExpMask = {Exp: (ExpEpochs['Exps'] == Exp)*ExpMask for Exp in FolderExps}
Folders = {Exp: ExpEpochs['Folders'][ExpMask[Exp]] for Exp in FolderExps}
Animals = {Exp: ExpEpochs['Animals'][ExpMask[Exp]] for Exp in FolderExps}

ExpMask = {K: np.ones(len(V), dtype='bool') for K,V in Folders.items()}
for E,Exp in enumerate(FolderExps):
    for F,Folder in enumerate(Folders[Exp]):
        # Get only animals with recordings for both exps
        if Animals[Exp][F] not in Animals[FolderExps[E-1]]:
            ExpMask[Exp][F] = False
            continue

        # Get only the last recording of each animal
        elif len(np.where((Animals[Exp] == Animals[Exp][F]))[0]) > 1:
            AnimalFolders = Folders[Exp][Animals[Exp] == Animals[Exp][F]]
            Dates = [int(_.split('/')[-1].split('-')[0]) for _ in AnimalFolders]
            if Folder != AnimalFolders[np.argmax(Dates)]:
                ExpMask[Exp][F] = False

Folders = {K: sorted(V[ExpMask[K]]) for K,V in Folders.items()}
Thresholds = {
    E: ABRs.GetThresholdsPerFreq([AnalysisFolder+'/'+_ for _ in Exp], Std=Std)
    for E,Exp in Folders.items()
}

Freqs = np.unique(np.concatenate((Thresholds['BeforeANT']['Freqs'], Thresholds['AfterANT']['Freqs'])))
Freqs = sorted(Freqs, key=lambda i: int(i.split('-')[1]))
for F in ['8000-16000', '8000-18000', '16000-18000']:
    if F in Freqs: Freqs.remove(F)

Scatter = [[Thresholds[Exp]['Thresholds'][Thresholds[Exp]['Freqs'] == _] for _ in Freqs] for Exp in FolderExps]
Scatter = [[Scatter[0][_], Scatter[1][_]] for _ in range(len(Freqs))]
Scatter = [_ for a in Scatter for _ in a]

IO.Bin.Write(ABR, AnalysisFolder+'/MalfattiEtAl2020/ABRs/'+Group+'-ABRs')
IO.Bin.Write(X, AnalysisFolder+'/MalfattiEtAl2020/ABRs/'+Group+'-X.dat')
IO.Bin.Write(Peaks, AnalysisFolder+'/MalfattiEtAl2020/ABRs/'+Group+'-Peaks.dat')
IO.Bin.Write(np.array(Freqs), AnalysisFolder+'/MalfattiEtAl2020/ABRs/'+Group+'-Freqs.dat')
for S,Sc in enumerate(Scatter):
    IO.Bin.Write(Sc, AnalysisFolder+'/MalfattiEtAl2020/ABRs/'+Group+f'-Scatter/Scatter_{S}.dat')


#%% Write all TTLs
Folders = glob(DataFolder+'/[Recover,Preventio]*/*ABRs/*201?-*', recursive=True)
ABRs.WriteRecAndTTLCh(Folders)


#%% Group data by hand
Thresholds = [
    55,60,60,55,60,
    65,70,80,75,80,
    60,60,70,60,45,
    80,70,70,80,99,
    65,60,65,80,75,
    50,65,60,55,50,
    99,99,99,99,99,
    55,55,65,55,00,
    50,50,65,50,60,
    65,60,65,55,55
]

Freq = [
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
    '8000-10000','9000-11000','10000-12000','12000-14000','14000-16000',
]

Animal = [
    'Old01','Old01','Old01','Old01','Old01',
    'Old02','Old02','Old02','Old02','Old02',
    'Old03','Old03','Old03','Old03','Old03',
    'Old04','Old04','Old04','Old04','Old04',
    'Old05','Old05','Old05','Old05','Old05',
    'New01','New01','New01','New01','New01',
    'New02','New02','New02','New02','New02',
    'New03','New03','New03','New03','New03',
    'New04','New04','New04','New04','New04',
    'New05','New05','New05','New05','New05',
]

#%%ForOlds
from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')

X = [0,1,2,3,4]
Old1= [55,60,60,55,60]
Old2= [65,70,80,75,80]
Old3= [60,60,70,60,45]
Old4= [80,70,70,80,99]
Old5= [65,60,65,80,75]

Fig, Ax = plt.subplots()
Ax.plot(X, Old1, '.--')
Ax.plot(X, Old2, '.--')
Ax.plot(X, Old3, '.--')
Ax.plot(X, Old4, '.--')
Ax.plot(X, Old5, '.--')

#Ax.set_xticklabels([0, 9, 10, 11, 13, 15])
Fig.savefig('PlotNew.pdf')
plt.show()


#%%ForOldsBoxPlots
X = [0,1,2,3,4]
Thrs = [
    [55,65,60,80,65],
    [60,70,60,70,60],
    [60,80,70,70,65],
    [55,75,60,80,80],
    [60,80,45,99,75]
]

Fig, Ax = plt.subplots()
Ax.boxplot(Thrs, showmeans=True)
Ax.set_xticklabels([9, 10, 11, 13, 15])
Fig.savefig('BoxplotOld.pdf')
plt.show()


#%%ForNews
X = [0,1,2,3,4]
New1 = [50,65,60,55,50]
New2 = [99,99,99,99,99]
New3 = [55,55,65,55,00]
New4 = [50,50,65,50,60]
New5 = [65,60,65,55,55]


Fig, Ax = plt.subplots()
Ax.plot(X, Old1, '.--')
Ax.plot(X, Old2, '.--')
Ax.plot(X, Old3, '.--')
Ax.plot(X, Old4, '.--')
Ax.plot(X, Old5, '.--')

Ax.set_xticklabels([0, 9, 10, 11, 13, 15])
Fig.savefig('PlotNew.pdf')
plt.show()


#%%ForNewsBoxplots
X = [0,1,2,3,4]
Thrs = [
    [50,55,50,65],
    [65,55,50,60],
    [60,65,65,65],
    [55,55,50,55],
    [50,00,60,55]
]

#ThrNew8= [50,99,55,50,65]
#ThrNew9= [65,99,55,50,60]
#ThrNew10= [60,99,65,65,65]
#ThrNew12= [55,99,55,50,55]
#ThrNew14= [50,99,00,60,55]

Fig, Ax = plt.subplots()
Ax.boxplot(Thrs, showmeans=True)
Ax.set_xticklabels([9, 10, 11, 13, 15])
Fig.savefig('BoxplotNew.pdf')
plt.show()



#%% Multiple pages
from matplotlib.backends.backend_pdf import PdfPages

with PdfPages('Boxplots.pdf') as PDF:
    X = [0,1,2,3,4]
    #Old
    Thrs = [
        [55,65,60,80,65],
        [60,70,60,70,60],
        [60,80,70,70,65],
        [55,75,60,80,80],
        [60,80,45,80,75]
    ]

    Fig, Ax = plt.subplots()
    Ax.boxplot(Thrs, showmeans=True)
    Ax.set_xticklabels([9, 10, 11, 13, 15])
    PDF.savefig()
    plt.close()

    # New
    Thrs = [
        [50,55,50,65],
        [65,55,50,60],
        [60,65,65,65],
        [55,55,50,55],
        [50,40,60,55]
    ]

    Fig, Ax = plt.subplots()
    Ax.boxplot(Thrs, showmeans=True)
    Ax.set_xticklabels([9, 10, 11, 13, 15])
    PDF.savefig()
    plt.close()


#%% ForBoth
Thrs = [
    [55,65,60,80,65],
    [50,55,50,65],
    [60,70,60,70,60],
    [65,55,50,60],
    [60,80,70,70,65],
    [60,65,65,65],
    [55,75,60,80,80],
    [55,55,50,55],
    [60,80,45,80,75],
    [50,40,60,55]
]

Pos = [1, 2, 4, 5, 7, 8, 10, 11, 13, 14]
Ticks = [1.5, 4.5, 7.5, 10.5, 13.5] #onde coloca os ticks do eixo x
Labels = ['8-10', '9-11', '10-12', '12-14', '14-16'] #os labels onde fica os ticks

Fig, Ax = plt.subplots(figsize=(7,5)) #tamanho da figura
Boxes = Ax.boxplot(Thrs, positions=Pos, showmeans=True)
for K in ['boxes', 'whiskers', 'caps', 'medians', 'fliers']:
    for I in range(len(Boxes[K])):
        Color = 'k' if not I%2 else 'r'
        Boxes[K][I].set(color=Color)

Ax.set_xticks(Ticks)
Ax.set_xticklabels(Labels)
Ax.set_xlabel('Frequency [kHz]')
Ax.set_ylabel('Threshold [dBSPL]')
Plot.Set(Fig=Fig, Ax=Ax)
Fig.savefig('BoxplotBoth.pdf')
plt.show()

Fig, Ax = plt.subplots(figsize=(7,5))
Ax.plot([1,2,3], 'k.-', label='Old')
Ax.plot([2,3,1], 'r.-', label='New')
Ax.legend()
Fig.savefig('Legend.pdf')
plt.show()

#%% Convert hdf5 info to dict
Files = glob('Prevention/*ABRs/2*.hdf5'); Files.sort()
ABRs.Hdf52TxtDict(Files)


#%% Notes
Broken = [
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_17-12-23_12-14B', 1],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-35-07_14-16B', 3],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-46-41_12-14B', 0],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-29-30_08-10B', 0,1,3,4],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-40-44_09-11B', 4],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-52-24_10-12B', 1,2,3],
    [os.environ['DATAPATH']+'/Prevention/20170704-Prevention_A1-ABRs/2017-07-04_10-44-11_14-16', 4],
    [os.environ['DATAPATH']+'/Prevention/20170718-Prevention_A5-ABRs/2017-07-18_18-28-35_09-11', 5],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-46-56_12-14B', 4],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-30-09_09-11B', 0,5],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-41-28_10-12B', 0],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-32-51_10-12', 3],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-37-32_10-12', 1,2],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-27-15_12-14', 3,5],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-21-13_12-14', 3],
    [os.environ['DATAPATH']+'/Prevention/20170718-Prevention_A4-ABRs/2017-07-18_13-09-59_14-16', 2,3],
    [os.environ['DATAPATH']+'/Prevention/20170718-Prevention_A4-ABRs/2017-07-18_12-55-21_09-11', 2],
    [os.environ['DATAPATH']+'/Prevention/20170718-Prevention_A3-ABRs/2017-07-18_16-17-37_14-16', 5],
    [os.environ['DATAPATH']+'/Prevention/20170718-Prevention_A3-ABRs/2017-07-18_16-23-10_09-11', 4],
    [os.environ['DATAPATH']+'/Prevention/20170718-Prevention_A3-ABRs/2017-07-18_16-11-36_08-10', 5],
    [os.environ['DATAPATH']+'/RecoveryControl/20171212-RecoveryControl_02-ABRs/RecoveryControl_02_2017-12-12_17-09-12_08-10', 1,2],

    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-14-59_08-10B', 'all'],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_17-12-23_12-14B', 'all'],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-46-41_12-14B', 'all'],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-32-51_10-12', 'all'],
    [os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-21-13_12-14', 'all'],
    [os.environ['DATAPATH']+'/Prevention/20170718-Prevention_A3-ABRs/2017-07-18_16-23-10_09-11', 'all'],
    [os.environ['DATAPATH']+'/RecoveryControl/20171213-RecoveryControl_05-ABRs/RecoveryControl_05_2017-12-13_10-30-50_12-14', 'all'],
    [os.environ['DATAPATH']+'/RecoveryControl/20171213-RecoveryControl_01-ABRs/RecoveryControl_01_2017-12-13_11-50-00_14-16', 'all'],
    [os.environ['DATAPATH']+'/PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-37-35_12-14', 'all'],
    [os.environ['DATAPATH']+'/PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-32-06_08-10', 'all'],
    [os.environ['DATAPATH']+'/PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-43-13_09-11', 'all'],
    [os.environ['DATAPATH']+'/PreventionControl/20171213-PreventionControl_03-ABRs/PreventionControl_03_2017-12-13_13-27-49_14-16', 'all']
]

NoTTLs = [
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-49-41_14-16',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-43-34_12-14',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-36-52_09-11',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-30-30_10-12',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_02-ABRs/2017-06-23_15-06-41_10-12',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_02-ABRs/2017-06-23_15-25-15_14-16',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_02-ABRs/2017-06-23_14-59-45_12-14',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_02-ABRs/2017-06-23_15-13-05_09-11',
    os.environ['DATAPATH']+'/Prevention/20170623-Prevention_02-ABRs/2017-06-23_15-19-21_08-10'
]

ToDel = [
    os.environ['DATAPATH']+'/Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-26-01_12-14'
]

Baseline = [
    os.environ['DATAPATH']+'/RecoveryControl/20171212-RecoveryControl_03-ABRs/RecoveryControl_03_2017-12-12_15-25-37_BL'
]

Folders = [
    'Prevention/20170623-Prevention_A2-ABRs/2017-06-23_14-59-45_12-14',
    'Prevention/20170623-Prevention_A2-ABRs/2017-06-23_15-06-41_10-12',
    'Prevention/20170623-Prevention_A2-ABRs/2017-06-23_15-13-05_09-11',
    'Prevention/20170623-Prevention_A2-ABRs/2017-06-23_15-19-21_08-10',
    'Prevention/20170623-Prevention_A2-ABRs/2017-06-23_15-25-15_14-16',
    'Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-30-30_10-12',
    'Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-36-52_09-11',
    'Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-43-34_12-14',
    'Prevention/20170623-Prevention_A3-ABRs/2017-06-23_16-49-41_14-16',
    'Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-14-59_08-10B',
    'Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-30-09_09-11B',
    'Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-35-47_14-16B',
    'Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-41-28_10-12B',
    'Prevention/20170703-Prevention_A4-ABRs/2017-07-03_15-46-56_12-14B',
    'Prevention/20170703-Prevention_A5-ABRs/2017-07-03_11-58-26_14-16',
    'Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-04-05_08-10',
    'Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-09-45_09-11',
    'Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-27-15_12-14',
    'Prevention/20170703-Prevention_A5-ABRs/2017-07-03_12-37-32_10-12',
    'Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-29-30_08-10B',
    'Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-35-07_14-16B',
    'Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-40-44_09-11B',
    'Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-46-41_12-14B',
    'Prevention/20170703-Prevention_B4-ABRs/2017-07-03_16-52-24_10-12B',
    'Prevention/20170703-Prevention_B4-ABRs/2017-07-03_17-06-44_08-10B',
    'Prevention/20170703-Prevention_B4-ABRs/2017-07-03_17-12-23_12-14B',
    'Prevention/20170704-Prevention_A1-ABRs/2017-07-04_10-21-14_12-14',
    'Prevention/20170704-Prevention_A1-ABRs/2017-07-04_10-27-34_09-11',
    'Prevention/20170704-Prevention_A1-ABRs/2017-07-04_10-33-06_10-12',
    'Prevention/20170704-Prevention_A1-ABRs/2017-07-04_10-38-35_08-10',
    'Prevention/20170704-Prevention_A1-ABRs/2017-07-04_10-44-11_14-16',
    'PreventionControl/20171213-PreventionControl_03-ABRs/PreventionControl_03_2017-12-13_13-22-20_12-14',
    'PreventionControl/20171213-PreventionControl_03-ABRs/PreventionControl_03_2017-12-13_13-27-49_14-16',
    'PreventionControl/20171213-PreventionControl_03-ABRs/PreventionControl_03_2017-12-13_13-33-15_09-11',
    'PreventionControl/20171213-PreventionControl_03-ABRs/PreventionControl_03_2017-12-13_13-38-56_10-12',
    'PreventionControl/20171213-PreventionControl_03-ABRs/PreventionControl_03_2017-12-13_13-54-21_08-10',
    'PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-32-06_08-10',
    'PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-37-35_12-14',
    'PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-43-13_09-11',
    'PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-48-35_14-16',
    'PreventionControl/20171213-PreventionControl_04-ABRs/PreventionControl_04_2017-12-13_12-54-07_10-12',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_15-04-15',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_15-21-12',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_15-28-59',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_15-37-26',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_16-10-07',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_16-17-53',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_16-26-14',
    'Recovery/20160512-CaMKIIahM4Dn07-ABRs/2016-05-12_16-35-07',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_16-57-02',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_17-08-11',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_17-21-29',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_17-35-48',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_17-56-19',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_18-07-13',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_18-18-17',
    'Recovery/20160514-CaMKIIahM4Dn06-ABRs/2016-05-14_18-29-40',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_09-46-50',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_10-00-50',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_10-14-08',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_10-24-28',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_10-35-42',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_10-45-57',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_11-02-32',
    'Recovery/20160514-CaMKIIahM4Dn08-ABRs/2016-05-14_11-14-19',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_14-12-49',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_14-23-24',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_14-34-40',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_14-48-14',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_15-03-09',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_15-17-31',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_15-30-39',
    'Recovery/20160514-CaMKIIahM4Dn09-ABRs/2016-05-14_15-44-32',
    'RecoveryControl/20171212-RecoveryControl_02-ABRs/RecoveryControl_02_2017-12-12_17-09-12_08-10',
    'RecoveryControl/20171212-RecoveryControl_02-ABRs/RecoveryControl_02_2017-12-12_17-20-50_09-11',
    'RecoveryControl/20171212-RecoveryControl_02-ABRs/RecoveryControl_02_2017-12-12_17-26-40_14-16',
    'RecoveryControl/20171212-RecoveryControl_02-ABRs/RecoveryControl_02_2017-12-12_17-43-58_12-14',
    'RecoveryControl/20171212-RecoveryControl_02-ABRs/RecoveryControl_02_2017-12-12_17-52-57_10-12',
    'RecoveryControl/20171212-RecoveryControl_03-ABRs/RecoveryControl_03_2017-12-12_15-25-37_BL',
    'RecoveryControl/20171212-RecoveryControl_03-ABRs/RecoveryControl_03_2017-12-12_15-33-31_12-14',
    'RecoveryControl/20171212-RecoveryControl_03-ABRs/RecoveryControl_03_2017-12-12_15-39-31_9-11',
    'RecoveryControl/20171212-RecoveryControl_03-ABRs/RecoveryControl_03_2017-12-12_15-52-49_10-12',
    'RecoveryControl/20171212-RecoveryControl_03-ABRs/RecoveryControl_03_2017-12-12_16-09-52_14-16',
    'RecoveryControl/20171212-RecoveryControl_03-ABRs/RecoveryControl_03_2017-12-12_16-17-41_08-10',
    'RecoveryControl/20171212-RecoveryControl_04-ABRs/RecoveryControl_04_2017-12-12_18-28-54_09-11',
    'RecoveryControl/20171212-RecoveryControl_04-ABRs/RecoveryControl_04_2017-12-12_18-34-44_12-14',
    'RecoveryControl/20171212-RecoveryControl_04-ABRs/RecoveryControl_04_2017-12-12_18-41-00_10-12',
    'RecoveryControl/20171212-RecoveryControl_04-ABRs/RecoveryControl_04_2017-12-12_18-56-08_14-16',
    'RecoveryControl/20171212-RecoveryControl_04-ABRs/RecoveryControl_04_2017-12-12_19-01-51_08-10',
    'RecoveryControl/20171213-RecoveryControl_01-ABRs/RecoveryControl_01_2017-12-13_11-26-07_12-14',
    'RecoveryControl/20171213-RecoveryControl_01-ABRs/RecoveryControl_01_2017-12-13_11-31-39_09-11',
    'RecoveryControl/20171213-RecoveryControl_01-ABRs/RecoveryControl_01_2017-12-13_11-37-14_10-12',
    'RecoveryControl/20171213-RecoveryControl_01-ABRs/RecoveryControl_01_2017-12-13_11-44-03_08-10',
    'RecoveryControl/20171213-RecoveryControl_01-ABRs/RecoveryControl_01_2017-12-13_11-50-00_14-16',
    'RecoveryControl/20171213-RecoveryControl_05-ABRs/RecoveryControl_05_2017-12-13_10-10-21_09-11',
    'RecoveryControl/20171213-RecoveryControl_05-ABRs/RecoveryControl_05_2017-12-13_10-15-55_10-12',
    'RecoveryControl/20171213-RecoveryControl_05-ABRs/RecoveryControl_05_2017-12-13_10-25-07_08-10',
    'RecoveryControl/20171213-RecoveryControl_05-ABRs/RecoveryControl_05_2017-12-13_10-30-50_12-14',
    'RecoveryControl/20171213-RecoveryControl_05-ABRs/RecoveryControl_05_2017-12-13_10-49-32_14-16'
]

Folders = [os.environ['DATAPATH']+'/'+_ for _ in Folders]
