#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 10:27:16 2019

@author: malfatti
"""

#%%
import numpy as np
import os

from glob import glob
from imp import load_source

from IO import IO, Klusta
from DataAnalysis import DataAnalysis
from DataAnalysis.ABRs import GetTTLs

from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')


#%% Rewrite huge .dat
Files = [
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-13-55_Recovery_12-1416/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-13-55_Recovery_12-1416/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_13-56-46_Recovery_08-1416/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_11-52-19_Recovery_16-1214/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-15-24_Recovery_05-0810/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_13-56-46_Recovery_08-1416/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-13-55_Recovery_12-1416/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-15-24_Recovery_05-0810/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_11-52-19_Recovery_16-1214/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-27-14_Recovery_05-1012/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_15-24-46_Recovery_12-0810/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-10-45_Recovery_08-0911/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-03-06_Recovery_16-1416/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-27-14_Recovery_05-1012/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-10-45_Recovery_08-0911/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-14-17_Recovery_16-1012/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-40-20_Recovery_05-1416/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-40-20_Recovery_05-1416/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-40-20_Recovery_05-1416/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-22-46_Recovery_08-1012/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-52-40_Recovery_05-1214/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-25-34_Recovery_16-0911/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-52-40_Recovery_05-1214/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_14-52-40_Recovery_05-1214/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-37-02_Recovery_08-0810/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_15-04-25_Recovery_05-0911/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-06-13_Recovery_12-1214/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-41-47_Recovery_16-0810/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-48-53_Recovery_08-1214/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180517-Recovery_05-ABRs/2018-05-17_15-04-25_Recovery_05-0911/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_16-ABRs/2018-05-14_12-41-47_Recovery_16-0810/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_08-ABRs/2018-05-14_14-48-53_Recovery_08-1214/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-06-13_Recovery_12-1214/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-16-48_Recovery_12-1012/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-16-48_Recovery_12-1012/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-27-44_Recovery_12-0911/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180515-Recovery_12-ABRs/2018-05-15_16-27-44_Recovery_12-0911/experiment1/recording10/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-29-40_Recovery_11-1012/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-29-40_Recovery_11-1012/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-40-48_Recovery_11-0810/experiment1/recording3/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-51-30_Recovery_11-0911/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-51-30_Recovery_11-0911/experiment1/recording6/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_15-51-30_Recovery_11-0911/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-04-38_Recovery_11-1214/experiment1/recording2/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-04-38_Recovery_11-1214/experiment1/recording5/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-04-38_Recovery_11-1214/experiment1/recording9/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-15-31_Recovery_11-1416/experiment1/recording4/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-15-31_Recovery_11-1416/experiment1/recording7/continuous/Rhythm_FPGA-100.0/continuous.dat',
    '/home/malfatti/Documents/PhD/Data/Recovery/20180514-Recovery_11-ABRs/2018-05-14_16-15-31_Recovery_11-1416/experiment1/recording8/continuous/Rhythm_FPGA-100.0/continuous.dat'
]

for F,File in enumerate(Files):
    if F < 26: continue
    Data = np.memmap(File, dtype='int16')
    Start = np.where((Data != 0))[0][0]
    Start = (Start//3)*3
    Data = Data[Start:]
    DatFile = 'DATFix/'+'/'.join(File.split('/')[6:])
    os.makedirs('/'.join(DatFile.split('/')[:-1]), exist_ok=True)
    with open(DatFile, 'wb') as F: Data.tofile(F)

    del(Data)


#%% Rewrite kwik to bin
Folders = glob('/home/Data/Malfatti/Data/Prevention/20170815-Prevention_05-UnitRec_Kwik/20??-*')

for Folder in Folders:
    a,b = IO.DataLoader(Folder, 'bits')

    for P,Proc in a.items():
        for R,Rec in Proc.items():
            File = 'experiment1_'+P+'_'+R+'.dat'
            File = Folder.replace('_Kwik','')+'/'+File

            print(Rec.dtype)
            print(R)
            print(File)
            print()

            IO.Bin.Write(Rec, File)


#%% Rewrite kwik with more recordings than expected
Folder = '/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_11-04-17_PreventionControl_03-09_11Kwik'

a,b = IO.DataLoader(Folder, 'bits')

Done = []
for P,Proc in a.items():
    Recs = [[R, Rec.shape[0]] for R,Rec in Proc.items()]
    Threshold = max([_[1] for _ in Recs])/2
    Recs = [_ for _ in Recs if _[1] > Threshold]

    for R,Rec in Recs:
        File = 'experiment1_'+P+'_'+str(len(Done))+'.dat'
        File = Folder+'/'+File
        File = File.replace('Kwik', '')

        print(Proc[R].dtype)
        print(R)
        print(File)
        print()

        IO.Bin.Write(Proc[R], File)

        Done.append(R)



#%% Update dicts
Dicts = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*UnitRec/*dict'))

for Dict in Dicts:
    Info = IO.Txt.Read(Dict)
    Info = IO.Txt.Dict_OldToNew(Info)
    IO.Txt.Write(Info, Dict)


#%% Update PRMs
PRMs = sorted(glob('/run/media/malfatti/Malfatti1TB4/Analysis/[Rec,Prev]*/*UnitRec/*/*prm'))

for PRMFile in PRMs:
    PRM = load_source('', PRMFile)
    Info = IO.Txt.Dict_OldToNew(PRM.DataInfo)
    PRM.DataInfo = Info

    Txt = []
    Attrs = [_ for _ in dir(PRM) if '__' not in _]
    for Attr in Attrs:
        if '__' in Attr: continue

        Txt.append(Attr + ' = ' + IO.Txt.Print(getattr(PRM, Attr))+'\n')
        Txt.append('\n')

    with open(PRMFile, 'w') as F:
        F.writelines(Txt)



#%% Add RawData to PRMs
Folders = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*UnitRec'))

for Folder in Folders:
    ExpFolder = os.environ['ANALYSISPATH']+'/'+'/'.join(Folder.split('/')[-2:]) + '/KlustaFiles'
    os.makedirs(ExpFolder, exist_ok=True)

    ExpName = Folder.split('/')[-2]
    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    ExpGroups = [Exps]
    if Folder+'/ExpGroups' in glob(Folder+'/*'):
        ExpGroups = IO.Txt.Read(Folder+'/ExpGroups')
        ExpGroups = [[Folder+'/'+_ for _ in F] for F in ExpGroups]

    for Es, Exps in enumerate(ExpGroups):
        FilesPrefix = ExpFolder+'/'+ExpName+'_Exp' + "{0:02d}".format(int(Es))

        raw_data_files = []
        DataInfo = glob(Folder + '/*.dict')[0]
        DataInfo = IO.Txt.Read(DataInfo)

        if 'Probe' in DataInfo.keys():
            if 'ChSpacing' in DataInfo['Probe']:
                ProbeSpacing = DataInfo['Probe']['ChSpacing']

            if DataInfo['Probe']['Remapped']:
                PrbFile = os.environ['SCRIPTSPATH'] + \
                          '/Python3/Klusta/A16-'+str(ProbeSpacing)+'.prb'
            else:
                PrbFile = FilesPrefix+'.prb'
        else:
            print('No probe info saved. Skip remap...')
            PrbFile = os.environ['SCRIPTSPATH'] + '/Python3/Klusta/A16-50.prb'

        for E, Exp in enumerate(Exps):
            FilesExt = [F.split('.')[-1] for F in glob(Exp+'/*.*')]

            ChNo, Procs = IO.OpenEphys.SettingsXML.GetRecChs(Exp+'/settings.xml')
            if len(ChNo) > 1:
                Proc = [K for K,V in Procs.items() if 'FPGA'in V][0]
            else:
                Proc = list(ChNo.keys())[0]
            ChNo = len(ChNo[Proc])


            if FilesExt == ['xml'] or 'dat' in FilesExt:
                Files = sorted(glob(Exp+'/**/*.dat', recursive=True))
                if 'experiment' in Files[0]:
                    Files = [_ for _ in Files if Proc in _.split('_')[-2]]
                raw_data_files += Files

                Rate = IO.OpenEphys.SettingsXML.GetSamplingRate(Exp+'/settings.xml')
                DType = 'int16'

            else:
                print('Loading', Exp, '...')
                Data, Rate = IO.DataLoader(Exp, 'Bits')
                # RecChMap = (np.arange(16)).tolist()+[DataInfo['DAqs']['StimCh']-1,DataInfo['DAqs']['TTLCh']-1]

                for R, Rec in sorted(Data[Proc].items(), key=lambda i: int(i[0])):
                    DataFile = ''.join([ExpName, '_Exp', "{0:02d}".format(int(Es)),
                                       '-', "{0:02d}".format(int(E)), '_Rec',
                                       "{0:02d}".format(int(R))])
                    RawDataInfo = {'Rate': Rate[Proc], 'Shape': Rec.shape, 'DType': str(Rec.dtype)}
                    raw_data_files.append(ExpFolder+'/'+DataFile+'.dat')

                ChNo = RawDataInfo['Shape'][1]
                Rate = int(RawDataInfo['Rate'])
                DType = RawDataInfo['DType']
                del(Data)

        DataInfo['RawData'] = Exps
        Klusta.PrmWrite(FilesPrefix+'.prm', ExpName+'_Exp' + "{0:02d}".format(int(Es)),
                        PrbFile, raw_data_files, Rate, ChNo, DType,
                        DataInfo=DataInfo)


#%% Rewrite wrong kwik data to binary (forgot to press '+' on OE)
a,b = IO.DataLoader('/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_10-51-52_PreventionControl_03-08_10Kwik', 'bits')

for P,Proc in a.items():
    for R,Rec in Proc.items():
        if int(R) < 5:
            f = '/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_10-51-52_PreventionControl_03-08_10'
        else:
            f = '/home/malfatti/Documents/PhD/Data/PreventionControl/20180124-PreventionControl_03-UnitRec/2018-01-24_10-51-52_PreventionControl_03-14_16'
            R = str(int(R)-5)

        print(Rec.dtype)
        print(f)
        print(int(R))
        print()

        IO.Bin.Write(Rec, f+'/experiment1_'+P+'_'+R+'.dat')



#%% Check TTLs
Folders = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-ABRs') + glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-UnitRec'))
PlotRange = np.arange(0,15000,6)

for F,Folder in enumerate(Folders):
    if Folder not in [
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180130-RecoveryControl_03-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_01-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_02-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_04-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_05-UnitRec'
    ]: continue

    ExpName = Folder.split('/')[-2]
    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    DataInfo = glob(Folder + '/*.dict')[0]
    DataInfo = IO.Txt.Read(DataInfo)
    # ChannelMap = DataInfo['DAqs']['RecCh']+[DataInfo['DAqs']['TTLCh']]
    # DataInfo['DAqs']['RecCh'] = list(np.arange(len(ChannelMap)-1)+1)
    # DataInfo['DAqs']['TTLCh'] = len(ChannelMap)

    for E, Exp in enumerate(Exps):
        if 'asal' in Exp or 'aselin' in Exp: continue
        if 'asal' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz'] or 'aselin' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']:
            continue

        print('Loading', Exp, '...')
        # Data, Rate = IO.DataLoader(Exp, 'Bits', ChannelMap)
        Data, Rate = IO.DataLoader(Exp, 'Bits')
        # Plot.AllCh(Data[P]['0'][PlotRange,:], lw=0.8, AxArgs={'title':Exp.split('/')[-2]+'_'+Exp.split('/')[-1].split('_')[-1]+'_'+P+'_'+R})


        for P,Proc in Data.items():
            if Data[P][list(Data[P].keys())[0]].shape[1] in [1,16]: continue

            for R,Rec in Proc.items():
                if R != '4': continue
                Rec = Rec.astype('float32')
                if Rec.shape[0] < max(PlotRange):
                    print('Rec', R, 'is too short, very likely it is broken.')
                    continue

                Freq = DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']
                print('Folder', F, 'Exp', E, 'Rec', R, 'Freq', Freq)

                ## Load Data file if present
                ABR = glob(Exp+'/Rec_'+R+'ABRs.ABRs')
                if len(ABR):
                    Rec[:,DataInfo['DAqs']['RecCh']] = IO.Bin.Read(ABR[0])[0]
                ## ====

                # TTLs = DataAnalysis.QuantifyTTLsPerRec(True, Rec[:,DataInfo['DAqs']['TTLCh']-1])
                TTLs = GetTTLs(Rec, Rate[P], DataInfo, Exp, True, R, Freq+'_'+"{0:02d}".format(int(E)))

                ## Check if rec is broken
                if type(TTLs) == str:
                    if TTLs == 'Broken': continue
                ## ====

                ## Visual double-check
                if len(TTLs):
                    RecTTLs = TTLs[(TTLs>PlotRange[0])*(TTLs<PlotRange[-1])]-PlotRange[0]
                else:
                    RecTTLs = [0]

                # if not len(RecTTLs):
                #     plt.plot(DataAnalysis.Normalize(Rec[:,DataInfo['DAqs']['RecCh']]))
                #     for TTL in TTLs: plt.axvspan(TTL, TTL+0.003*Rate[P], color='r', alpha=0.3)
                #     plt.plot(Rec[:,-1])
                #     plt.plot([0,len(Rec)], [-232]*2)
                #     plt.show()
                #     continue

                if not len(RecTTLs):
                    RecTTLs = [(_-TTLs[0])//6 for _ in TTLs[:6]]
                    RecN = DataAnalysis.Normalize(Rec[TTLs[0]:TTLs[6]:6,:])
                else:
                    RecTTLs = [_//6 for _ in RecTTLs]
                    RecN = DataAnalysis.Normalize(Rec[PlotRange,:])

                Colors = ['silver']*RecN.shape[1]
                if max([DataInfo['DAqs']['TTLCh']]+DataInfo['DAqs']['RecCh']) <= RecN.shape[1]:
                    Colors[DataInfo['DAqs']['TTLCh']-1] = 'b'
                    if 'StimCh' in DataInfo['DAqs']: Colors[DataInfo['DAqs']['StimCh']-1] = 'g'
                    Colors = [Color if C+1 not in DataInfo['DAqs']['RecCh'] else 'k' for C,Color in enumerate(Colors)]

                print('Data:', DataInfo['DAqs']['RecCh'])
                print('TTL:', DataInfo['DAqs']['TTLCh'])
                Fig, Ax = plt.subplots()

                for TTL in RecTTLs:
                    Ax.axvspan(TTL, TTL+0.003*Rate[P]//6, color='r', alpha=0.3)

                Ax = Plot.AllCh(RecN, lw=0.8, Ax=Ax,Colors=Colors,
                                AxArgs={'title':Exp.split('/')[-2]+'_'+Exp.split('/')[-1].split('_')[-1]+'_'+P+'_'+R})

                # SquareY = Ax.get_lines()[DataInfo['DAqs']['TTLCh']-1].get_ydata()
                # SquareY = [SquareY.min(), SquareY.max(), SquareY.max(), SquareY.min(), SquareY.min()]
                # SquareX = [0, 0, RecN.shape[0], RecN.shape[0], 0]
                # Ax.plot(SquareX, SquareY, color='silver', linestyle='--')

                plt.show()
                ## ====

                print(''); print('='*20); print('')

                ## Restore channels if changed
                # if ChangedCh:
                #     DataInfo['DAqs']['RecCh'], DataInfo['DAqs']['TTLCh'] = RecCh, TTLCh
                ## ====


# for TTL in TTLs: plt.axvspan(TTL, TTL+0.003*Rate[P], color='r', alpha=0.3)
# plt.plot(Rec[:,-1])
# plt.plot([0,len(Rec)], [-232]*2)
# plt.show()



#%% Find TTLs for RecControl
from DataAnalysis import ABRs

FilterFreq = [600,1500]
SampleStart = []

for F,Folder in enumerate([
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_01-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180201-RecoveryControl_02-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_04-UnitRec',
        '/home/malfatti/Documents/PhD/Data/RecoveryControl/20180202-RecoveryControl_05-UnitRec'
    ]):

    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    DataInfo = glob(Folder + '/*.dict')[0]
    DataInfo = IO.Txt.Read(DataInfo)

    for E, Exp in enumerate(Exps):
        if 'asal' in Exp or 'aselin' in Exp: continue
        if 'asal' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz'] or 'aselin' in DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']:
            continue

        print('Loading', Exp, '...')
        Data, Rate = IO.DataLoader(Exp, 'Bits')

        for P,Proc in Data.items():
            if Data[P][list(Data[P].keys())[0]].shape[1] in [1,16]: continue

            for R,Rec in Proc.items():
                # TTLs = GetTTLs(Rec, Rate[P], DataInfo, Exp, True, R, Freq+'_'+"{0:02d}".format(int(E)))
                TTLs = DataAnalysis.GenFakeTTLsRising(Rate[P], DataInfo['Audio']['SoundPulseDur'], DataInfo['Audio']['SoundPauseBeforePulseDur'], DataInfo['Audio']['SoundPauseAfterPulseDur'], 0, DataInfo['Audio']['SoundPulseNo'])
                ABR = ABRs.ABRPerCh(Rec, Rate[P], TTLs, DataInfo['DAqs']['RecCh'], [0, 0.1], FilterFreq, 4)

                #===
                Fig, Axes = plt.subplots(3,1,figsize=(10,10), sharex=True)

                Axes[0].plot(ABR, alpha=0.3)
                Axes[1].plot(abs(DataAnalysis.GetInstFreq(ABR, Rate[P])), alpha=0.3)
                Axes[2].plot(Rec[:ABR.shape[0],16:])

                Axes[1].set_ylim(FilterFreq)
                Fig.suptitle('Where is the click?')
                plt.show()
                #===

                SS = input('Sample where clicks start: ')
                SampleStart.append([F,E,R,int(SS)])


#%% Update NoTTLs.dict
Folders = sorted(glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-ABRs') + glob(os.environ['DATAPATH']+'/[Rec,Prev]*/*-UnitRec'))
NoTTLsFile = os.environ['NEBULAPATH']+'/Documents/PhD/Notes/Experiments/NoTTLRecs.dict'
NoTTLRecs = IO.Txt.Read(NoTTLsFile)
ToAddHz = []
WrongExpNo = []

for F,Folder in enumerate(Folders):
    Exps = sorted(glob(Folder+'/*20??-*'))
    Exps = [_ for _ in Exps if '.dict' not in _
                            and 'Impedance' not in _
                            and '.hdf5' not in _
                            and 'BigOne' not in _] # Temp override

    DataInfo = glob(Folder + '/*.dict')
    if len(DataInfo) > 1:
        WrongExpNo.append(Folder)
        continue
    else:
        DataInfo = DataInfo[0]

    DataInfo = IO.Txt.Read(DataInfo)
    if 'Hz' not in DataInfo['ExpInfo']['00'].keys():
        ToAddHz.append(Folder)
        continue

    if len(Exps) != len(DataInfo['ExpInfo']):
        WrongExpNo.append(Folder)
        continue

    for E, Exp in enumerate(Exps):
        Freq = DataInfo['ExpInfo']["{0:02d}".format(int(E))]['Hz']
        nF = DataInfo['InfoFile'].split('/')[-1].split('.')[0]

        if nF in NoTTLRecs:
            if Freq in NoTTLRecs[nF]:
                print('Exp', E, 'Rec', R, 'Freq', Freq)
                NoTTLRecs[nF][Freq+'_'+"{0:02d}".format(int(E))] = NoTTLRecs[nF].pop(Freq)

# IO.Txt.Write(NoTTLRecs, NoTTLsFile)
