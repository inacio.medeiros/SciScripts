#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 20181010
@author: ciralli
"""

import numpy as np                    # work with arrays
import os                             # system tasks
from glob import glob                 # Grab file names
from xml.etree import ElementTree     # Read XML files
from DataAnalysis import DataAnalysis # Use pre-written functions
from IO import IO                     # Read/write data from/to disk

# Work with plots
from DataAnalysis.Plot import Plot
from matplotlib import rcParams; rcParams.update({'backend': 'TkCairo'})
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt


def GetDataFromXMLs(File):
    """
    Function for getting tracking results
    """
    Data = {}
    CoordTree = ElementTree.parse(File)
    CoordRoot = CoordTree.getroot()

    Data['Coords'] = {}
    Animal = CoordRoot[2][0]
    if Animal.tag not in Data['Coords']: Data['Coords'][Animal.tag] = {}

    Keys = list(Animal[0].keys())
    Data['Coords'][Animal.tag] = {Key: np.array([F.get(Key) for F in Animal], dtype='float32')
                                  for Key in Keys if Key != 't'}

    Data['t'] = np.array([F.get('t') for F in Animal], dtype='float32')

    return(Data)


def GetEucDist(X, Y):
    """
    Function to calculate the distance between two spatial points
    """
    Dist = ((X[1:] - X[:-1])**2 + (Y[1:] - Y[:-1])**2)**0.5
    return(Dist)


def PolyArea(X,Y):
    """
    Author: Madhi
    Source: https://stackoverflow.com/a/30408825
    """
    Area = 0.5 * np.abs(np.dot(X, np.roll(Y,1)) - np.dot(Y, np.roll(X,1)))
    return(Area)


#%%
## Parameters
Groups = {
    # 'WT': [36273, 36018, 40065, 40061, 40067, 40162],
    # 'KO': [36280, 36237, 36257, 36458, 40055, 40056, 40153]
    'WT': [40065],
    # 24683 - Only ctr recordings
    # 24673 - No traccking available
    # 36537 - Opto stimulation in the middle of the treatment
}
# Doses = ['ctr', '5mg', '20mg']
Doses = ['sal', 'dmt']

TrackingsPath = os.environ['ANALYSISPATH']+'/KiaData/Locomotion'
ArenaSize = [27, 16]
IMGSize = [800,600]
FPS = 15
CornerThreshold = 5             # Should be the same unit of (ArenaSize[0]*ArenaSize[1])
PixelSize = ArenaSize[1]/317    # 317 is the size in pixels of the box in the video


## Compute data
# Organize data in groups and treatments
# XMLFiles = glob(TrackingsPath+'/**/*.xml', recursive=True)
XMLFiles = glob(TrackingsPath+'/400*/**/*.xml', recursive=True)
XMLFiles = [_ for _ in XMLFiles if 'xml tracking backup' not in _]

XMLFiles = {G: [File for Animal in Group for File in XMLFiles if str(Animal) in File]# if '_non' in File]
			for G,Group in Groups.items()}

XMLFiles = {G: {Dose: [File for File in Group if Dose in File.lower()]
				for Dose in Doses} for G, Group in XMLFiles.items()}

XMLFiles = {G: {D: {str(Animal): sorted([File for File in Dose if str(Animal) in File.lower()])
                    for Animal in Groups[G]}
                for D,Dose in Group.items()}
            for G, Group in XMLFiles.items()}

for G, Group in XMLFiles.items():
    for D,Dose in Group.items():
        for A,Animal in Dose.items():
            try:
                Sets = sorted(np.unique([_.split('/')[-1].split('.')[0].split('_')[-2]
                                         for _ in Animal]
                              ), key=lambda i: int(i)
                       )
            except Exception as e:
                print(e)
                XMLFiles[G][D][A] = [Animal]
                continue

            Order = [[_ for _ in XMLFiles[G][D][A]
                      if _.split('/')[-1].split('.')[0].split('_')[-2] == Set]
                     for Set in Sets]

            Order = [sorted(_, key=lambda i: int(i.split('/')[-1].split('.')[0].split('_')[-1])
                            if 'Converted' not in i else
                            int(i.split('/')[-1].split('.')[0].split('_')[-1].split('-')[0]))
                     for _ in Order]

            for O in range(len(Order)):
                if True in [True if '2nd' in _ else False for _ in Order[O]]:
                    SecondSet = [_ for _ in Order[O] if '2nd' in _]
                    Order.append(SecondSet)
                    for S in SecondSet:
                        del(Order[O][Order[O].index(S)])

            XMLFiles[G][D][A] = Order


AllAnimals = {G: {D: {str(A): [] for A in Groups[G]} for D in Doses} for G in Groups.keys()}
Errors = {G: {D: {str(A): [] for A in Groups[G]} for D in Doses} for G in Groups.keys()}

for G,Group in XMLFiles.items():
    for D,Dose in Group.items():
        for A,Animal in Dose.items():
            for S, Set in enumerate(Animal):
                X = np.array([], dtype='float32')
                Y = np.array([], dtype='float32')
                t = np.array([], dtype='float32')

                for F, File in enumerate(Set):
                    try:
                        Data = GetDataFromXMLs(File)
                    except Exception as e:
                        Errors[G][D][A].append([S, F, e])
                        print([F, e])
                        continue

                    if Data['t'].shape[0] < 150:
                        Errors[G][D][A].append([S, F, 'Too short'])
                        print([F, Data['t'].shape[0], 'Too short'])
                        continue

                    Xf = Data['Coords']['MOUSEA']['bodyx'] * PixelSize
                    Yf = Data['Coords']['MOUSEA']['bodyy'] * PixelSize
                    X = np.concatenate((X, Xf))
                    Y = np.concatenate((Y, Yf))

                    Offset = t[-1] + 1/FPS if len(t) else 0
                    tf = (Data['t']/FPS) + Offset
                    t = np.concatenate((t, tf))

                if not len(t): continue
                Distance = GetEucDist(X, Y)
                Area = PolyArea(X, Y)
                AllAnimals[G][D][A].append({'X':X, 'Y':Y, 't':t,
                                            'Distance':Distance, 'Area': Area})

for G in Groups.keys():
    for D in Doses:
        for A in Groups[G]:
            A = str(A)
            for Error in Errors[G][D][A]:
                del(XMLFiles[G][D][A][Error[0]][Error[1]])

AllAnimals['WT']['sal']['40065'] = [AllAnimals['WT']['sal']['40065'][1]]
AllAnimals['WT']['dmt']['40065'] = [AllAnimals['WT']['dmt']['40065'][5]]

MeanDistances = [
    [AllAnimals[G][D][str(A)][-1]['Distance'].sum()
     for A in Groups[G] if len(AllAnimals[G][D][str(A)])]
    for D in Doses for G in Groups
]

MeanDistancesNoCorners = [
    [AllAnimals[G][D][str(A)][-1]['Distance'].sum()
     for A in Groups[G] if len(AllAnimals[G][D][str(A)])
                        and AllAnimals[G][D][str(A)][0]['Area'] > CornerThreshold]
    for D in Doses for G in Groups
]

MeanDistancesCorners = [
    [AllAnimals[G][D][str(A)][-1]['Distance'].sum()
     for A in Groups[G] if len(AllAnimals[G][D][str(A)])
                        and AllAnimals[G][D][str(A)][0]['Area'] < CornerThreshold]
    for D in Doses for G in Groups
]

Areas = [
    [AllAnimals[G][D][str(A)][-1]['Area']
     for A in Groups[G] if len(AllAnimals[G][D][str(A)])]
    for G in Groups for D in Doses
]

CornerNo = [len([_ for _ in A if _ < CornerThreshold]) for A in Areas]
NotCornerNo = [len(A) - CornerNo[a] for a,A in enumerate(Areas)]

# # Check incomplete recs
# for G,Group in Groups.items():
#     for Animal in Group:
#         print(Animal)
#         for D in Doses:
#             print(len(AllAnimals[G][D][str(Animal)]))
#             print([G] + [AllAnimals[G][D][str(Animal)][0]['Distance'].sum()])


TxtData = [['Name', 'Condition', 'SumDist0mg/kg', 'SumDist5mg/kg', 'SumDist20mg/kg', 'Area0mg/kg', 'Area5mg/kg', 'Area20mg/kg']] + \
          [[str(Animal), G] +
           # ['Corner' if AllAnimals[G][D][str(Animal)][0]['Area'] < CornerThreshold else 'NotCorner'] +
           [str(AllAnimals[G][D][str(Animal)][-1]['Distance'].sum()) for D in Doses] +
           [str(AllAnimals[G][D][str(Animal)][-1]['Area']) for D in Doses]
           for G,Group in Groups.items() for Animal in Group]

with open('TotalDistance.txt', 'w') as F:
    for L in TxtData: F.write('; '.join(L)+'\n')



#%%
## Plot animal position
for G,Group in AllAnimals.items():
    Fig, Axes = plt.subplots(len(Groups[G]), len(Doses), figsize=(4.5,6))

    for AI, Animal in enumerate(Groups[G]):
        A = str(Animal)
        tList = [Set['t'].shape[0] for Dose in Group.values() for Set in Dose[A]]
        if not len(tList): continue

        Min = min(tList)

        for d,(D,Dose) in enumerate(Group.items()):
            if not Dose[A]:
                Axes[AI, d].text(0.4, 0.4, 'Not found')
                continue

            F = len(Dose[A])//2
            Axes[AI, d].plot(Dose[A][F]['X'][:Min], Dose[A][F]['Y'][:Min], 'k.--', ms=2, lw=0.8)
            Axes[AI, d].set_xlim([0, 800 * PixelSize])
            Axes[AI, d].set_ylim([0, 600 * PixelSize])
            # Axes[AI, d].set_title(G+', '+ A + ', ' + D)
            Title = D if D != 'ctr' else 'Control'
            if not AI: Axes[AI, d].set_title(Title)
            # Axes[AI, d].legend()

        Axes[AI, 0].set_ylabel(A)

    for AxL in Axes:
        for Ax in AxL:
            # Plot.Set(Ax=Ax)
            for Side in ['right', 'left', 'bottom', 'top']:
                Ax.spines[Side].set_visible(True)
                Ax.spines[Side].set_linewidth(1)

            Ax.set_xticks([])
            Ax.set_yticks([])
            Ax.set_xticklabels('')
            Ax.set_yticklabels('')
            Ax.set_aspect(0.75)

    Plot.Set(Fig=Fig)
    Fig.subplots_adjust(hspace=0, wspace=0)
    Fig.savefig('Ov'+G+'.pdf')
    plt.close()


#%% Kia
NormNaCl = '/home/ciralli/Documents/PhD/Analysis/40065-NaCl-Norm.dat'
NormDMT = '/home/ciralli/Documents/PhD/Analysis/40065-DMT-Norm.dat'
NormNaCl = IO.Bin.Read(NormNaCl)[0]
NormDMT = IO.Bin.Read(NormDMT)[0]
X = (np.arange(NormNaCl.shape[0])/25000)-0.5

for G,Group in AllAnimals.items():
    Fig = plt.figure()
    Axes = [
        plt.subplot2grid((3,3), (0,0)),
        plt.subplot2grid((3,3), (0,1)),
        plt.subplot2grid((3,3), (0,2)),
        plt.subplot2grid((3,3), (1,0), colspan=3),
        plt.subplot2grid((3,3), (2,0), colspan=3)
    ]

    for AI, Animal in enumerate(Groups[G]):
        A = str(Animal)
        tList = [Set['t'].shape[0] for Dose in Group.values() for Set in Dose[A]]
        if not len(tList): continue

        Min = min(tList)

        for d,(D,Dose) in enumerate(Group.items()):
            if not Dose[A]:
                Axes[d].text(0.4, 0.4, 'Not found')
                continue

            F = len(Dose[A])//2
            Axes[d].plot(Dose[A][F]['X'][:Min], Dose[A][F]['Y'][:Min], 'k.--', ms=2, lw=0.8)
            Axes[d].set_xlim([0, 800 * PixelSize])
            Axes[d].set_ylim([0, 600 * PixelSize])
            print(G, A, D, F)
            # Axes[d].set_title(G+', '+ A + ', ' + D)
            if D == 'ctr':
                Title = 'Control'
            elif D == 'sal':
                Title = 'NaCl'
            elif D == 'dmt':
                Title = 'DMT'
            else:
                Title = D

            if not AI: Axes[d].set_title(Title)
            # Axes[d].legend()

        MDs = [_[0] for _ in MeanDistances]
        Axes[2].bar(range(len(MDs)), MDs, facecolor='k', width=0.3)
        Axes[2].set_xticks(range(len(MDs)))
        Axes[2].set_xticklabels(['NaCl', 'DMT'])
        Axes[0].set_yticks([0,10,20,30])
        Axes[1].set_yticks([0,10,20,30])
        Axes[2].set_yticks([0,200,400,600,800])

        Axes[0].set_ylabel(A)

    Axes[3].plot(X, NormNaCl.mean(axis=1), 'k')
    Axes[4].plot(X, NormDMT.mean(axis=1), 'k')

    for Ax in Axes[:2]:
        Ax.set_aspect(IMGSize[1]/IMGSize[0])

    AR = [max(Axes[2].get_xlim()), max(Axes[2].get_ylim())]
    Axes[2].set_aspect((AR[0]/AR[1])*0.7)

    Axes[0].set_xlabel('Distance [cm]')
    Axes[1].set_xlabel('Distance [cm]')
    Axes[0].set_ylabel('Distance [cm]')
    Axes[1].set_ylabel('')
    Axes[0].set_ylim([0,30])
    Axes[1].set_ylim([0,30])
    Axes[2].set_ylim([0,800])

    for Ax in Axes[3:]:
        Ax.set_xlim([-0.5,1])
        Ax.set_ylim([-1,1])
        Ax.set_xlabel('Time [s]')
        Ax.set_ylabel('Amplitude [Norm.]')

    Axes[3].set_title('NaCl')
    Axes[4].set_title('DMT')

    for Ax in Axes: Plot.Set(Ax=Ax)

    # Axes[2].set_ylabel('Total Distance [cm]')
    # Axes[1].set_yticks([])
    Axes[1].set_yticklabels('')
    Plot.Set(Fig=Fig)
    # Fig.subplots_adjust(hspace=0, wspace=0)
    Fig.savefig('Ov'+G+'-DMT.pdf')
    # Fig.tight_layout()
    plt.show()


#%%
with PdfPages('Overview.pdf') as PDF:
    Count = 0; D = ''
    Fig, Axes = plt.subplots(3, len(Doses), figsize=(8.27, 11.69))
    for G,Group in AllAnimals.items():
        for Animal in Groups[G]:
            A = str(Animal)

            if Count > 2:
                for AxL in Axes:
                    for Ax in AxL: Plot.Set(Ax=Ax)

                Plot.Set(Fig=Fig)
                PDF.savefig()
                plt.close()

                Count = 0
                Fig, Axes = plt.subplots(3, len(Doses), figsize=(8.27, 11.69))

            tList = [Set['t'].shape[0] for Dose in Group.values() for Set in Dose[A]]
            if not len(tList): continue

            Min = min(tList)

            for d,(D,Dose) in enumerate(Group.items()):
                if not Dose[A]:
                    Axes[Count, d].text(0.4, 0.4, 'Not found')
                    continue

                F = len(Dose[A])//2
                Axes[Count, d].plot(Dose[A][F]['X'][:Min], Dose[A][F]['Y'][:Min], 'k.--', label=str(Dose[A][F]['Area']))
                Axes[Count, d].set_xlim([0, 800 * PixelSize])
                Axes[Count, d].set_ylim([0, 600 * PixelSize])
                Axes[Count, d].set_title(G+', '+ A + ', ' + D)
                Axes[Count, d].legend()

            Count += 1
plt.close()


## Plot total distance per group and treatment
# Fig, Ax = plt.subplots()
# Ax.boxplot(MeanDistances, positions=[1,2,4,5,7,8], showmeans=True)
# Ax.set_xticklabels([G+'\n            '+D.capitalize() if G == 'WT' else G for D in Doses for G in Groups])
# Ax.set_ylabel('Mean distance [cm]')
# Plot.Set(Ax=Ax, Fig=Fig)
# Fig.savefig('AllRecs.pdf')
# plt.show()


## Plot total distance per group and treatment
Fig, Ax = plt.subplots()
Ax.boxplot(MeanDistances, positions=[1,2,4,5,7,8], showmeans=True)

y = MeanDistancesNoCorners[0]
x = [1]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='k')

y = MeanDistancesNoCorners[1]
x = [2]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='k')

y = MeanDistancesNoCorners[2]
x = [4]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='k')

y = MeanDistancesNoCorners[3]
x = [5]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='k')

y = MeanDistancesNoCorners[4]
x = [7]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='k')

y = MeanDistancesNoCorners[5]
x = [8]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='k')

#corner
y = MeanDistancesCorners[0]
x = [1]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='b')

y = MeanDistancesCorners[1]
x = [2]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='b')

y = MeanDistancesCorners[2]
x = [4]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='b')

y = MeanDistancesCorners[3]
x = [5]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='b')

y = MeanDistancesCorners[4]
x = [7]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='b')

y = MeanDistancesCorners[5]
x = [8]*len(y)
x = np.array(x, dtype='float')
x += np.random.uniform(-0.1, 0.1, len(x))
Ax.scatter(x, y, color='b')

Ax.set_xticklabels([G+'\n            '+D.capitalize() if G == 'WT' else G for D in Doses for G in Groups])
Ax.set_ylabel('Total distance [cm]')
Plot.Set(Ax=Ax, Fig=Fig)
Fig.savefig('AllRecsNoCorners.pdf')
plt.show()


## Plot perc. of corner behaviour
Fig, Ax = plt.subplots()
Ax.bar([0,1,2,4,5,6], CornerNo, label='Corner')
Ax.bar([0,1,2,4,5,6], NotCornerNo, bottom=CornerNo, label='Not corner')
Ticks = [D+'\n'+G if D == Doses[1] else D.capitalize() for G in Groups for D in Doses]
Ax.set_xticks([0,1,2,4,5,6])
Ax.set_xticklabels(Ticks)
Ax.set_ylabel('No of animals')
Ax.legend()
Plot.Set(Ax=Ax, Fig=Fig)
Fig.savefig('CornerBehaviour.pdf')
plt.show()
#%%traces locomotion each animal 0, 5mg and 20mg

from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')

X = [0,1,2]
A36273= [215.2302, 86.54518, 166.32593]
A36018= [112.9845, 34.07773, 31.64782]
A40065= [44.881905, 118.77971, 548.2522]
A40061= [327.51892, 49.50312, 41.2821]
A40067= [109.69824, 76.50558, 364.57758]
A40162= [216.29506, 85.259636, 412.1202]
A36280= [1451.0994, 307.89398, 669.7782]
A36237= [168.0271, 2530.3135, 46.76812]
A36257= [295.0558, 14.548601, 110.40417]
A36458= [249.82907, 8.550301, 69.315125]
A40055= [256.64835, 45.24095, 584.21936]
A40056= [0.5740344, 469.5629, 228.95139]
A40153= [107.12377, 74.735855, 9.493479]


Fig, Ax = plt.subplots(2,1)
Ax[0].plot(X, A36273, '.--')
Ax[0].plot(X, A36018, '.--')
Ax[0].plot(X, A40065, '.--')
Ax[0].plot(X, A40061, '.--')
Ax[0].plot(X, A40067, '.--')
Ax[0].plot(X, A40162, '.--')
Ax[1].plot(X, A36280, '.--')
Ax[1].plot(X, A36237, '.--')
Ax[1].plot(X, A36257, '.--')
Ax[1].plot(X, A36458, '.--')
Ax[1].plot(X, A40055, '.--')
Ax[1].plot(X, A40056, '.--')
Ax[1].plot(X, A40153, '.--')

Ax[0].set_xticks(X)
Ax[1].set_xticks(X)
Ax[0].set_xticklabels(['0mg/Kg', '5mg/Kg', '20mg/Kg'])
Ax[1].set_xticklabels(['0mg/Kg', '5mg/Kg', '20mg/Kg'])
Ax[0].set_title('SLC10A4+/+')
Ax[1].set_title('SLC10A4-/-')
Fig.savefig('LocomotionEach.pdf')
Fig.subplots_adjust(hspace=0.6)
plt.show()

#%% Neural data
def GetPSD(Data, Rate, WindowSize):
    DataPSD = []
    for C in range(Data.shape[1]):
        F, Sxx = DataAnalysis.PSD(Data[:,C], Rate, WindowSize=int(WindowSize*Rate))

        if not len(DataPSD): DataPSD = np.zeros((Sxx.shape[0], Data.shape[1]))
        DataPSD[:,C] = Sxx

    return(F, DataPSD)


def GetSxx(Data, Rate, Window):
    DataSxx = []
    for C in range(Data.shape[1]):
        Fxx, txx, Sxx = DataAnalysis.Spectrogram(Data[:,C], Rate, int(Window*Rate), Overlap=int(Window*Rate)//1.25)
        if not len(DataSxx):
            DataSxx = np.zeros((Fxx.shape[0], txx.shape[0], Data.shape[1]), Data.dtype)

        DataSxx[:,:,C] = Sxx

    return(Fxx, txx, DataSxx)


def GetTFD(DataSxx, DataFxx, FreqBands):
    DataTFD = {F: np.zeros((DataSxx.shape[1], DataSxx.shape[2]))
               for F in FreqBands}

    for F, Freq in FreqBands.items():
        FreqFxx = (DataFxx > Freq[0]) * (DataFxx < Freq[1])
        DataTFD[F] = DataSxx[FreqFxx,:,:].mean(axis=0)

    return(DataTFD)


def PlotTFD(TFD):
    Fig, Axes = plt.subplots(Data.shape[1], 1, sharex=True, figsize=(7,7))
    for C in range(Data.shape[1]):
        for F,Freq in TFD.items():
            Axes[C].plot(Freq[:,C], label=F)

        Axes[C].legend()
    plt.show()



## Parameters
Groups = {
    'WT': [24683, 36273, 36018, 40065, 40061, 40067, 40162],
    'KO': [24673, 36280, 36237, 36257, 36458, 36537, 40055, 40056, 40153]
}
Doses = ['ctr', '5mg', '20mg']

IntanPath = os.environ['DATAPATH']+'/KiaData'
AnalysisPath = os.environ['ANALYSISPATH']+'/KiaData/ERPs/BinFiles'
FreqBands = {'Theta': [4, 12], 'Gamma': [30, 80]}
FilterFreq = [60]
ERPWindow = 2
SxxWindow = 0.5
ChannelMap = [4,3,2,1,5,6,7,8,9,10,11,12,16,15,14,13]

Doses = ['sal', '5mg', '20mg']

IntanFiles = glob(IntanPath+'/**/*.int', recursive=True)
IntanFiles = {G: [File for Animal in Group for File in IntanFiles if str(Animal) in File]
        		      for G,Group in Groups.items()}

IntanFiles = {G: {Dose: [File for File in Group if Dose in File.lower()]
				  for Dose in Doses} for G, Group in IntanFiles.items()}

IntanFiles = {G: {D: {str(Animal): sorted([File for File in Dose if str(Animal) in File.lower()])
                      for Animal in Groups[G]}
                  for D,Dose in Group.items()}
              for G, Group in IntanFiles.items()}

for G, Group in IntanFiles.items():
    for D,Dose in Group.items():
        for A,Animal in Dose.items():
            if not len(Animal): continue
            Sets = [0, 0]
            Sets[0] = sorted([_ for _ in Animal if '_rep_' not in _.split('/')[-1]
                                             and '2nd' not in _.split('/')[-1]
                                             and '8Hz' not in _.split('/')[-1]
                                             and '_rep' not in _.split('/')[-2]])
            Sets[1] = sorted([_ for _ in Animal if '_rep_' in _.split('/')[-1]
                                            or '2nd' in _.split('/')[-1]
                                            or '8Hz' in _.split('/')[-1]
                                            or '_rep' in _.split('/')[-2]])

            if not Sets[1]: del(Sets[1])
            IntanFiles[G][D][A] = Sets



for G in Groups.keys(): IntanFiles[G]['ctr'] = IntanFiles[G].pop('sal')
Doses = ['ctr', '5mg', '20mg']

for G, Group in IntanFiles.items():
    for D, Dose in Group.items():
        for A, Animal in Dose.items():
            for S, Set in enumerate(Animal):
                for FileInd,File in enumerate(Set):
                    if 'tetrode' in File: continue
                    if File == os.environ['DATAPATH']+'/KiaData/2nd batch/22 jul 13/m40055_rep/m40055_rep_click_5mg_130722_124026.int':
                        continue

                    DataPath = '_'.join([G, D, A, "{0:02d}".format(S)])
                    FilePath = "{0:02d}".format(FileInd)
                    os.makedirs(AnalysisPath+'/'+DataPath, exist_ok=True)
                    Done = glob(AnalysisPath+'/'+DataPath+'/'+FilePath+'/*')
                    Done = sorted([_.split('/')[-1] for _ in Done if '.dict' not in _])
                    if not False in [_ in Done for _ in [
                               'DataPSD', 'DataFxx.dat', 'Datatxx.dat',
                               'DataSxx.dat', 'DataTFD', 'ERPs.dat',
                               'ERPFxx.dat', 'ERPtxx.dat', 'ERPSxx.dat',
                               'ERPTFD', 'ERPMeanFxx.dat', 'ERPMeantxx.dat',
                               'ERPMeanSxx.dat', 'ERPMeanTFD', 'MeanPower']
                    ]:
                        print(DataPath+'_'+FilePath, 'already done. Skipping...')
                        continue


                    Data, Rate = IO.DataLoader(File, ChannelMap=ChannelMap)
                    ChNo = Data.shape[1]-1
                    AllData = {FilePath: {}}

                    ##== Data power spectrum density (PSD) ==##
                    if 'DataPSD' not in Done:
                        DataPSD = {
                            Freq: GetPSD(
                                DataAnalysis.FilterSignal(
                                    Data[:,:-1], Rate, Band, FilterOrder=2
                                ),
                                Rate, ERPWindow
                            )
                            for Freq, Band in FreqBands.items()
                        }

                        DataPSD = {F: {'F': Freq[0], 'PSD': Freq[1]} for F,Freq in DataPSD.items()}
                        AllData[FilePath]['DataPSD'] = DataPSD


                    # ##== Data spectrogram (Sxx) and time-frequency decomposition (TFD) ==##
                    if False in [_ in Done for _ in [
                            'DataFxx.dat', 'Datatxx.dat',
                            'DataSxx.dat', 'DataTFD']
                    ]:
                        DataFxx, Datatxx, DataSxx = GetSxx(Data[:,:-1], Rate, SxxWindow)
                        DataTFD = GetTFD(DataSxx, DataFxx, FreqBands)
                        AllData[FilePath]['DataFxx'] = DataFxx
                        AllData[FilePath]['Datatxx'] = Datatxx
                        AllData[FilePath]['DataSxx'] = DataSxx
                        AllData[FilePath]['DataTFD'] = DataTFD


                    if '_non' not in File:
                        # Get TTL edges
                        TTLs = DataAnalysis.QuantifyTTLsPerRec(True, Data[:,-1])
                        if not len(TTLs) and Data.shape[1] < 18: continue
                        TTLsFall = DataAnalysis.QuantifyTTLsPerRec(True, Data[:,-1], Edge='fall')

                        # Calculate click duration
                        ClickDur = (TTLsFall[0] - TTLs[0])/Rate

                        # Get number of trials
                        TrialNo = len(TTLs)//2

                        ##== ERPs ==##
                        if 'ERPs.dat' not in Done:
                            # Filter signal for ERPs
                            DataERPs = DataAnalysis.FilterSignal(Data[:,:-1], Rate, FilterFreq, Type='lowpass')

                            # Slice data around each 1st click for each channel
                            ERPs = np.zeros((int(Rate+Rate*ERPWindow*2), TrialNo, ChNo), DataERPs.dtype)
                            for Ch in range(ChNo):
                                ERPs[:,:,Ch] = DataAnalysis.SliceData(DataERPs[:,Ch], TTLs[::2], ERPWindow*Rate, Rate+ERPWindow*Rate, True)

                            del(DataERPs)
                            AllData[FilePath]['ERPs'] = ERPs

                        else:
                            ERPs = IO.Bin.Read(AnalysisPath+'/'+DataPath+'/'+FilePath+'/ERPs.dat')

                        # Take the mean of all trials
                        ERPMean = ERPs.mean(axis=1)


                        ##== Phase reversal ==##
                        if 'Channels.dat' not in Done:
                            # Create time window for calculating signal phase
                            PhaseWindow = np.arange(ERPWindow*Rate+int(Rate*0.01), ERPWindow*Rate+int(Rate*0.05))

                            # Apply phase window to the mean of ERPs
                            ERPMeanNorm = ERPMean[PhaseWindow,:]

                            # Normalize all channels
                            for Ch in range(ChNo):
                                ERPMeanNorm[:,Ch] = \
                                    (ERPMeanNorm[:,Ch] - ERPMeanNorm[:,Ch].mean()) / \
                                    (ERPMeanNorm[:,Ch].max() - ERPMeanNorm[:,Ch].mean())

                            # Get phase difference between adjacent channels
                            DeltaPhases = np.zeros((ERPMeanNorm.shape[0], ChNo-1))
                            for P, Pair in enumerate(DataAnalysis.Pairwise(range(ChNo))):
                                DeltaPhases[:,P] = DataAnalysis.GetDeltaPhase(
                                    ERPMeanNorm[:,Pair[0]], ERPMeanNorm[:,Pair[1]]
                                )

                            # Get the two channels with the greatest phase changes
                            PhaseInv = (DeltaPhases**2).mean(axis=0)**0.5
                            PhaseInv = sorted([
                                PhaseInv.argmax()+1,
                                np.hstack((PhaseInv[:PhaseInv.argmax()], [0],
                                           PhaseInv[PhaseInv.argmax()+1:])).argmax()
                            ])

                            Channels = np.arange(PhaseInv[0], PhaseInv[1]+1)


                        ##== Spectrogram of the mean of ERPs ==##
                        if False in [_ in Done for _ in [
                            'ERPMeanFxx.dat', 'ERPMeantxx.dat',
                            'ERPMeanSxx.dat', 'ERPMeanTFD']
                        ]:
                            ERPMeanFxx, ERPMeantxx, ERPMeanSxx = GetSxx(ERPMean, Rate, SxxWindow)
                            ERPMeanTFD = GetTFD(ERPMeanSxx, ERPMeanFxx, FreqBands)

                            AllData[FilePath]['ERPMeanFxx'] = ERPMeanFxx
                            AllData[FilePath]['ERPMeantxx'] = ERPMeantxx
                            AllData[FilePath]['ERPMeanSxx'] = ERPMeanSxx
                            AllData[FilePath]['ERPMeanTFD'] = ERPMeanTFD

                        else:
                            ERPMeanSxx = IO.Bin.Read(AnalysisPath+'/'+DataPath+'/'+FilePath+'/ERPMeanSxx.dat')


                        ##== Spectrogram of each ERP trial ==##
                        if False in [_ in Done for _ in [
                            'ERPFxx.dat', 'ERPtxx.dat',
                            'ERPSxx.dat', 'ERPTFD']
                        ]:
                            ERPSxx = []
                            for T in range(TrialNo):
                                ERPFxx, ERPtxx, Sxx = GetSxx(ERPs[:,T,:], Rate, SxxWindow)

                                if not len(ERPSxx):
                                    ERPSxx = np.zeros((ERPFxx.shape[0], ERPtxx.shape[0],
                                                       TrialNo, ChNo), Data.dtype)

                                ERPSxx[:,:,T,:] = Sxx


                            ##== TFD of each ERP trial ==##
                            ERPTFD = {F: np.zeros((ERPSxx.shape[1], TrialNo, ChNo))
                                        for F in FreqBands}

                            for T in range(TrialNo):
                                TFD = GetTFD(ERPSxx[:,:,T,:], ERPFxx, FreqBands)

                                for F, Freq in TFD.items(): ERPTFD[F][:,T,:] = Freq

                            AllData[FilePath]['ERPFxx'] = ERPFxx
                            AllData[FilePath]['ERPtxx'] = ERPtxx
                            AllData[FilePath]['ERPSxx'] = ERPSxx
                            AllData[FilePath]['ERPTFD'] = ERPTFD

                        else:
                            ERPFxx = IO.Bin.Read(AnalysisPath+'/'+DataPath+'/'+FilePath+'/ERPFxx.dat')
                            ERPSxx = IO.Bin.Read(AnalysisPath+'/'+DataPath+'/'+FilePath+'/ERPSxx.dat')


                        ##== Calculate the mean power for the evoked and the induced ERPs for each freq band ==##
                        if 'MeanPower' not in Done:
                            MeanPower = {'Evoked': {}, 'Induced': {}}
                            for F, Freq in FreqBands.items():
                                FreqFxx = (ERPFxx > Freq[0]) * (ERPFxx < Freq[1])
                                MeanPower['Evoked'][F] = ERPSxx[FreqFxx,:,:,:].mean(axis=2).mean(axis=1).mean(axis=0)
                                MeanPower['Induced'][F] = ERPMeanSxx[FreqFxx,:,:].mean(axis=1).mean(axis=0)

                            AllData[FilePath]['MeanPower'] = MeanPower

                    if len(AllData[FilePath]):
                        IO.Bin.Write(AllData, AnalysisPath + '/' + DataPath)
                    del(AllData)



#%%
Rate = 25000
AsdfFiles = sorted(glob(os.environ['ANALYSISPATH']+'/KiaData/ERPs/AllData/*5mg*.asdf'))
Animals = np.unique([F.split('/')[-1].split('_')[2] for F in AsdfFiles])

with PdfPages('ERPOverview-5mg.pdf') as PDF:
    for Animal in Animals:
        Files = [F for F in AsdfFiles if Animal in F]
        Files = [[F for F in Files if F.split('/')[-1].split('_')[3] == S] for S in np.unique([F.split('/')[-1].split('_')[3] for F in Files])]

        for Set in Files:
            ERPs = []
            for File in Set:
                print('Loading file', File, '...')
                All = IO.Asdf.Read(File)
                if 'ERPs' not in All.keys(): continue
                if not len(ERPs): ERPs = All['ERPs'].copy()
                else:
                    ERPs = np.concatenate((ERPs, All['ERPs']), axis=1)
                del(All)

            print('Done loading set', Set[0].split('/')[-1].split('.')[0])
            if not len(ERPs): continue
            ERPs = ERPs.mean(axis=1)

            Spaces = Plot.GetSpaces(ERPs)
            Fig, Axes = plt.subplots(1, 2, figsize=(8.27, 11.69))
            for Ax in Axes:
                YTicks = []
                for Ch in range(ERPs.shape[1]):
                    X = (np.arange(ERPs.shape[0])/Rate) - ERPWindow
                    Y = ERPs[:,Ch] + Spaces[Ch]
                    YTicks.append(Y.mean())
                    Ax.plot(X, Y, 'k')

                Lims = Ax.get_ylim()
                Ax.plot([0]*2, [max(Lims), min(Lims)], 'r--')
                Ax.plot([0.5]*2, [max(Lims), min(Lims)], 'r--')
                Ax.set_yticks(YTicks)
                Ax.set_yticklabels(np.arange(ERPs.shape[1])+1)
                Ax.set_xlabel('Time [s]')

            Axes[0].set_xlim([-0.01, 0.2])
            Axes[1].set_xlim([0.49, 0.7])
            Axes[1].set_xticklabels([round(_,3) for _ in Axes[0].get_xticks()])
            Axes[0].set_title('1st click')
            Axes[1].set_title('2nd click')
            Axes[0].set_ylabel('Channel')
            Fig.suptitle(Set[0].split('/')[-1].split('.')[0].replace('_', ', '))
            PDF.savefig()
            plt.close()

plt.close()


#%%
Rate = 25000
Doses = ['ctr', '5mg', '20mg']
Colors = ['k', 'b', 'r']
Files = sorted(glob(os.environ['ANALYSISPATH']+'/KiaData/ERPs/BinFiles/*/*'))
NonFiles = [_ for _ in Files if not glob(_+'/ERPTFD')]
ClickFiles = [_ for _ in Files if glob(_+'/ERPTFD')]
Animals = np.unique([_.split('/')[-2].split('_')[2] for _ in Files])

# with PdfPages('SxxNonClickOverview.pdf') as PDF:
#     Count = 0; D = ''
#     Fig, Axes = plt.subplots(3, len(Doses), figsize=(8.27, 11.69))

#     for Animal in Animals:
#         AnimalFiles = [_ for _ in NonFiles if Animal in _]

#         if Count > 2:
#             for AxL in Axes:
#                 for Ax in AxL: Plot.Set(Ax=Ax)

#             Plot.Set(Fig=Fig)
#             PDF.savefig()
#             plt.close()

#             Count = 0
#             Fig, Axes = plt.subplots(3, len(Doses), figsize=(8.27, 11.69))

#         AxLim = [0, 0]
#         DoseAxes = [_ for _ in Doses]
#         for d, Dose in enumerate(Doses):
#             DoseFile = [_ for _ in AnimalFiles if Dose in _]
#             if not DoseFile:
#                 Axes[Count, d].text(0.4, 0.4, 'Not found')
#                 DoseAxes[d] = None
#                 continue

#             DoseFile = DoseFile[0]
#             Fxx = IO.Bin.Read(DoseFile+'/DataFxx.dat')[0]
#             txx = IO.Bin.Read(DoseFile+'/Datatxx.dat')[0]
#             Sxx = IO.Bin.Read(DoseFile+'/DataSxx.dat')[0]

#             DoseAxes[d] = Axes[Count, d].contourf(txx, Fxx, Sxx[:,:,BestCh], cmap='inferno')
#             Axes[Count, d].set_ylim([0, 50])
#             Axes[Count, d].set_title(DoseFile.split('/')[-2])
#             if Sxx.min() < AxLim[0]: AxLim[0] = Sxx.min()
#             if Sxx.max() < AxLim[1]: AxLim[1] = Sxx.max()

#         for DoseAx in DoseAxes:
#             if DoseAx: DoseAx.set_clim(AxLim)

#         Fig.subplots_adjust(left=0.1, right=0.8, wspace=0.02)
#         AxColorbar = Fig.add_axes([0.83, 0.1, 0.02, 0.8])
#         Colorbar = Fig.colorbar(DoseAx, cax=AxColorbar)

#         Count += 1
# plt.close()


# with PdfPages('SxxNonClickOverview.pdf') as PDF:
#     for Animal in Animals:
#         AnimalFiles = [_ for _ in NonFiles if Animal in _]

#         Fig, Axes = plt.subplots(16, len(Doses), figsize=(8.27, 11.69))
#         for d, Dose in enumerate(Doses):
#             DoseFile = [_ for _ in AnimalFiles if Dose in _]
#             if not DoseFile:
#                 for Ax in range(16): Axes[Ax, d].text(0.4, 0.4, 'Not found')
#                 continue

#             DoseFile = DoseFile[0]
#             print('Plotting', DoseFile.split('/')[-2], '...')
#             Fxx = IO.Bin.Read(DoseFile+'/DataFxx.dat')[0]
#             txx = IO.Bin.Read(DoseFile+'/Datatxx.dat')[0]
#             Sxx = IO.Bin.Read(DoseFile+'/DataSxx.dat')[0]

#             ChAxes = [0]*Sxx.shape[2]
#             for Ch in range(Sxx.shape[2]):
#                 ChAxes[Ch] = Axes[Ch, d].contourf(txx, Fxx, Sxx[:,:,Ch], cmap='inferno')
#                 Axes[Ch, d].set_ylim([0, 50])

#             for ChAx in ChAxes: ChAx.set_clim([Sxx.min(), Sxx.max()])
#             Axes[0, d].set_title(DoseFile.split('/')[-2])

#         Fig.subplots_adjust(left=0.1, right=0.8, wspace=0.02)
#         AxColorbar = Fig.add_axes([0.83, 0.1, 0.02, 0.8])
#         Colorbar = Fig.colorbar(ChAx, cax=AxColorbar)
#         PDF.savefig()
# plt.close()

with PdfPages('ERPOverview.pdf') as PDF:
    for Animal in Animals:
        AnimalFiles = [_ for _ in ClickFiles if Animal in _]
        AnimalFiles = [[F for F in AnimalFiles if F.split('/')[-2].split('_')[-1] == S]
                       for S in np.unique([F.split('/')[-2].split('_')[-1] for F in AnimalFiles])]

        for Set in AnimalFiles:
            Fig, Axes = plt.subplots(1, len(Doses), figsize=(8.27, 11.69))

            for d, Dose in enumerate(Doses):
                DoseFile = [_ for _ in Set if Dose in _]
                if not DoseFile:
                    Axes[d].text(0.4, 0.4, 'Not found')
                    continue

                ERPs = []
                for File in DoseFile:
                    print('Loading file', '/'.join(File.split('/')[-2:]), '...')
                    All = IO.Bin.Read(File+'/ERPs.dat')[0]
                    if not len(ERPs): ERPs = All.copy()
                    else:
                        ERPs = np.concatenate((ERPs, All), axis=1)
                    del(All)

                print('Done loading set', File.split('/')[-2])
                if not len(ERPs): continue
                ERPs = ERPs.mean(axis=1)
                t = np.arange(ERPs.shape[0])/Rate
                t -=2
                Axes[d] = Plot.AllCh(ERPs[int(1.9*Rate):int(3*Rate)],
                                     t[int(1.9*Rate):int(3*Rate)], ScaleBar=100,
                                     Ax=Axes[d])
                Y = Axes[d].get_ylim()
                Axes[d].plot([0.02]*2, Y, 'b--', zorder=1)
                Axes[d].plot([0.52]*2, Y, 'b--', zorder=1)
                Axes[0].set_xlabel('Time [s]')
                Axes[d].set_title(DoseFile[0].split('/')[-2])
                Plot.Set(Ax=Axes[d])

            Axes[0].set_ylabel('Channels')

            Plot.Set(Fig=Fig)
            PDF.savefig()
            plt.close()


with PdfPages('ThetaPSDNonClickOverview.pdf') as PDF:
    for Animal in Animals:
        AnimalFiles = [_ for _ in NonFiles if Animal in _]
        AnimalFiles = [[F for F in AnimalFiles if F.split('/')[-2].split('_')[-1] == S]
                       for S in np.unique([F.split('/')[-2].split('_')[-1] for F in AnimalFiles])]

        for Set in AnimalFiles:
            Fig, Axes = plt.subplots(4, 4, figsize=(8.27, 11.69), sharex=True)
            for d, Dose in enumerate(Doses):
                DoseFile = [_ for _ in Set if Dose in _]
                if not DoseFile:
                    # for Ax in range(16): Axes[Ax].text(0, 0, 'Not found')
                    continue

                DoseFile = DoseFile[0]
                print('Plotting', DoseFile.split('/')[-2], '...')
                F = IO.Bin.Read(DoseFile+'/DataPSD/Theta/F.dat')[0]
                PSD = IO.Bin.Read(DoseFile+'/DataPSD/Theta/PSD.dat')[0]
                Freq = (F >= 0) * (F <= 40)

                for Ch in range(PSD.shape[1]):
                    Axes[Ch//4,Ch%4].plot(F[Freq], PSD[Freq,Ch], Colors[d], label=Dose)
                    Axes[Ch//4,Ch%4].set_title('Channel' + str(Ch+1))
                    Plot.Set(Ax=Axes[Ch//4,Ch%4])

                Axes[0,0].legend()


            # ChLim = np.zeros((2,16,3))
            # for Ch in range(len(Axes)):
            #     for d in range(len(Axes[0])):
            #         ChLim[:,Ch,d] = Axes[Ch,d].get_ylim()

            # for Ch in range(len(Axes)):
            #     for DoseAx in Axes[Ch]:
            #         DoseAx.set_ylim([ChLim[:,Ch,:].min(), ChLim[:,Ch,:].max()])
            #         DoseAx.set_ylabel('Frequency [Hz]')
            #         DoseAx.set_ylabel('Power\n[$V^{2}/Hz$]')
            #         Plot.Set(Ax=DoseAx)
            #         DoseAx.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

            Plot.Set(Fig=Fig)
            Fig.suptitle(Set[0].split('/')[-2])
            PDF.savefig()
            plt.close()



with PdfPages('ThetaNonClickOverview.pdf') as PDF:
    for Animal in Animals:
        AnimalFiles = [_ for _ in NonFiles if Animal in _]
        AnimalFiles = [[F for F in AnimalFiles if F.split('/')[-2].split('_')[-1] == S]
                       for S in np.unique([F.split('/')[-2].split('_')[-1] for F in AnimalFiles])]

        for Set in AnimalFiles:
            Fig, Axes = plt.subplots(16, len(Doses), figsize=(8.27, 11.69), sharex=True)
            for d, Dose in enumerate(Doses):
                DoseFile = [_ for _ in Set if Dose in _]
                if not DoseFile:
                    for Ax in range(16): Axes[Ax, d].text(0, 0, 'Not found')
                    continue

                DoseFile = DoseFile[0]
                print('Plotting', DoseFile.split('/')[-2], '...')
                DataTFD = IO.Bin.Read(DoseFile+'/DataTFD/Theta.dat')[0]

                t = np.arange(DataTFD.shape[0])/Rate
                for Ch in range(DataTFD.shape[1]):
                    Axes[Ch, d].plot(t, DataTFD[:,Ch], 'k')

                Axes[0, d].set_title(DoseFile.split('/')[-2])

            ChLim = np.zeros((2,16,3))
            for Ch in range(len(Axes)):
                for d in range(len(Axes[0])):
                    ChLim[:,Ch,d] = Axes[Ch,d].get_ylim()

            for Ch in range(len(Axes)):
                for DoseAx in Axes[Ch]:
                    DoseAx.set_ylim([ChLim[:,Ch,:].min(), ChLim[:,Ch,:].max()])
                    Plot.Set(Ax=DoseAx)
                    DoseAx.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

            Plot.Set(Fig=Fig)

            PDF.savefig()
            plt.close()



with PdfPages('GammaNonClickOverview.pdf') as PDF:
    for Animal in Animals:
        AnimalFiles = [_ for _ in NonFiles if Animal in _]
        AnimalFiles = [[F for F in AnimalFiles if F.split('/')[-2].split('_')[-1] == S]
                       for S in np.unique([F.split('/')[-2].split('_')[-1] for F in AnimalFiles])]

        for Set in AnimalFiles:
            Fig, Axes = plt.subplots(16, len(Doses), figsize=(8.27, 11.69), sharex=True)
            for d, Dose in enumerate(Doses):
                DoseFile = [_ for _ in Set if Dose in _]
                if not DoseFile:
                    for Ax in range(16): Axes[Ax, d].text(0, 0, 'Not found')
                    continue

                DoseFile = DoseFile[0]
                print('Plotting', DoseFile.split('/')[-2], '...')
                DataTFD = IO.Bin.Read(DoseFile+'/DataTFD/Gamma.dat')[0]

                t = np.arange(DataTFD.shape[0])/Rate
                for Ch in range(DataTFD.shape[1]):
                    Axes[Ch, d].plot(t, DataTFD[:,Ch], 'k')

                Axes[0, d].set_title(DoseFile.split('/')[-2])

            ChLim = np.zeros((2,16,3))
            for Ch in range(len(Axes)):
                for d in range(len(Axes[0])):
                    ChLim[:,Ch,d] = Axes[Ch,d].get_ylim()

            for Ch in range(len(Axes)):
                for DoseAx in Axes[Ch]:
                    DoseAx.set_ylim([ChLim[:,Ch,:].min(), ChLim[:,Ch,:].max()])
                    Plot.Set(Ax=DoseAx)
                    DoseAx.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

            Plot.Set(Fig=Fig)

            PDF.savefig()
            plt.close()


#%% Plots
## Check selected channels
Colors = np.array(['k'] * ChNo)
Colors[Channels] = 'r'
Plot.AllCh(ERPMeanNorm, PhaseWindow/Rate, Colors, SubPlotsArgs={'figsize': (1,4)})

## ERPs
Spaces = Plot.GetSpaces(ERPs.mean(axis=1))
Fig, Axes = plt.subplots(1,2, figsize=(3,7), dpi=100)
for Ax in Axes:
    YTicks = []
    for Ch in range(ERPs.shape[2]):
        X = (np.arange(ERPs.shape[0])/Rate) - ERPWindow
        Y = ERPMean[:,Ch] + Spaces[Ch]
        YTicks.append(Y.mean())
        Ax.plot(X, Y, 'k')

    Lims = Ax.get_ylim()
    Ax.plot([0]*2, [max(Lims), min(Lims)], 'r--')
    Ax.plot([0.5]*2, [max(Lims), min(Lims)], 'r--')
    Ax.set_yticks(YTicks)
    Ax.set_yticklabels(np.arange(ERPs.shape[2])+1)
    Ax.set_xlabel('Time [s]')

Axes[0].set_xlim([-0.01, 0.1])
Axes[1].set_xlim([0.49, 0.6])
Axes[1].set_xticklabels([round(_,3) for _ in Axes[0].get_xticks()])
Axes[0].set_title('1st click')
Axes[1].set_title('2nd click')
Axes[0].set_ylabel('Channel')
Fig.tight_layout()
Fig.savefig('ERPs.pdf')
plt.show()


# PSDs
Fig, Axes = plt.subplots(1, 2, figsize=(4,5), dpi=100)
for F,(Freq,Band) in enumerate(FreqBands.items()):
    Axes[F] = Plot.AllCh(DataPSD[Freq][1][DataPSD[Freq][0]<max(Band), :],
                         DataPSD[Freq][0][DataPSD[Freq][0]<max(Band)],
                         Ax=Axes[F])
    Axes[F].set_title(Freq)
    Axes[F].set_xlabel('Frequency [Hz]')
Axes[0].set_ylabel('Power per channel [$V^{2}/Hz$]')
Fig.savefig('PSD.pdf')
plt.show()


# Data TFDs
Fig, Axes = plt.subplots(1, 2, figsize=(4,5), dpi=100)
for F,(Freq,Band) in enumerate(FreqBands.items()):
    Axes[F] = Plot.AllCh(DataTFD[Freq],
                         np.linspace(0, Data.shape[0]/Rate, DataTFD[Freq].shape[0]),
                         Ax=Axes[F])
    Axes[F].set_title(Freq)
    Axes[F].set_xlabel('Time [s]')
Axes[0].set_ylabel('Power per channel [$V^{2}/Hz$]')
Fig.savefig('TFD.pdf')
plt.show()

# ERP TFD
Fig, Axes = plt.subplots(1, 2, figsize=(4,5), dpi=100)
for F,(Freq,Band) in enumerate(FreqBands.items()):
    Axes[F] = Plot.AllCh(ERPTFD[Freq].mean(axis=1),
                         np.linspace(-ERPWindow, 1+ERPWindow, ERPTFD['Theta'].shape[0]),
                         Colors, Ax=Axes[F])
    Axes[F].set_title(Freq)
    Axes[F].set_xlabel('Time [s]')
Axes[0].set_ylabel('Power per channel [$V^{2}/Hz$]')
Fig.savefig('ERPTFD.pdf')
plt.show()

PlotTFD({F: Freq.mean(axis=1) for F, Freq in ERPTFD.items()})

# Sxxs of trials
Fig, Axes = plt.subplots(Data.shape[1], ERPSxx.shape[2], figsize=(7,12))
for C in range(Data.shape[1]):
    for T in range(ERPSxx.shape[2]):
        Axes[C][T].contourf(ERPtxx-ERPWindow, ERPFxx, ERPSxx[:,:,T,C], cmap='inferno', linewidths=0)
        Axes[C][T].set_ylim([0,40])

for C in range(Data.shape[1]): Axes[C][0].set_ylabel('Ch. '+str(C+1))
for T in range(ERPSxx.shape[2]):
    Axes[0][T].set_title('Trial '+str(T+1))
    Axes[-1][T].set_xlabel('Time [s]')
Fig.savefig('SxxTrials.pdf')
plt.show()


# Sxxs induced and evoked
Fig, Axes = plt.subplots(Data.shape[1], 2, figsize=(7,12))
for C in range(Data.shape[1]):
    Axes[C][0].contourf(ERPtxx-ERPWindow, ERPFxx, ERPSxx[:,:,:,C].mean(axis=2), cmap='inferno', linewidths=0)
    Axes[C][1].contourf(ERPMeantxx-ERPWindow, ERPMeanFxx, ERPMeanSxx[:,:,C], cmap='inferno', linewidths=0)

    for Ax in Axes[C]: Ax.set_ylim([0,40])

for C in range(Data.shape[1]): Axes[C][0].set_ylabel('Ch. '+str(C+1))
Axes[0][0].set_title('Evoked')
Axes[0][1].set_title('Induced')
Axes[-1][0].set_xlabel('Time [s]')
Axes[-1][1].set_xlabel('Time [s]')
Fig.savefig('SxxMeans.pdf')
plt.show()


# Power per ch
Fig, Axes = plt.subplots(1,2, figsize=(5,5))
for A, (T, Type) in enumerate(MeanPower.items()):
    for F, Freq in Type.items():
        Axes[A].plot(Freq, np.arange(16)+1, '.--', label=F)

    Axes[A].set_yticks(np.arange(16)+1)
    Axes[A].set_yticklabels(np.arange(16)[::-1]+1)
    Axes[A].set_title(T)
    Axes[A].set_xlabel('Power [$V^{2}/Hz$]')
    Axes[A].set_ylabel('Channel')
    Axes[A].legend()
Fig.savefig('PowerPerCh.pdf')
plt.show()