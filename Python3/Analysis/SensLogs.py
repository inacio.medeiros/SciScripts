#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2019-01-14
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""

import numpy as np
from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')

Folder = '/home/malfatti/ToDel'
with open(Folder + '/accelerometer.txt', 'r') as F: Sensor = F.readlines()
with open(Folder + '/references.txt', 'r') as F: TS = F.readlines()

TS = [float(Line.split('\n')[0].split(' ')[0]) for Line in TS[6:]]

Columns = Sensor[5].split('\n')[0].split(' ')

Data = np.zeros((len(Sensor[6:]), len(Columns)), dtype=float)
for L,Line in enumerate(Sensor[6:]):
    Line = [float(_) for _ in Line.split('\n')[0].split(' ')]
    Data[L,:] = Line

Data = {Column: Data[:,C] for C, Column in enumerate(Columns)}


for Ax in ['x', 'y', 'z']:
    plt.plot(Data['elapsed-time-system'], Data[Ax])
plt.plot(TS, [0]*len(TS), 'k*')
plt.show()