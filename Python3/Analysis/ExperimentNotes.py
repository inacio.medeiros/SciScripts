#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2017-12-18
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
#%%
import os

from glob import glob
from IO import Txt
from IO.IO import RunProcess


#%%
def IsInt(i):
    try:
        int(i)
        return(True)
    except ValueError:
        return(False)


def NotebookToDict(NoteFile):
    with open(NoteFile, 'r') as F: Note = F.readlines()
    Note = [_[:-1] for _ in Note]
    Info = {}
    Info['Injections'] = [_ for _ in Note if len(_.split(' ')[0]) == 6 and IsInt(_.split(' ')[0])]
    Info['Comments'] = [_ for _ in Note if _ not in Info['Injections']]
    Info['Injections'] = sorted(Info['Injections'])
    Info['Injections'] = {_.split(' ')[0]: ' '.join(_.split(' ')[1:]) for _ in Info['Injections']}


def TimeKey(Key):
    MM = 0 if int(Key[4:]) <30 else 1
    MM = "{0:02d}".format(int(Key[2:4])+MM)
    HH = Key[:2]

    if MM == 60:
        HH = "{0:02d}".format(int(HH)+1)
        MM = '00'

    Time = HH + 'h' + MM
    return(Time)


def TimeToKey(Note):
    Info = Txt.Read(Note)

    for E, Exp in Info['Experiments'].items():
        if 'Injections' not in Exp: continue

        for D, Drug in Exp['Injections'].items():
            Keys = [K for K in Drug if K not in ['Location', 'Units'] and len(K) == 2]
            for K in Keys:
                Key = Info['Experiments'][E]['Injections'][D][K][-1]
                Info['Experiments'][E]['Injections'][D][Key] = Drug.pop(K)
                del(Info['Experiments'][E]['Injections'][D][Key][-1])

            if 'HHMMSS' in Info['Experiments'][E]['Injections'][D]['Units']:
                Ind = Info['Experiments'][E]['Injections'][D]['Units'].index('HHMMSS')
                del(Info['Experiments'][E]['Injections'][D]['Units'][Ind])

    Txt.Write(Info, Note)
    return(None)


def Note2Text(Note):
    Info = Txt.Read(Note)

    if 'Animal' in Info.keys():
        if 'Name' in Info['Animal']:
            Text = ['~~~ ' + Info['Animal']['Name'], '', '']
        elif 'AnimalName' in Info['Animal']:
            Text = ['~~~ ' + Info['Animal']['AnimalName'], '', '']
        else:
            Title = Note.split('/')[-1].split('.')[0]
            Text = ['~~~ ' + Title, '', '']

        Text += ['### Animal info', '']
        for K,V in Info['Animal'].items():
            if K in ['Name', 'AnimalName']: continue
            Text.append(K+': '+str(V))
        Text += ['', '']

    else:
        Title = Note.split('/')[-1].split('.')[0]
        Text = ['~~~ ' + Title, '', '']

    if 'Experiments' in Info.keys():
        Text += ['### Experiments', '']
        for E,Exp in Info['Experiments'].items():
            Text += ['## '+E, '']

            for EB, ExpBlock in Exp.items():
                if EB == 'Animal':
                    Text.append('# '+EB)
                    for K,V in ExpBlock.items(): Text.append(K+': '+str(V))
                    Text.append('')

                if EB == 'Setup':
                    Text.append('# '+EB)
                    for K,V in ExpBlock.items():
                        if type(V) != dict: Text.append(K+': '+str(V))
                        else:
                            Text.append(K+':')

                            if K == 'Electrodes':
                                for k,v in V.items():
                                    Text.append('    '+k+':')

                                    for kk,vv in v.items():
                                        if kk == 'Location':
                                            if type(v['Location']) == dict:
                                                Loc = 'AP: '+str(v['Location']['AP']) + '\n                  ' + \
                                                      'ML: '+str(v['Location']['ML']) + '\n                  ' + \
                                                      'DV: '+str(v['Location']['DV']) + '\n'
                                            else:
                                                Loc = str(v['Location'])

                                            Text.append('        Location: '+Loc)
                                        else: Text.append('        '+kk+': '+str(vv))
                                        Text.append('')

                            else:
                                for k,v in V.items():
                                    Text.append('    '+k+': '+str(v))

                    Text.append('')

                elif EB == 'Injections':
                    Text.append('# '+EB)
                    for I,Inj in ExpBlock.items():
                        Text.append(I+':')

                        if type(Inj['Location']) == str:
                            Loc = Inj['Location']
                        elif type(Inj['Location']) == dict:
                            Loc = 'AP: '+str(Inj['Location']['AP']) + '\n              ' + \
                                  'ML: '+str(Inj['Location']['ML']) + '\n              ' + \
                                  'DV: '+str(Inj['Location']['DV']) + '\n'

                        Text.append('    Location: '+Loc)

                        ThisDrug = [
                                '    ' + \
                                TimeKey(D) + ': ' + \
                                ', '.join([str(Drug[_])+''+Inj['Units'][_] for _ in range(len(Drug))])
                                # str(Drug[0])+''+Inj['Units'][0] + ', ' + \
                                # str(Drug[1])+''+Inj['Units'][1]
                            for D, Drug in Inj.items()
                            if D not in ['Units', 'Location', 'FullName']
                        ]
                        ThisDrug.sort()

                        Text += ThisDrug+['']

                elif EB == 'Coordinates':
                    Text.append('# '+EB)
                    for K,V in ExpBlock.items():
                        Text.append(K+': '+str(V))
                    Text.append('')

            ## These are in separate loops to enforce order
            for EB, ExpBlock in Exp.items():
                if EB == 'Comments':
                    Text.append('# '+EB)
                    for K,V in ExpBlock.items():
                        KK = TimeKey(K) if len(K) == 6 else K
                        Text.append(KK+': '+str(V))
                    Text.append('')

            for EB, ExpBlock in Exp.items():
                if EB not in ['Animal', 'Setup', 'Injections', 'Coordinates', 'Comments']:
                    Text.append('# '+EB)
                    if type(ExpBlock) == dict:
                        for K,V in ExpBlock.items():
                            KK = TimeKey(K) if len(K) == 6 else K
                            Text.append(KK+': '+str(V))
                        Text.append('')
                    else:
                        Text.append(str(ExpBlock))


            Text.append('')

    for K,Key in Info.items():
        if K in ['Animal', 'Experiments']: continue

        Text.append('## '+K)
        if type(Key) == dict:
            for k,v in Key.items(): Text.append(k+': '+str(v))
            Text.append('')
        else:
            Text.append(str(Key))

        Text.append('')

    Text = '\n'.join(Text)
    return(Text)


def Text2Tex(Text):
    # Fix sections
    Replace = {
        # '~~~ ': '{\\LARGE \\bfseries \centering{}',
        '~~~ ': '\\chapter*{',
        '### ': '\\section{',
        '## ': '\\subsection*{',
        '# ': '\\subsubsection{'
    }

    Text = Text.split('\n')
    for L,Line in enumerate(Text):
        Start = Line.split(' ')[0]+' '
        if Start in ['~~~ ', '# ']:
            End = '\\underline{'+Line[len(Start):]+'}'
            Line = Start+End
        if Start in Replace: Text[L] = Line.replace(Start, Replace[Start]) + '}'

    Sections = [Text.index(_) for _ in Text if '\\subsection*{' in _]
    for S in Sections: Text[S-1] = Text[S-1]+' \\noindent\\rule{\\textwidth}{1pt}'

    # # Columns
    # for S in Sections: Text[S] = Text[S]+' \\begin{multicols}{2}'
    # for S in Sections[1:]: Text[S-2] = Text[S-2]+' \\end{multicols}'
    # Text.append('\\end{multicols}')

    # Fix symbols and empty lines
    Text = '\n'.join(Text)
    Replace = [('%', '\\%'), ('µl', '\\si{\\ul}'), ('~', '$\\approx$'),
               ('_', '\\textunderscore '), ('\n', '\n\n')]

    for R in Replace: Text = Text.replace(R[0], R[1])

    # Fix "Location" alignment
    Text = Text.replace('                  ', '~~~~~~~~~~~~~~~~~~~')
    Text = Text.replace('              ', '~~~~~~~~~~~~~~~')
    Text = Text.replace('        ', '~~~~~~~~')
    Text = Text.replace('    ', '~~~~')
    return(Text)


NoteTexHead = r'''%% Expnotes
\documentclass[11pt,a4paper,notitlepage,twoside]{report}
\usepackage[utf8]{inputenc}
\usepackage{siunitx}
\usepackage[version=3]{mhchem}

\usepackage{multicol}
\usepackage[margin=2cm]{geometry}
\usepackage{titlesec}
\titleformat{\chapter}{\normalfont\huge}{\thechapter.}{20pt}{\huge\bf}

\begin{document}
\setlength{\parindent}{0pt}
\setcounter{chapter}{1}
\setcounter{section}{0}

'''


def NoteWrite(Note, Format='txt'):
    Text = Note2Text(Note)

    if Format == 'txt':
        with open(Note.split('.')[0]+'.txt', 'w') as F: F.write(Text)

    elif Format in ['tex', 'pdf']:
        Path = '/'.join(Note.split('/')[:-1])+'/Tex'
        File = Note.split('/')[-1].split('.')[0]
        os.makedirs(Path, exist_ok=True)

        Head = NoteTexHead
        Text = Text2Tex(Text)
        with open(Path+'/'+File+'.tex', 'w') as F:
            F.write(Head)
            F.write(Text)
            F.write('\\end{document}\n')

        if Format == 'pdf':
            Here = os.getcwd(); os.chdir(Path)
            Cmd = ['pdflatex', File+'.tex']
            ReturnCode = RunProcess(Cmd, File+'.log'); os.chdir(Here)

            if ReturnCode: print('An error ocurred: no pdf generated.')
            else:
                os.rename(Path+'/'+File+'.pdf', Path+'/../'+File+'.pdf')
                for f in glob(Path+'/*'): os.remove(f)
                os.rmdir(Path)

        elif Format == 'tex':
            os.rename(Path+'/'+File+'.tex', Path+'/../'+File+'.tex')
            os.rmdir(Path)



#%% Convert notes to beautiful pdfs
NotesPath = os.environ['HOME']+'/Nebula/Documents/PhD/Notes/Experiments/Animals'
Notes = sorted([N for N in glob(NotesPath+'/*.dict')])
Format = 'pdf'

for N,Note in enumerate(Notes):
    # if N<69: continue
    if 'Recovery_' in Note: continue
    print(N,Note)
    NoteWrite(Note, Format)


#%% Report animals that died during surgeries
Notes = [N for N in glob(NotesPath+'/*.py') if 'Prevention' in N or
                                               'Control' in N or
                                               'VTF' in N]
Notes.sort()

Animals = {
    'Dead': {K: [] for K in ['Date', 'Ket', 'Xyl', 'Weight', 'Age', 'Shots']},
    'Alive': {K: [] for K in ['Date', 'Ket', 'Xyl', 'Weight', 'Age', 'Shots']}
}

for Note in Notes:
    Info = Txt.Read(Note)

    for E, Exp in Info['Experiments'].items():
        if 'UnitRec' in E: continue

        if 'Comments' in Exp.keys():
            Comms = list(Exp['Comments'].values())
            Died = [True for C in Comms if 'died' in C.lower()
                                        or 'dead' in C.lower()
                                        or 'death' in C.lower()]

            Key = 'Dead' if True in Died else 'Alive'
            First = sorted(Exp['Injections']['Ketamine'].keys())[0]

            Animals[Key]['Date'].append(E.split('-')[0])
            Animals[Key]['Ket'].append(Exp['Injections']['Ketamine'][First][0])
            Animals[Key]['Xyl'].append(Exp['Injections']['Xylazine'][First][0])
            Animals[Key]['Shots'].append(len(Exp['Injections']['Ketamine'].keys()) - 3)
            Animals[Key]['Weight'].append(Exp['Animal']['Weight'])
            Animals[Key]['Age'].append(Exp['Animal']['Age'])

from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')

Colors = ['r', 'b']
Fig, Ax = plt.subplots()
for E, End in enumerate([('Dead', Animals['Dead']), ('Alive', Animals['Alive'])]):
    Ax.scatter(End[1]['Age'], End[1]['Weight'], c=Colors[E], label=End[0])
    # X = [I[0] for I in sorted(enumerate(End['Age']), key = lambda x:x[1])]
    # Y = [End['Weight'][I] for I in X]
    # X = [End['Age'][I] for I in X]

    # plt.plot(X, Y)

AxArgs = {'xlabel': 'Age [days]',
          'ylabel': 'Weight [g]',}
Plot.Set(Ax=Ax, AxArgs=AxArgs)
plt.legend(loc='best')
plt.savefig('WeightxAge.pdf', format='pdf', dpi=300)
plt.show()
