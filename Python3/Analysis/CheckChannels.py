#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 29 09:20:49 2018

@author: malfatti
"""
from IO import IO
from DataAnalysis.DataAnalysis import FilterSignal

Params = {'backend': 'TKCairo'}
from matplotlib import rcParams; rcParams.update(Params)
from matplotlib import pyplot as plt


A = IO.DataLoader('Barbara/Nebula/Documents/PhD/Data/2018-06-28_17-53-36_BCB/')[0]
A = A['101']['0']
Rate = 25000


plt.plot(FilterSignal(A[Rate:Rate*5, 7], Rate, [10], Type='lowpass'), label='8', lw=2)
plt.plot(FilterSignal(A[Rate:Rate*5, 8], Rate, [10], Type='lowpass')+1000, label='9', lw=2)
#plt.show()
plt.savefig('Ch8and9.pdf')

#Fig, Ax = plt.subplots(16,1, sharex=True)
#for Ch in range(A.shape[1]): Ax[Ch].plot(A[:,Ch], label=str(Ch))
#plt.show()