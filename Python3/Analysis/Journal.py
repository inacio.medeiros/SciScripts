#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 14:53:42 2019

@author: malfatti
"""
#%% Import

JournalFile = ''

class Journal():
    Symbols = ['@', '!', '$', '&', '*', '%']
    Meaning = ['Tags', 'People', 'Signs', 'Places', 'Notes', 'AwakeNotes']

    def Read(File):
        with open(File, 'r') as F: Journal = F.readlines()
        return(Journal)

    def Write(Journal, File):
        with open(File, 'w') as F: F.write(''.join(Journal))
        return(None)

    def MonthStr2No(Month):
        Months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
        if Month.lower() not in Months:
            print('Month should be in', Months)
            return(None)

        Month = "{0:02d}".format(Months.index(Month.lower())+1)
        return(Month)

    class Dreams():
        def LinesToLists(Lines, Starts):
            DreamList = []
            for S,Start in enumerate(Starts):
                if S != len(Starts)-1:
                    DreamList.append(Lines[Start:Starts[S+1]])
                else:
                    DreamList.append(Lines[Start:])

            return(DreamList)


        def ListsToDict(DreamList):
            Dreams = {}
            for Dream in DreamList:
                Time = '_'.join(Dream[0].split(' ')[1:3]).replace('h','').replace('?','0')
                Dreams[Time] = {}

                for Line in Dream:
                    if Line[:3] == '===': continue
                    if Line[-1] == '\n': Line = Line[:-1]
                    if not Line: continue

                    while Line[-1] == ' ':
                        Line = Line[:-1]
                        if not Line: break

                    if not Line: continue


                    if Line[0] in Journal.Symbols:
                        Keys = Line.split(' ')[1:]
                        if Keys:
                            Key = Journal.Meaning[Journal.Symbols.index(Line[0])]

                            if Key not in Dreams[Time]: Dreams[Time][Key] = []

                            if Key in ['Notes', 'AwakeNotes']:
                                Dreams[Time][Key].append(' '.join(Keys))
                            else:
                                Dreams[Time][Key] += Keys
                    else:
                        if 'Description' not in Dreams[Time]: Dreams[Time]['Description'] = []
                        Dreams[Time]['Description'].append(Line)

                if not Dreams[Time]: del(Dreams[Time])

            return(Dreams)


        def LinesToDict(Lines, Starts):
            DreamList = Journal.Dreams.LinesToLists(Lines, Starts)
            Dreams = Journal.Dreams.ListsToDict(DreamList)
            return(Dreams)


        class Text():
            def Read(File):
                Lines = Journal.Read(File)
                Starts = [L for L,Line in enumerate(Lines) if Line[:3] == '===']
                Dreams = Journal.Dreams.LinesToDict(Lines, Starts)
                return(Dreams)


        class Oneironaut():
            def Read(File):
                Lines = Journal.Read(File)
                Starts = []

                for L,Line in enumerate(Lines):
                    Line = Line.replace(' (lucid)', '')
                    if ':' in Line:
                        if Line[-4] == ':' and Line.split(' ')[-2][:2] == '20':
                            if Lines[L+1][:3] == '===':
                                Lines[L] = '\n'
                                Starts.append(L+1)
                            else:
                                Line = Line.split(' ')
                                Line = '=== '+Line[3]+Journal.MonthStr2No(Line[2])+Line[1]+' '+Line[4].replace(':','h')[:-1]+' ===\n'
                                Starts.append(L)
                                Lines[L] = Line

                Dreams = Journal.Dreams.LinesToDict(Lines, Starts)
                return(Dreams)


            def Write(Dreams, File):
                Keys = sorted(Dreams.keys(), key=lambda x: int(''.join(x.split('_'))))
                Lines = []
                for K in Keys:
                    KDate = K[:4]+'/'+K[4:6]+'/'+K[6:8]+' '+K[-4:-2]+':'+K[-2:]
                    Lines.append(KDate+'\n')
                    KLines = []
                    for S,Sig in enumerate(Journal.Meaning[:-1]):
                        if Sig in Dreams[K].keys():
                            if Sig == 'Notes':
                                for N in Dreams[K][Sig]:
                                    L = Journal.Symbols[S]+' '+N
                                    KLines.append(L+'\n')
                            else:
                                L = Journal.Symbols[S]+' '+' '.join(Dreams[K][Sig])
                                KLines.append(L+'\n')

                    if 'Description' in Dreams[K]:
                        for N in Dreams[K]['Description']: KLines.append(N+'\n')

                    if 'AwakeNotes' in Dreams[K]:
                        for N in Dreams[K]['AwakeNotes']:
                            L = Journal.Symbols[Journal.Meaning.index('AwakeNotes')]+' '+N
                            KLines.append(L+'\n')

                    KLines.reverse()
                    KLines.append('\n')
                    for L in KLines: Lines.append(L)

                Journal.Write(Lines, File)
                return(None)


OneirFile = '/home/malfatti/Nebula/Documents/Notes/Journal/20191013-DreamJournal_Oneironaut'
Oneir = Journal.Dreams.Oneironaut.Read(OneirFile)
TextFile = '/home/malfatti/Nebula/Documents/Notes/Journal/20180705-DreamJournal_Text'
Text = Journal.Dreams.Text.Read(TextFile)
Text = {**Oneir, **Text}
Journal.Dreams.Oneironaut.Write(Text, 'Test.txt')
