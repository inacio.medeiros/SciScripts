# -*- coding: utf-8 -*-
"""
Created on 20190418
@author: malfatti

Comodulation between two freqbands
"""
#%%
import os
import numpy as np
from DataAnalysis.DataAnalysis import FilterSignal, Comodulation, RemapCh
from IO import IO

from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')



#%%
Folder = '/home/malfatti/Barbara/PhD/Data/2019-03-29_10-01-24_Baseline'
Ch = 12
PhaseFreqBand = [1, 20]
PhaseFreqBandWidth = 4
AmpFreqBand = [20, 200]
AmpFreqBandWidth = 10
FilterOrder = 3

Data, Rate = IO.DataLoader(Folder, ChannelMap=RemapCh('Ciralli', 'None16'))
Key = list(Data.keys())[0]
Data, Rate = Data[Key]['0'][:,:16], Rate[Key]

# Downsample
Data = Data[::Rate//1000,:]
Rate = 1000

if len(Data[:,0]) > 300*Rate: Data = Data[:300*Rate,:]
Data = Data[:int(len(Data[:,0])**0.5)**2,:]
Data = FilterSignal(Data, Rate, [2, Rate//2-1], FilterOrder)

# import scipy.io as sio
# LFP = sio.loadmat('NotSynced/Downloads/LFP_HG_HFO.mat')
# LFP, LFPRate = LFP['lfpHFO'][0], 1000

Cmdlgrm, AmpFreq, PhaseFreq = Comodulation(Data, Rate, PhaseFreqBand, PhaseFreqBandWidth, AmpFreqBand, AmpFreqBandWidth, FilterOrder)
Plot.Comodulogram(Cmdlgrm, AmpFreq+PhaseFreqBandWidth//2, PhaseFreq+PhaseFreqBandWidth//2)
