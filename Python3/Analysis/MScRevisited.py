#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20180226
@license: GNU GPLv3 <https://gitlab.com/malfatti/SciScripts/raw/master/LICENSE>
@homepage: https://gitlab.com/Malfatti/SciScripts
"""
#%% Reanalysis again
import numpy as np

from glob import glob
from imp import load_source
from klusta.kwik import KwikModel

from DataAnalysis import DataAnalysis
from DataAnalysis.Plot import Klusta as KlPlot
from DataAnalysis.Units import Units, Klusta as Kl
from IO import IO, Klusta


def CheckChannels(PrmFiles):
    Plt = KlPlot.Plot.Return('Plt')

    TTLFix = [None for _ in PrmFiles]
    PrmOrder = [None for _ in PrmFiles]

    for P, PrmFile in enumerate(PrmFiles):
        print(PrmFile)
        with open(PrmFile, 'r') as F: Prm = load_source('Prm', '', F)
        DataFiles = Prm.traces['raw_data_files']
        Stims = Prm.DataInfo['Stims']
        TTLFix[P] = [None for _ in DataFiles]
        PrmOrder[P] = [None for _ in DataFiles]

        for D, DataFile in enumerate(DataFiles):
            if 'cerebro' in DataFile:
                DataFile = '/'.join(PrmFile.split('/')[:7])+'/'+'/'.join(DataFile.split('/')[5:])

            InfoFile = '.'.join(DataFile.split('.')[:-1]) + '-Info.dict'
            Info = IO.Txt.Read(InfoFile)

            TTLCh = list(range(16, Info['Shape'][1]))
            TTLCh = [_+1 for _ in TTLCh]
            Data = IO.Bin.Read(DataFile, TTLCh)[0]

            for Ch in range(Data.shape[1]):
                Plt.plot(Data[:,Ch], label=str(TTLCh[Ch]), lw=0.5)
            Plt.legend(loc='best')
            Plt.title(DataFile.split('/')[-1])
            Plt.show()

            TTLFix[P][D] = input('Fix: ')
            PrmOrder[P][D] = Stims[D]

    return(None)


def ClusterizeSpks(Animals, Stim, ProbeSpacing, Log=None):
    if not Log: Log = {K: [] for K in ['Done', 'Errors', 'ErrorsLog', 'Skipped']}

    for A, Animal in Animals.items():
        try:
            for Es, Exps in enumerate(Animal):
                if A+'-'+str(Es) in Log['Done']: continue
                ExpFolder = '/'.join(Exps[0].split('/')[:-2]) + '/KlustaFiles'

                for E, Exp in enumerate(Exps):
                    ExpName = Exp.split('/')[-1][:-4]
                    DataFile = ''.join([ExpName, '_Exp', "{0:02d}".format(int(Es)),
                                        '-', "{0:02d}".format(int(E))])

                    Data, Rate = IO.Intan.Read(Exp)

                    # Override to fix my stupidity -- make all files with the same Ch number
                    if Data.shape[1] == 15:
                        D = np.zeros((Data.shape[0],17))
                        D[:,:15] = Data[:,:]
                        D[:,-1] = Data[:,-1]
                        Data = D
                        del(D)

                    RawDataInfo = {'Rate': Rate}
                    IO.Bin.Write(Data, ExpFolder+'/'+DataFile+'.dat', RawDataInfo)

                FilesPrefix = ExpFolder+'/'+'Exp' + "{0:02d}".format(int(Es))

                raw_data_files = sorted(glob(ExpFolder+'/*'+'Exp' + "{0:02d}".format(int(Es))+'*.dat'))

                RawDataInfo = [IO.Txt.Read(D) for D in glob(ExpFolder+'/*-Info.dict')]
                RawDataInfo = RawDataInfo[0]

                Map = DataAnalysis.RemapCh('A16', 'RHAHeadstage')
                Klusta.PrbWrite(FilesPrefix+'.prb', Map, ProbeSpacing)

                Klusta.PrmWrite(FilesPrefix+'.prm', 'Exp' + "{0:02d}".format(int(Es)),
                                FilesPrefix+'.prb', raw_data_files, RawDataInfo['Rate'],
                                RawDataInfo['Shape'][1], RawDataInfo['DType'],
                                DataInfo={'Stims': Stims[A][Es]})


                Klusta.Run('Exp' + "{0:02d}".format(int(Es))+'.prm',
                            ExpFolder, Overwrite=True)

                Log['Done'].append(A+'-'+str(Es))

        except Exception as e:
            Log['Errors'].append(A+'-'+str(Es))
            Log['ErrorsLog'].append(e)
            print(''); print(e); print('')


def ConvertFiles(Animals):
    for A, Animal in Animals.items():
        for Es, Exps in enumerate(Animal):
            for E, Exp in enumerate(Exps):
                ExpName = Exp.split('/')[-1][:-4]
                ExpFolder = '/'.join(Exp.split('/')[:-2]) + '/KlustaFiles'
                DataFile = ''.join([ExpName, '_Exp', "{0:02d}".format(int(Es)),
                                    '-', "{0:02d}".format(int(E))])

                Data, Rate = IO.Intan.Read(Exp)

                # Override to fix my stupidity -- make all files with the same Ch number
                if Data.shape[1] == 15:
                    D = np.zeros((Data.shape[0],17))
                    D[:,:15] = Data[:,:]
                    D[:,-1] = Data[:,-1]
                    Data = D
                    del(D)

                RawDataInfo = {'Rate': Rate}
                IO.Bin.Write(Data, ExpFolder+'/'+DataFile+'.dat', RawDataInfo)

    return(None)


def QuantifyTTLs(Data, Fix):
    if Fix in ['L2S8', 'L1S4', 'L1S2']:
        Stim = 'SL'
        Thresholds = [max(Data)*0.9, max(Data)*0.6, max(Data)*0.1]
        SoundLight, Sound, Light = [np.where((Data[:-1] < T)*(Data[1:] > T))[0] for T in Thresholds]

    elif Fix == 'L4S1':
        Stim = 'SL'
        Thresholds = [max(Data)*0.9, max(Data)*0.6, max(Data)*0.1]
        SoundLight, Light, Sound = [np.where((Data[:-1] < T)*(Data[1:] > T))[0] for T in Thresholds]

    elif Fix == 'FakeTTLs':
        Stim = 'SL'
        Rise = np.where((Data[:-1] < max(Data)*0.7)*(Data[1:] > max(Data)*0.7))[0]
        Fall = np.where((Data[:-1] > max(Data)*0.7)*(Data[1:] < max(Data)*0.7))[0]
        TTLs = [DataAnalysis.GenFakeTTLsRising(Rate, 0.003, 0.004, 0.093, Rise[S], (Fall[S]-Rise[S])//2500) for S in range(len(Rise))]
        TTLs = [b for a in TTLs for b in a]
        SoundLight, Light, Sound = TTLs, TTLs, Rise

    elif Fix == 'FakeTTLsOnset':
        Stim = 'S'
        Start = np.where((Data[:-1] < max(Data)*0.7)*(Data[1:] > max(Data)*0.7))[0]
        Sound = [DataAnalysis.GenFakeTTLsRising(Rate, 0.003, 0.004, 0.093, S, 200) for S in Start]
        Sound = [b for a in Sound for b in a]

    elif Fix == 'MinDist':
        Stim = 'S'
        TTLs = np.where((Data[:-1] < max(Data)*0.7)*(Data[1:] > max(Data)*0.7))[0]
        Sound = [TTLs[t] for t in range(len(TTLs)-1) if TTLs[t+1]-TTLs[t] > 1000] + TTLs[-1]


    if Fix and Stim == 'SL':
        Sound = [_ for _ in Sound if _ not in SoundLight]
        Light = [_ for _ in Light if _ not in SoundLight]
        return(SoundLight, Sound, Light)

    elif Fix and Stim == 'S':
        return(Sound)

    elif not Fix:
        TTLs = np.where((Data[:-1] < max(Data)*0.7)*(Data[1:] > max(Data)*0.7))[0]
        return(TTLs)

    return(TTLs)


#%% Experiment information
DataDir = '/run/media/malfatti/Malfatti1TB3/Data'
AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'

Animals = dict(
    CaMKIIaChR2n20 = [
        [DataDir+'/OptogeneticExcitation/20140902-CaMKIIaChR2n20-UnitRec/Intan/s45Light_155245.int',
          # DataDir+'/OptogeneticExcitation/20140902-CaMKIIaChR2n20-UnitRec/Intan/s45SoundLight_155903.int',
          DataDir+'/OptogeneticExcitation/20140902-CaMKIIaChR2n20-UnitRec/Intan/s45Sound_155441.int'],
        [DataDir+'/OptogeneticExcitation/20140902-CaMKIIaChR2n20-UnitRec/Intan/s4mmS_154056.int',
          # DataDir+'/OptogeneticExcitation/20140902-CaMKIIaChR2n20-UnitRec/Intan/s4mm_154456.int',
          DataDir+'/OptogeneticExcitation/20140902-CaMKIIaChR2n20-UnitRec/Intan/s4mmL_153644.int']
    ],

    CaMKIIaChR2n21 = [
        [DataDir+'/OptogeneticExcitation/20140917-CaMKIIaChR2n21-UnitRec/Intan/n42L_161907.int',
          DataDir+'/OptogeneticExcitation/20140917-CaMKIIaChR2n21-UnitRec/Intan/n42S_162158.int',
          DataDir+'/OptogeneticExcitation/20140917-CaMKIIaChR2n21-UnitRec/Intan/n42SL_162727.int'],
        [DataDir+'/OptogeneticExcitation/20140917-CaMKIIaChR2n21-UnitRec/Intan/n45L_154601.int',
          DataDir+'/OptogeneticExcitation/20140917-CaMKIIaChR2n21-UnitRec/Intan/n45S_154908.int',
          DataDir+'/OptogeneticExcitation/20140917-CaMKIIaChR2n21-UnitRec/Intan/n45SL_160259.int']
    ],

    CaMKIIaChR2n22 = [
        [DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n35L_141027_171433.int',
          DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n35LS_141027_174533.int',
          DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n35S_141027_171722.int'],
        [DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n40L_141027_175250.int',
          DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n40LS_141027_175934.int',
          DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n40S_141027_175540.int'],
        [DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n43L_141027_180804.int',
          DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n43LS_141027_181511.int',
          DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/Intan/n43S_141027_181114.int']
    ],

    CaMKIIaChR2n23 = [
        [DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-40L_170540.int',
          DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-40S_170252.int',
          DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-40SL_173656.int'],
        [DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-42L_174424.int',
          DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-42S_174140.int',
          DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-42SL_175200.int'],
        [DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-45L_180359.int',
          DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-45S_180035.int',
          DataDir+'/OptogeneticExcitation/20150529-CaMKIIaChR2n23-UnitRec/Intan/CaMKIIaChR2n23-45SL_180736.int']
    ],

    CaMKIIaChR2n24 = [
        [DataDir+'/OptogeneticExcitation/20150602-CaMKIIaChR2n24-UnitRec/Intan/CaMKIIaChR2n24-415L_172612.int',
          DataDir+'/OptogeneticExcitation/20150602-CaMKIIaChR2n24-UnitRec/Intan/CaMKIIaChR2n24-415S_172855.int',
          DataDir+'/OptogeneticExcitation/20150602-CaMKIIaChR2n24-UnitRec/Intan/CaMKIIaChR2n24-415SL_173131.int'],
        [DataDir+'/OptogeneticExcitation/20150602-CaMKIIaChR2n24-UnitRec/Intan/CaMKIIaChR2n24-45L_173705.int',
          DataDir+'/OptogeneticExcitation/20150602-CaMKIIaChR2n24-UnitRec/Intan/CaMKIIaChR2n24-45S_174001.int',
          DataDir+'/OptogeneticExcitation/20150602-CaMKIIaChR2n24-UnitRec/Intan/CaMKIIaChR2n24-45SL_174333.int'],
        [DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4000Bo_142407.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4000L_143307.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4000S_134214.int'],
        [DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4300Bo_144945.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4300L_144628.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4300S_144151.int'],
        [DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4500Bo_150305.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4500L_145953.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/CChR2-4500S_145653.int'],
        [DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/MS-4000Bo_162644.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/MS-4000L_162402.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/MS-4000S_162023.int'],
        [DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/MS-4500Bo_165731.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/MS-4500L_165441.int',
          DataDir+'/OptogeneticExcitation/20150923-CaMKIIaChR2n24-UnitRec/Intan/MS-4500S_165152.int']
    ],

    CaMKIIaArch3n01 = [
        [DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch367L_143837.int',
          DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch367S_143507.int',
          DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch367SL_144132.int'],
        [DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch40L_150233.int',
          DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch40S_145922.int',
          DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch40SL_150909.int'],
        [DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch45L_153548.int',
          DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch45S_153244.int',
          DataDir+'/OptogeneticInhibition/20150319-CaMKIIaArch3n01-UnitRec/Intan/Arch45SL_153835.int']
    ],

    CaMKIIaArch3n02 = [
        [DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanLightOnly/Arch3n2-35L_180342.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-35S_175902.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-35SL_180751.int'],
        [DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanLightOnly/Arch3n2-40L_184008.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-40S_184417.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-40SL_183521.int'],
        [DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanLightOnly/Arch3n2-43L_185924.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-43LS_185520.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-43S_184905.int'],
        [DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanLightOnly/Arch3n2-45L_190432.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-45S_191253.int',
          DataDir+'/OptogeneticInhibition/20150416-CaMKIIaArch3n02-UnitRec/IntanSound/Arch3n2-45SL_190841.int']
    ],

    CaMKIIaArch3n03 = [
        [DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanTroubles/Arch3n3-x2-37L_143710.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanTroubles/Arch3n3-x2-37LS_143226.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanTroubles/Arch3n3-x2-37S_144131.int'],
        [DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanLightOnly/Arch3n3-40L_124123.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanSound/Arch3n3-40LS_123724.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanSound/Arch3n3-40S_123259.int'],
        [DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanLightOnly/Arch3n3-43L_124636.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanSound/Arch3n3-43S_125706.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanSound/Arch3n3-43SL_125126.int'],
        [DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanLightOnly/Arch3n3-45L_131728.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanSound/Arch3n3-45LS_131210.int',
          DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/IntanSound/Arch3n3-45S_130436.int']
    ],

    CaMKIIaArch3n04 = [
        [DataDir+'/OptogeneticInhibition/20150604-CaMKIIaArch3n04-UnitRec/Intan/CaMKIIaArch3n4-35S_190501.int',
          DataDir+'/OptogeneticInhibition/20150604-CaMKIIaArch3n04-UnitRec/Intan/CaMKIIaArch3n4-35SL_190800.int'],
        [DataDir+'/OptogeneticInhibition/20150604-CaMKIIaArch3n04-UnitRec/Intan/CaMKIIaArch3n4-43S_191146.int',
          DataDir+'/OptogeneticInhibition/20150604-CaMKIIaArch3n04-UnitRec/Intan/CaMKIIaArch3n4-43SL_191426.int'],
        [DataDir+'/OptogeneticInhibition/20150604-CaMKIIaArch3n04-UnitRec/Intan/CaMKIIaArch3n4-45S_191823.int',
          DataDir+'/OptogeneticInhibition/20150604-CaMKIIaArch3n04-UnitRec/Intan/CaMKIIaArch3n4-45SL_192118.int']
    ],

    DIOChR2n03 = [
        [DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n37_L_164402.int',
          DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n37_S_163002.int',
          DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n37_SL_164658.int'],
        [DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n40_L_165345.int',
          DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n40_S_165839.int',
          DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n40_SL_170356.int'],
        [DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n43_L_172757.int',
          DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n43_S_172341.int',
          DataDir+'/OptogeneticExcitation/20141112-DIOChR2n03-UnitRec/Intan/n43_SL_173437.int']
    ],

    DIOChR2n04 = [
        [DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/L3.5_161903.int',
          DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/LS3.5_163031.int',
          DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/S3.5_162410.int'],
        [DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/L4_163724.int',
          DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/LS4_164501.int',
          DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/S4_164024.int'],
        [DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/L4.5_165842.int',
          DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/LS4.5_170614.int',
          DataDir+'/OptogeneticExcitation/20141121-DIOChR2n04-UnitRec/Intan/S4.5_170146.int']
    ],

    DIOChR2n05 = [
        [DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-315L_123316.int',
          DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-315S_124102.int',
          DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-315SL_124440.int'],
        [DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-45L_130351.int',
          DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-45S_130711.int',
          DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-45SL_131034.int'],
        [DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-45L_134651.int',
          DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-45S_134957.int',
          DataDir+'/OptogeneticExcitation/20150602-DIOChR2n05-UnitRec/Intan/DioChR2n5-45SL_135241.int']
    ],
)

Stims = dict(
    CaMKIIaChR2n20 = [['Light', 'Sound'], ['Sound', 'Light']],
    CaMKIIaChR2n21 = [['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight']],
    CaMKIIaChR2n22 = [['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound']],
    CaMKIIaChR2n23 = [['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight']],
    CaMKIIaChR2n24 = [['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight'],
                      ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound'],
                      ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound']],

    CaMKIIaArch3n01 = [['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight']],
    CaMKIIaArch3n02 = [['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight'],
                       ['Light', 'SoundLight', 'Sound'], ['Light', 'Sound', 'SoundLight']],
    CaMKIIaArch3n03 = [['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
                       ['Light', 'Sound', 'SoundLight'], ['Light', 'SoundLight', 'Sound']],
    CaMKIIaArch3n04 = [['Sound', 'SoundLight'], ['Sound', 'SoundLight'], ['Sound', 'SoundLight']],

    DIOChR2n03 = [['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight']],
    DIOChR2n04 = [['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound']],
    DIOChR2n05 = [['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight'], ['Light', 'Sound', 'SoundLight']],
)

# PrmOrder = [
#     ['Light', 'Sound'], ['Light', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Light', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['Light', 'SoundLight', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['SoundLight', 'Light', 'Sound'], ['Light', 'SoundLight', 'Sound'],
#     ['', '', ''],
#     ['SoundLight', 'Light', 'Sound'], ['Light', 'SoundLight', 'Sound'], ['SoundLight', 'Light', 'Sound'],
#     ['SoundLight', 'Light', 'Sound'], ['SoundLight', 'Sound'], ['SoundLight', 'Sound']
# ]

TTLFix = [
    ['', ''], ['', ''],
    ['', 'L2S8', ''], ['', 'L2S8', ''],
    ['L1S4', '', ''], ['L1S4', '', ''], ['L1S4', '', ''],
    ['', 'L1S4', ''], ['', 'L1S4', ''], ['', 'L1S4', ''],
    ['', 'L1S4', ''], ['', 'L1S4', ''], ['', 'L1S4', ''],
    ['', 'L4S1', ''], ['', 'L4S1', ''], ['', 'L4S1', ''],
    ['', 'L4S1', ''], ['', 'L4S1', ''],
    ['', 'L4S1', ''], ['', 'L4S1', ''], ['', 'L4S1', ''],
    ['', '', ''], ['L1S2', '', ''], ['', '', ''], ['L1S2', '', 'MinDist'], ['L1S2', '', ''],
    ['', 'FakeTTLs', 'NoTTLs'], ['', 'FakeTTLs', 'NoTTLs'], ['', 'FakeTTLs', 'NoTTLs'],
    ['', 'FakeTTLs', 'FakeTTLsOnset'], ['', 'FakeTTLs', 'FakeTTLsOnset'], ['FakeTTLs', '', 'FakeTTLsOnset'], ['', 'FakeTTLs', 'FakeTTLsOnset'],
    ['FakeTTLs', '', 'FakeTTLsOnset'],
    ['FakeTTLs', '', 'FakeTTLsOnset'], ['', 'FakeTTLs', 'FakeTTLsOnset'], ['FakeTTLs', '', 'FakeTTLsOnset'],
    ['L1S2', ''], ['L1S2', ''], ['L1S2', '']
]

N = dict(
    CaMKIIaChR2 = 5,
    DIOChR2 = 3,
    CaMKIIaArch3 = 4
)


#%% GetFeatures
ProbeChSpacing = 50
TimeBeforeTTL = 50
TimeAfterTTL = 50
BinSize = 1
AnalogTTLs = True
ISISize = 0.01

# DataDir and TTLFix are all in the last cell

PrmFiles = sorted(glob(AnalysisFolder+'/Optog*/**/*.prm', recursive=True))
for P, PrmFile in enumerate(PrmFiles):
    # if P < 29: continue
    if 'CaMKIIaChR2n23' not in PrmFile: continue
    Prm = load_source('Prm', PrmFile)
    AnalysisPath = '/'.join(PrmFile.split('/')[:-2]) + '/Units'
    AnalysisFile = AnalysisPath + '/'+ PrmFile.split('/')[-3]
    KwikFile = PrmFile[:-3]+'kwik'
    Exp = PrmFile.split('/')[-1][:-4]

    # if glob(AnalysisFile + '_' + Exp + '_AllUnits.asdf'): continue

    Clusters = KwikModel(KwikFile)
    Rate = np.array(Clusters.sample_rate)
    Offsets = Clusters.all_traces.offsets
    Good = [Id for Id,Key in Clusters.cluster_groups.items() if Key == 'good']
    RecLengths = [int(Offsets[_]/Rate-Offsets[_-1]/Rate) for _ in range(1,len(Offsets))]

    #== Verify noise units
    # RealGood = [Id for Id,Key in Clusters.cluster_groups.items() if Key == 'good']
    # Good = [[Id,Key] for Id,Key in Clusters.cluster_groups.items() if Id not in RealGood]
    # GoodK = [_[1] for _ in Good]
    # Good = [_[0] for _ in Good]
    #==

    # Overrides - Good units in noise clusters
    if PrmFile == AnalysisFolder+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/KlustaFiles/Exp00.prm': Good += [5,8,12,16,19,21,37,40,42]
    if PrmFile == AnalysisFolder+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/KlustaFiles/Exp01.prm': Good += [5,6,12,13,20,28,30,31,37,41,47,48,49,56,62,65,67]
    if PrmFile == AnalysisFolder+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/KlustaFiles/Exp02.prm': Good += [7,10,13,17,23,37,39,44,47,53,55,56,58]

    if not Good: continue
    if not BinSize: BinSize = 1000/Rate
    PSTHX = np.arange(-TimeBeforeTTL, TimeAfterTTL, BinSize)
    RasterX = np.arange(PSTHX[0], PSTHX[-1], 1000/Rate)

    UnitRec = {}
    for K in ['UnitId', 'DV']: UnitRec[K] = np.zeros((len(Good)*len(Clusters.recordings)), dtype=np.int16)
    for K in ['dB', 'SpkResp']: UnitRec[K] = np.zeros((len(Good)*len(Clusters.recordings)), dtype=np.float32)
    for K in ['Freq', 'StimType']: UnitRec[K] = ['0' for _ in range(len(Good)*len(Clusters.recordings))]
    # for K in ['PSTH', 'Raster', 'Spks', 'FiringRate']: UnitRec[K] = [0 for _ in range(len(Good)*len(Clusters.recordings))]

    UnitRec['RMSs'] = np.zeros((len(Clusters.channels), len(Good)*len(Clusters.recordings)), dtype=np.float32)
    UnitRec['PSTHX'] = np.zeros((len(PSTHX),len(Good)*len(Clusters.recordings)), dtype=np.float32)
    UnitRec['RasterX'] = np.zeros((len(RasterX),len(Good)*len(Clusters.recordings)), dtype=np.float32)


    for rec, Rec in enumerate(Clusters.recordings):
        Fix = TTLFix[P][rec]
        if Fix == 'NoTTLs': print('No TTLs, cannot generate PSTH. Skipping...'); continue

        DataFile = Prm.traces['raw_data_files'][rec]
        if 'cerebro' in DataFile:
            DataFile = '/'.join(PrmFile.split('/')[:7])+'/'+'/'.join(DataFile.split('/')[6:])
        if '/Data/' in DataFile:
            DataFile = '/'.join(PrmFile.split('/')[:6])+'/'+'/'.join(DataFile.split('/')[6:])

        TTLFile = '.'.join(DataFile.split('.')[:-1])+'_TTLs.dat'

        print(DataFile)
        print('Loading TTLs... ')
        if not glob(TTLFile):
            TTLs = IO.Bin.Read(DataFile)[0]
            print('Quantifying... ')
            TTLs = QuantifyTTLs(TTLs[:,-1], Fix)
            if type(TTLs) == tuple: TTLs = TTLs[0]
            if type(TTLs) != np.ndarray: TTLs = np.array(TTLs)
            print('Saving to', TTLFile.split('/')[-1], '...')
            IO.Bin.Write(TTLs, TTLFile)
        else:
            TTLs = IO.Bin.Read(TTLFile)[0]
        print('Done.')

        for I, Id in enumerate(Good):
            Ind = I + (len(Good)*rec)
            SpksId = (Clusters.spike_clusters == Id) & \
                     (Clusters.spike_recordings == Rec)

            ## PSTH
            PSTH = Units.PSTHLine(Clusters.spike_samples[SpksId], TTLs, Rate, PSTHX, Offsets[rec])
            Raster = Units.Raster(Clusters.spike_samples[SpksId], TTLs, Rate, RasterX, Offsets[rec])

            ## Waveforms
            Spks = Clusters.all_waveforms[SpksId]
            ChNo = Spks.shape[2]
            RMSs = [(np.nanmean((np.nanmean(Spks[:, :, Ch], axis=0))**2))**0.5
                    for Ch in range(ChNo)]
            if RMSs[0] != RMSs[0]: print('Spk contains NaNs. Skipping...'); continue

            ## Firing rate and ISI
            FiringRate = Units.FiringRate(Clusters.spike_times[SpksId], RecLengths[rec], Offsets[rec]/Rate)
            ISI = Units.ISI(Clusters.spike_times[SpksId], ISISize, Rate)

            UnitRec['UnitId'][Ind] = Id
            UnitRec['DV'][Ind] = RMSs.index(max(RMSs))
            UnitRec['Freq'][Ind] = '5000-15000' if 'Sound' in Prm.DataInfo['Stims'][rec] else '634THz'
            UnitRec['dB'][Ind] = 80 if 'Sound' in Prm.DataInfo['Stims'][rec] else 0
            UnitRec['StimType'][Ind] = Prm.DataInfo['Stims'][rec]
            UnitRec['RMSs'][:, Ind] = RMSs
            UnitRec['PSTHX'][:,Ind] = PSTHX
            UnitRec['RasterX'][:,Ind] = RasterX
            # UnitRec['Spks'][Ind] = Spks
            # UnitRec['PSTH'][Ind] = PSTH
            # UnitRec['Raster'][Ind] = Raster
            # UnitRec['FiringRate'][Ind] = FiringRate

            if Id != 0:
                Keys = ['ISI', 'PSTH', 'Raster', 'Spks', 'FiringRate']
                for K in Keys:
                    IO.Bin.Write(locals()[K], AnalysisFile + '_' + Exp + '_AllUnits/'+K+'/'+K+'_'+str(Ind)+'.dat')

            del(Spks, PSTH, Raster, ISI, FiringRate)

    # Cleaning
    ValidIds = UnitRec['UnitId'] != 0

    for Key in ['Freq', 'StimType']: UnitRec[Key] = np.array(UnitRec[Key])

    for Key in ['UnitId', 'DV', 'dB', 'Freq', 'StimType']:
        UnitRec[Key] = UnitRec[Key][ValidIds]

    for Key in ['RMSs', 'PSTHX', 'RasterX']:
        UnitRec[Key] = UnitRec[Key][:,ValidIds]

    # for Key in ['Spks', 'PSTH', 'Raster', 'FiringRate']:
    #     UnitRec[Key] = [V for S, V in enumerate(UnitRec[Key]) if ValidIds[S]]

    if UnitRec['UnitId'].size:
        IO.Bin.Write(UnitRec, AnalysisFile + '_' + Exp + '_AllUnits')
    # IO.Asdf.Write(UnitRec, AnalysisFile + '_' + Exp + '_AllUnits.asdf')


#%% PlotFeatures
Show = False
Save = True
Ext = ['pdf']
BinSize = None

# AsdfFiles = sorted(glob(AnalysisFolder+'/Optog*/**/*nits.asdf', recursive=True))
# AsdfFiles = sorted(glob(os.environ['ANALYSISPATH'] +'/Optog*/**/*nits.asdf', recursive=True))
AsdfFiles = sorted(glob('/home/malfatti/Documents/???/Analysis/Optog*/*/Units/*AllUnits.asdf', recursive=True))

FR = {K: [] for K in ['Animal', 'Exp', 'UnitId', 'StimType', 'FR']}
for A, AsdfFile in enumerate(AsdfFiles):
    # if A <14: continue
    # if 'CaMKIIaArch3n03' not in AsdfFile: continue
    UnitRec = IO.Asdf.Read(AsdfFile)
    if not UnitRec['Raster']: continue

    # New data override
    UnitRec['StimType'][UnitRec['StimType'] == 'Laser'] = 'Light'
    UnitRec['StimType'][UnitRec['StimType'] == 'Sound_Laser'] = 'SoundLight'
    if '/PhD/' in AsdfFile: UnitRec = Units.MergeUnits(UnitRec, AsdfFile, Save)


    CellsResponse = Units.GetCellsResponse(UnitRec)
    Rate = round(1/(UnitRec['RasterX'][1,0]-UnitRec['RasterX'][0,0]))*1000

    IDs = np.unique(UnitRec['UnitId'])
    FigPath = '/'.join(AsdfFile.split('/')[:-2]) + '/Plots'
    Exp = AsdfFile.split('/')[-1][:-14]

    if BinSize:
        UnitRec['PSTHX'] = np.tile(np.arange(UnitRec['PSTHX'][:,0][0],
                                             UnitRec['PSTHX'][:,0][-1],
                                             BinSize),
                                   (UnitRec['PSTHX'].shape[1],1)).T

        for i in range(len(UnitRec['PSTH'])):
            UnitRec['PSTH'][i] = np.vstack((UnitRec['PSTH'][i], np.zeros((1,UnitRec['PSTH'][i].shape[1]))))
            UnitRec['PSTH'][i] = UnitRec['PSTH'][i].reshape((int(UnitRec['PSTH'][i].shape[0]/(BinSize*Rate/1000)),
                                                             int(BinSize*Rate/1000),
                                                             UnitRec['PSTH'][i].shape[1])).sum(axis=1)

            UnitRec['PSTH'][i] = UnitRec['PSTH'][i][:-1,:]

    for I, Id in enumerate(IDs):
        try:
            Ind = np.array(
                [np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] == 'Sound'))[0][0],
                 np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] == 'Light'))[0][0],
                 np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] == 'SoundLight'))[0][0]]
            )
        except IndexError:
            # Arch override
            Ind = np.where((UnitRec['UnitId'] == Id))[0]
            if 'arch' in AsdfFile.lower():
                Ind = np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] != 'Light'))[0]
                ExtraInfo = {'Opsin': 'Arch'}
            elif 'chr2' in AsdfFile.lower():
                ExtraInfo = {'Opsin': 'ChR2'}
        else:
            # Arch override
            if 'arch' in AsdfFile.lower():
                Ind = Ind[[0,2]]
                ExtraInfo = {'Opsin': 'Arch'}
            elif 'chr2' in AsdfFile.lower():
                ExtraInfo = {'Opsin': 'ChR2'}

        for i in Ind:
            print(Exp+'_Unit'+"{0:04d}".format(Id)+'_AllRecs')
            FR['FR'].append(UnitRec['FiringRate'][i])
            FR['StimType'].append(UnitRec['StimType'][i])
            FR['UnitId'].append(UnitRec['UnitId'][i])
            FR['Exp'].append(Exp.split('_')[-1])
            FR['Animal'].append(Exp.split('-')[1])

        Stims = UnitRec['StimType'][Ind]

        if sorted(Stims.tolist()) == ['Sound', 'SoundLight']:
            Stims = sorted(Stims)
        elif sorted(Stims.tolist()) == ['Light', 'Sound', 'SoundLight']:
            Stims[:] = np.array(sorted(Stims))[[1,0,2]]


        ThisId = {_:[] for _ in ['StimWindow', 'StimType']}

        for Stim in Stims:
            if Stim == 'Sound':
                ThisId['StimWindow'].append([[0,3]])
                ThisId['StimType'].append([Stim])
            elif Stim == 'Light':
                ThisId['StimWindow'].append([[0,10]])
                ThisId['StimType'].append([Stim])
            else:
                ThisId['StimWindow'].append([[0,10],[4,7]])
                ThisId['StimType'].append(['Sound', 'Light'])

        FigFile = FigPath+'/'+Exp+'_Unit'+"{0:04d}".format(Id)+'_AllRecs'
        # Debug
        print(Exp+'_Unit'+"{0:04d}".format(Id)+'_AllRecs')
        print(UnitRec['StimType'][Ind])
        # print(UnitRec['SpkResp'][Ind])
        print('')

        ExtraInfo['SpkResp'] = [CellsResponse['SpkResp'][_] for _ in Ind]
        ExtraInfo['FiringRate'] = [UnitRec['FiringRate'][_] for _ in Ind]

        # Plot only PSTH
        # for i,I in enumerate(Ind):
        #     if ThisId['StimType'][i] != ['Light']: continue
        #     PSTHWindow = [0, 60]
        #     Lim = (UnitRec['PSTHX'][:,I] >= PSTHWindow[0])*(UnitRec['PSTHX'][:,I] < PSTHWindow[1])
        #     FigFile = FigPath+'/'+Exp+'_Unit'+"{0:04d}".format(Id)+'-PSTH-'+'_'.join(ThisId['StimType'][i])
        #     print(FigFile)
        #     KlPlot.LinePSTH(UnitRec['PSTH'][I][Lim[:-1],:], UnitRec['PSTHX'][Lim,I], ThisId['StimWindow'][i], ThisId['StimType'][i], None, {'xlim': PSTHWindow}, File=FigFile, Ext=Ext, Save=Save, Show=Show)

        # Plot Spikes, Raster and PSTH for all recs
        KlPlot.WF_Raster_PSTH_AllRecs([UnitRec['Spks'][_] for _ in Ind],
                                  [UnitRec['Raster'][_] for _ in Ind],
                                  UnitRec['RasterX'][:,Ind],
                                  [UnitRec['PSTH'][_] for _ in Ind],
                                  UnitRec['PSTHX'][:,Ind],
                                  Rate, ThisId['StimWindow'], ThisId['StimType'],
                                  None, ExtraInfo, FigFile, Ext=Ext, Save=Save, Show=Show)

    del(UnitRec)



#%% QuantifyUnits
AsdfFiles = dict(
    CaMKIIaChR2 = sorted(glob(AnalysisFolder+'/Optog*/*CaMK*ChR2*/**/*.asdf', recursive=True)),
    DIOChR2 = sorted(glob(AnalysisFolder+'/Optog*/*DI*ChR2*/**/*.asdf', recursive=True)),
    CaMKIIaArch3 = sorted(glob(AnalysisFolder+'/Optog*/*CaMK*Arch*/**/*.asdf', recursive=True))
)


R, RId = Kl.QuantifyPerOpsinPerStim(AsdfFiles, Save=False, Return=True)


#%% QuantifyUnits - New dataset
AsdfFiles = dict(
    CaMKIIaChR2 = sorted(glob(AnalysisFolder+'/Optog*/*Optog*/**/*AllUnits.asdf', recursive=True))

)

R, RId = Kl.QuantifyPerOpsinPerStim(AsdfFiles, Save=True, Return=True)



#%% Convert to Bin
AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'
AsdfFiles = sorted(glob(AnalysisFolder+'/Optog*/*/Units/*AllUnits.asdf', recursive=True))

ToReRun = []
for A, AsdfFile in enumerate(AsdfFiles):
    try:
        UnitRec = IO.Asdf.Read(AsdfFile)
    except:
        ToReRun.append(AsdfFile)
        continue

    if not UnitRec['Raster']: continue
    UnitRec = {K: V for K,V in UnitRec.items() if K not in ['asdf_library', 'history']}

    Keys = ['PSTH', 'Raster', 'Spks', 'FiringRate']
    for K in Keys:
        for El,Val in enumerate(UnitRec[K]):
            IO.Bin.Write(Val, AsdfFile[:-5]+'/'+K+'/'+K+'_'+str(El)+'.dat')

    UnitRec = {K: V for K,V in UnitRec.items() if K not in Keys}
    IO.Bin.Write(UnitRec, AsdfFile[:-5])


#%% Get all firing rate
AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'
PrmFiles = sorted(glob(AnalysisFolder+'/Optog*/*/Klus*/*prm', recursive=True))

FR = {K: [] for K in ['Animal', 'Exp', 'UnitId', 'FR-Sound', 'FR-Light', 'FR-SoundLight']}
for P, PrmFile in enumerate(PrmFiles):
    # if P < 19: continue
    UnitRec = Kl.UnitRecLoad(PrmFile)
    if not len(UnitRec['Raster']): continue

    # New data override
    UnitRec['StimType'][UnitRec['StimType'] == 'Laser'] = 'Light'
    UnitRec['StimType'][UnitRec['StimType'] == 'Sound_Laser'] = 'SoundLight'
    if '/PhD/' in PrmFile: UnitRec = Units.MergeUnits(UnitRec, PrmFile, False)

    CellsResponse = Kl.GetCellsResponse(UnitRec)

    IDs = np.unique(UnitRec['UnitId'])
    Exp = PrmFile.split('/')[-1][:-4]

    for I, Id in enumerate(IDs):
        try:
            Ind = np.array(
                [np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] == 'Sound'))[0][0],
                 np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] == 'Light'))[0][0],
                 np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] == 'SoundLight'))[0][0]]
            )
        except IndexError:
            # Arch override
            Ind = np.where((UnitRec['UnitId'] == Id))[0]
            if 'arch' in PrmFile.lower():
                Ind = np.where((UnitRec['UnitId'] == Id)*(UnitRec['StimType'] != 'Light'))[0]
        else:
            # Arch override
            if 'arch' in PrmFile.lower(): Ind = Ind[[0,2]]

        if min([CellsResponse['SpkResp'][i] for i in Ind]) > 0.05: continue

        FR['UnitId'].append(UnitRec['UnitId'][Ind[0]])
        FR['Exp'].append(Exp)
        FR['Animal'].append(PrmFile.split('/')[-3].split('-')[1])

        print(
            '['+FR['Animal'][-1]+']',
            '['+FR['Exp'][-1]+']',
            '[Unit'+"{0:04d}".format(FR['UnitId'][-1])+']'
        )

        ToDo = ['FR-Sound', 'FR-Light', 'FR-SoundLight']
        for i in Ind:
            IsStr = 'str' in str(type(UnitRec['FiringRate'][i]))
            UnitFR = IO.Bin.Read(UnitRec['FiringRate'][i])[0] if IsStr else UnitRec['FiringRate'][i]

            if CellsResponse['SpkResp'][i] < 0.05:
                FR['FR-'+UnitRec['StimType'][i]].append(UnitFR.mean())
            else:
                FR['FR-'+UnitRec['StimType'][i]].append(float('NaN'))

            del(ToDo[ToDo.index('FR-'+UnitRec['StimType'][i])])

        for i in ToDo:
            FR[i].append(float('NaN'))


OE = [
    ['OptogeneticExcitation_14','Exp00',14,13.95,11.49,12.33],
    ['OptogeneticExcitation_14','Exp00',43,float('NaN'),17.54,float('NaN')],
    ['OptogeneticExcitation_14','Exp01',32,35.62,33.96,37.88],
    ['OptogeneticExcitation_14','Exp01',43,float('NaN'),37.35,float('NaN')],
    ['OptogeneticExcitation_14','Exp01',44,25.47,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_11','Exp00',22,55.31,52.54,52.28],
    ['OptogeneticExcitation_15','Exp00',3,4.31,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_15','Exp00',17,4.95,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_15','Exp00',19,1.53,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_15','Exp00',29,2.51,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_15','Exp00',34,1.7,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_15','Exp00',61,5.63,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_15','Exp00',71,13.91,float('NaN'),float('NaN')],
    ['OptogeneticExcitation_15','Exp00',75,20.42,float('NaN'),float('NaN')]
]

for L in OE:
    FR['Animal'].append(L[0])
    FR['Exp'].append(L[1])
    FR['UnitId'].append(L[2])
    FR['FR-Sound'].append(L[3])
    FR['FR-Light'].append(L[4])
    FR['FR-SoundLight'].append(L[5])

FR = {K: np.array(Key) for K,Key in FR.items()}



#%% Firing rate table
import pandas as pd
plt = KlPlot.UnitsPlot.Plot.Return('plt')
# pd.DataFrame(FR).to_csv('UnitsFR.csv')
dfs = pd.read_excel('/home/malfatti/Nebula/Documents/PhD/Papers/MalfattiEtAl2019/UnitsFR.xlsx', sheetname=None)
FR = np.array(dfs['Sheet4']['FR-Sound'].to_numpy().tolist())

# FigSize = list(KlPlot.UnitsPlot.Plot.FigSize)
# FigSize[0] *= 0.2
Fig, Ax = plt.subplots()
a = FR.copy()
a = a[~np.isnan(a)]
Ax = KlPlot.UnitsPlot.Plot.BoxPlots(a, ['0'], Ax=Ax)

Ax.plot(np.random.uniform(size=FR.shape[0])*0+1, FR, 'ko', alpha=0.4)
plt.show()



#%% Check all Firing patterns
import pandas as pd
plt = KlPlot.UnitsPlot.Plot.Return('plt')
# pd.DataFrame(FR).to_csv('UnitsFR.csv')
dfs = pd.read_excel('/home/malfatti/Nebula/Documents/PhD/Papers/MalfattiEtAl2019/UnitsFR.xlsx', sheetname=None)
FR = {K: np.array(dfs['Sheet4'][K].to_numpy().tolist())
      for K in ['Animal', 'Exp', 'UnitId', 'FR-Sound', 'FR-Light', 'FR-SoundLight']}

AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'

for U in range(FR['Exp'].shape[0]):
    Base = '/'.join([AnalysisFolder, 'Optog*', '*'+FR['Animal'][U]+'*', 'Units', '*'+FR['Exp'][U]+'*'])
    if not glob(Base):
        print(Base)
        print('AnalysisMissing')
        continue

    Unit = {
        K: IO.Bin.Read(glob('/'.join([Base, Key]))[0])[0]
        for K,Key in [('Ids', 'UnitId.dat'),('Stims', 'StimType.dat')]
    }

    if ~np.isnan(FR['FR-Sound'][U]):
        Stim = 'Sound'
    elif ~np.isnan(FR['FR-SoundLight'][U]):
        Stim = 'SoundLight'
    else:
        continue

    if FR['UnitId'][U] not in Unit['Ids']:
        print(Base)
        print('UnitMissing')
        continue

    UnitId = np.where((Unit['Ids'] == FR['UnitId'][U])*(Unit['Stims'] == Stim))[0][0]

    PSTH = IO.Bin.Read(glob('/'.join([Base, 'PSTH', 'PSTH_'+str(UnitId)+'.dat']))[0])[0]
    PSTHX = IO.Bin.Read(glob('/'.join([Base, 'PSTHX.dat']))[0])[0]

    Title = ' '.join([FR['Animal'][U], FR['Exp'][U], str(FR['UnitId'][U])])
    print(Title)
    KlPlot.UnitsPlot.LinePSTH(PSTH, PSTHX[:,0], [[0,3]], ['Sound'], AxArgs={'title':Title})


#%% Quantify FR
import pandas as pd
import numpy as np
from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')

# pd.DataFrame(FR).to_csv('UnitsFR.csv')
dfs = pd.read_excel('/home/malfatti/Nebula/Documents/PhD/Papers/MalfattiEtAl2019/UnitsFR.xlsx', sheetname=None)
FR = {K: np.array(dfs['Sheet4'][K].to_numpy().tolist())
      for K in ['Animal', 'Exp', 'UnitId', 'FR-Sound', 'FR-Light', 'FR-SoundLight', 'FiringPattern']}

FR['FiringPattern'][FR['FiringPattern'] == 'nan'] = ''

Total = FR['FiringPattern'][FR['FiringPattern'] != ''].shape[0]
FP = [
    FR['FiringPattern'][FR['FiringPattern'] == 'Pauser'].shape[0],
    FR['FiringPattern'][FR['FiringPattern'] == 'PrimaryLike'].shape[0],
    FR['FiringPattern'][FR['FiringPattern'] == 'NegativeResponder'].shape[0],
    FR['FiringPattern'][FR['FiringPattern'] == 'Chopper'].shape[0],
    FR['FiringPattern'][FR['FiringPattern'] == 'Onset'].shape[0],
    FR['FiringPattern'][FR['FiringPattern'] == 'DelayedOnset'].shape[0]
]

FRPerFP = [
    FR['FR-Sound'][FR['FiringPattern'] == 'Pauser'].mean(),
    FR['FR-Sound'][FR['FiringPattern'] == 'PrimaryLike'].mean(),
    FR['FR-Sound'][FR['FiringPattern'] == 'NegativeResponder'].mean(),
    FR['FR-Sound'][FR['FiringPattern'] == 'Chopper'].mean(),
    FR['FR-Sound'][FR['FiringPattern'] == 'Onset'].mean(),
    FR['FR-Sound'][FR['FiringPattern'] == 'DelayedOnset'].mean()
]

FPP = [_*100/Total for _ in FP]

Fig, Ax = plt.subplots(figsize=(2.5,3.2))
Ax.bar(range(len(FPP)), FPP, facecolor='r', edgecolor='r', alpha=0.7, width=0.7)
Ax.set_ylim([0,25])
Plot.Set(Fig=Fig, Ax=Ax)
Ax.set_xticks([])
Ax.spines['bottom'].set_visible(False)
Fig.savefig('FiringPatterns.pdf')
plt.show()

#%% for arch units not responding to sound

a = [
     # Increase
     [[0.29, 4.24], [1.92, 75.23], [0.21, 3.15], [2.1, 12.81], [20.92, 40.08], [15.21, 27.04]],
     # Decrease
     [[18.22, 0.86], [7.48, 2.47], [24.89, 3.05], [15.05, 2.5], [15.14, 8.5], [2.2, 0.24], [56.5, 26.07], [4.21, 0.56], [13.15, 6.76]]
]

b = [[round(_[1] - _[0], 6) for _ in b] for b in a]
c = [[round(np.mean(_),3), round(np.std(_)/(len(_)**0.5), 3)] for _ in b]
Mean = [np.round(np.mean(_, axis=0), 3) for _ in a]
SEM = [np.round(np.std(_, axis=0)/(len(_)**0.5), 3) for _ in a]



#%% PlotQuantification
Preffix = DataDir+'/Optogenetic'
Resp = IO.Txt.Read(Preffix+'_Resp.txt')
RespIDs = IO.Txt.Read(Preffix+'_RespIDs.txt')

KlPlot.Qnt_Opsin_Stims(
    Resp,
    ['CaMKIIaChR2', 'DIOChR2', 'CaMKIIaArch3'],
    [['Sound', 'Light', 'SoundLight', 'Both', 'LightMask'],
     ['Sound', 'Light', 'SoundLight', 'Both', 'LightMask'],
     ['Sound', 'SoundLight', 'LightMask']]

)



#%% ABRs
from DataAnalysis.ABRs import Single
from DataAnalysis.Plot import ABRs as ABRPlot
DataDir = '/run/media/malfatti/Malfatti1TB4/Analysis'
# File = DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/KlustaFiles/n35S_141027_171722_Exp00-02.dat'
File = DataDir+'/OptogeneticExcitation/20141027-CaMKIIaChR2n22-UnitRec/KlustaFiles/n35L_141027_171433_Exp00-00.dat'

Parameters = dict(
    ABRCh = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
    TTLCh = 17,
    TimeWindow = [-0.003, 0.012],
    FilterFreq = [600, 1500]
)

Data, Rate = IO.Bin.Read(File)
Rate = Rate['Rate']

ABR, X = Single(Data, Rate, **Parameters)
XLim = [_*1000 for _ in Parameters['TimeWindow']]
ABRPlot.Single(-ABR/1000, X, AllCh=True, AxArgs={'xlim': XLim}, Save=False)


#%% All ABRs
import os
from DataAnalysis.ABRs import ABRPerCh

TTLFix = [
    ['', ''], ['', ''],
    ['', 'L2S8', ''], ['', 'L2S8', ''],
    ['L1S4', '', ''], ['L1S4', '', ''], ['L1S4', '', ''],
    ['', 'L1S4', ''], ['', 'L1S4', ''], ['', 'L1S4', ''],
    ['', 'L1S4', ''], ['', 'L1S4', ''], ['', 'L1S4', ''],
    ['', 'L4S1', ''], ['', 'L4S1', ''], ['', 'L4S1', ''],
    ['', 'L4S1', ''], ['', 'L4S1', ''],
    ['', 'L4S1', ''], ['', 'L4S1', ''], ['', 'L4S1', ''],
    ['', '', ''], ['L1S2', '', ''], ['', '', ''], ['L1S2', '', 'MinDist'], ['L1S2', '', ''],

    ['', ''], ['', ''], ['', ''], ['', ''], ['', ''], ['', ''], ['', ''], ['', ''],

    ['', 'FakeTTLs', 'NoTTLs'], ['', 'FakeTTLs', 'NoTTLs'], ['', 'FakeTTLs', 'NoTTLs'],
    ['', 'FakeTTLs', 'FakeTTLsOnset'], ['', 'FakeTTLs', 'FakeTTLsOnset'], ['FakeTTLs', '', 'FakeTTLsOnset'], ['', 'FakeTTLs', 'FakeTTLsOnset'],
    ['FakeTTLs', '', 'FakeTTLsOnset'],
    ['FakeTTLs', '', 'FakeTTLsOnset'], ['', 'FakeTTLs', 'FakeTTLsOnset'], ['FakeTTLs', '', 'FakeTTLsOnset'],
    ['L1S2', ''], ['L1S2', ''], ['L1S2', '']
]

AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'

ABRCh = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
TimeWindow = [-0.003, 0.012]
FilterFreq = [600, 1500]
Freq = '5000-15000'
dB = '80'
Save = True

PrmFiles = sorted(glob(AnalysisFolder+'/Optog*/**/*.prm', recursive=True))
for P, PrmFile in enumerate(PrmFiles):
    # if P < 26: continue
    # if 'CaMKIIaC' not in PrmFile: continue
    AnalysisPath = '/'.join(PrmFile.split('/')[:-2]) + '/ABRs'

    Recs = [_.replace('/home/malfatti/Documents/MSc/Data', AnalysisFolder) for _ in Prm.traces['raw_data_files']]
    if 'experiment1/recording' in Recs[0]:
        Recs = np.unique(['/'.join(_.split('/')[:-5]) for _ in Recs]).tolist()

    for R, Rec in enumerate(Recs):
        if 'cerebro' in Rec:
            Rec = '/'.join(PrmFile.split('/')[:7])+'/'+'/'.join(Rec.split('/')[6:])
        if '/Data/' in Recs:
            Rec = '/'.join(PrmFile.split('/')[:6])+'/'+'/'.join(Rec.split('/')[6:])

        print(Rec.replace('/run/media/malfatti/Malfatti1TB4/Analysis/', ''))

        if 'OptogeneticExcitation/20181' in Rec:
            Data, Rate = IO.DataLoader(Rec, 'Bits')
            Proc = list(Data.keys())[0]
            Data, Rate = Data[Proc], Rate[Proc]

            TTLs = np.concatenate([Data[_][:,-1] for _ in sorted(Data.keys(), key=lambda x: int(x))])
            TTLs = QuantifyTTLs(TTLs, '')
            Data = np.concatenate([Data[_][:,:-2] for _ in sorted(Data.keys(), key=lambda x: int(x))])

        else:
            Fix = TTLFix[P][R]
            if Fix == 'NoTTLs': print('No TTLs, cannot generate PSTH. Skipping...'); continue

            TTLFile = '.'.join(Rec.split('.')[:-1])+'_TTLs.dat'
            print('Loading TTLs... ')
            if not glob(TTLFile):
                TTLs = IO.Bin.Read(Rec)[0]
                print('Quantifying... ')
                TTLs = QuantifyTTLs(TTLs[:,-1], Fix)
                if type(TTLs) == tuple: TTLs = TTLs[0]
                if type(TTLs) != np.ndarray: TTLs = np.array(TTLs)
                print('Saving to', TTLFile.split('/')[-1], '...')
                IO.Bin.Write(TTLs, TTLFile)
            else:
                TTLs = IO.Bin.Read(TTLFile)[0]
            print('Done.')

            Data, Rate = IO.Bin.Read(Rec)
            Rate = Rate['Rate']

        ABR = ABRPerCh(Data, Rate, TTLs, ABRCh, TimeWindow, FilterFreq)
        X = np.arange(int(TimeWindow[0]*Rate), int(TimeWindow[1]*Rate))*1000/Rate

        XSavePath = '/'.join([AnalysisPath, Freq])
        if os.path.isdir(XSavePath): Trial = 'Trial'+str(len(glob(XSavePath+'/Trial*')))
        else: Trial = 'Trial0'
        SavePath = '/'.join([XSavePath, Trial])

        if Save:
            print('Saving to', SavePath+'...')
            IO.Bin.Write(X, XSavePath + '/X.dat')
            IO.Bin.Write(ABR, SavePath+'/'+dB)
            print('Done.')

        print('='*20); print('')



#%% ABRs group analysis
AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'
Freq = '5000-15000'
dB = '80'

Folders = sorted(glob(AnalysisFolder+'/Optog*/**/*.prm', recursive=True))
Folders = np.unique(['/'.join(_.split('/')[:-2]) for _ in Folders])

Save = os.path.commonprefix(Folders.tolist())+'_ABRs'

ABRs = {'Light': [], 'Sound': [], 'SoundLight':[], 'SoundArch':[]}
X = []

for F, Folder in enumerate(Folders):
    print('/'.join(Folder.split('/')[-2:])+':')
    # if P < 26: continue
    # if 'CaMKIIaC' not in PrmFile: continue
    Prms = [load_source(_, _) for _ in sorted(glob(Folder+'/K*/*.prm'))]

    if 'OptogeneticExcitation/20181' in Folder:
        Stims = [
            ''.join(sorted(Prms[0].DataInfo['ExpInfo'][_]['StimType'], reverse=True)).replace('Laser', 'Light')
            for _ in sorted(Prms[0].DataInfo['ExpInfo'].keys())
        ]
    else:
        Stims = [Stim for Prm in Prms for Stim in Prm.DataInfo['Stims']]

    AnalysisPath = Folder + '/ABRs'
    Recs = glob('/'.join([AnalysisPath, Freq, 'Trial*']))
    Recs = sorted(Recs, key=lambda x: int(x.split('Trial')[-1]))

    if 'Arch' in Folder:
        if 'CaMKIIaArch3n01' in Folder:
            Stims = [_ for _ in Stims if _ != 'Sound']

        Remove = np.where((np.array(Stims) == 'Light'))[0]

        Stims = [Stim for S,Stim in enumerate(Stims) if S not in Remove]
        Stims = [_ if _ != 'SoundLight' else 'SoundArch' for _ in Stims]
        Recs = [Rec for R,Rec in enumerate(Recs) if R not in Remove]


    if len(Stims) != len(Recs):
        print('    Wrong number of stimulus/recs. Skipping...')
        continue

    for R, Rec in enumerate(Recs):
        if not len(X):
            X = glob('/'.join(Rec.split('/')[:-1])+'/X.dat')[0]
            X = IO.Bin.Read(X)[0]

        Stim = Stims[R]

        print('    '+'/'.join(Rec.split('/')[8:]), Stim)
        ABR = IO.Bin.Read(Rec)[0]
        BestCh = DataAnalysis.GetStrongestCh(ABR[dB])
        ABR = ABR[dB][:,BestCh]-ABR[dB].mean(axis=1)

        if 'OptogeneticExcitation/20181' in Folder:
            ABR = np.interp(
                np.arange(round(X[0]), round(X[-1]), 1/25),
                np.arange(round(X[0]), round(X[-1]), 1/30),
                ABR
            )


        ABR = ABR.reshape((len(ABR),1))

        if not len(ABRs[Stim]):
            ABRs[Stim] = ABR.copy()
        else:
            ABRs[Stim] = np.concatenate((ABRs[Stim], ABR), axis=1)

        print('='*20); print('')


if Save:
    print('Saving to', Save+'...')
    IO.Bin.Write(X, Save + '/X.dat')
    IO.Bin.Write(ABRs, Save)
    print('Done.')


#%% ABRs from "control"
import os

AnalysisFolder = '/run/media/malfatti/Malfatti1TB4/Analysis'
Freq = '8000-10000'
dB = '80'

Folders = glob(AnalysisFolder+'/Rec*/*ABRs')+glob(AnalysisFolder+'/Prev*/*ABRs')
ExpEpochs = IO.Txt.Read(AnalysisFolder+'/ExpEpochs.dict')
Folders = [_ for _ in Folders if _ in ExpEpochs['BeforeANT']]

Folders = np.random.permutation(Folders)
Save = os.path.commonprefix(Folders.tolist())+'ABRs_80dB'

ABRs = {'Sound': []}
X = []
Done = []

for F, Folder in enumerate(Folders):
    print('/'.join(Folder.split('/')[-2:])+':')
    # if P < 26: continue

    AnalysisPath = Folder + '/ABRs'
    Recs = glob('/'.join([AnalysisPath, Freq, 'Trial*']))
    if not Recs: continue

    Recs = sorted(Recs, key=lambda x: int(x.split('Trial')[-1]))
    Rec = Recs[0]

    if not len(X):
        X = glob('/'.join(Rec.split('/')[:-1])+'/X.dat')[0]
        X = IO.Bin.Read(X)[0]

    print('    '+'/'.join(Rec.split('/')[8:]))
    ABR = IO.Bin.Read(Rec)[0]

    if ABR[dB].shape[1] > 1:
        BestCh = DataAnalysis.GetStrongestCh(ABR[dB])
        ABR = ABR[dB][:,BestCh]-ABR[dB].mean(axis=1)
        ABR = ABR.reshape((len(ABR),1))
    else:
        ABR = ABR[dB]

    if not len(ABRs['Sound']):
        ABRs['Sound'] = ABR.copy()
    else:
        ABRs['Sound'] = np.concatenate((ABRs['Sound'], ABR), axis=1)

    print('='*20); print('')
    Done.append(F)
    if len(Done) >= 35: break


if Save:
    print('Saving to', Save+'...')
    IO.Bin.Write(X, Save + '/X.dat')
    IO.Bin.Write(ABRs, Save)
    print('Done.')


#%% ABR figure
from scipy.signal import savgol_filter
from DataAnalysis.Plot import Plot
plt = Plot.Return('plt')

ABRs = {
    'Opto': '/run/media/malfatti/Malfatti1TB4/Analysis/Optogenetic_ABRs',
    # 'Control': '/run/media/malfatti/Malfatti1TB4/Analysis/ABRs_80dB'
}

ABRs = {K: IO.Bin.Read(V)[0] for K,V in ABRs.items()}
ABRs = {K: {k: v for k,v in V.items() if k in ['Sound', 'SoundLight', 'X']} for K,V in ABRs.items()}

X = {K: V.pop('X')  for K,V in ABRs.items()}
Stims = {'Control': ['Sound'], 'Opto': ['Sound', 'SoundLight']}
XTicks = [-3, 0, 3, 6, 9, 12]

Size = list(Plot.FigSize)
# Size[1] *=1.5

Fig, Axes = plt.subplots(sum([len(_) for _ in ABRs.values()]), 3, figsize=Size)
StimColors = Plot.GetColors('StimColors')

for g,(G,Group) in enumerate(ABRs.items()):
    for S,Stim in enumerate(Stims[G]):
        L = g*len(Stims[G])+S

        ABR = Group[Stim]/1000

        Peaks = DataAnalysis.GetPeaks(ABR, 1)
        Peaks = [[P for P in Peak if X[G][P] > 2] for Peak in Peaks['Pos']]
        Peaks = [[Peak[0] if len(Peak) else []] + [P for p,P in enumerate(Peak) if P-Peak[p-1] > 10] for Peak in Peaks]
        Peaks = [[P,Peak] for P,Peak in enumerate(Peaks) if len(Peak) >= 5]
        Good = [_[0] for _ in Peaks]

        Peaks = {
            'Y': [ABR[Peak[1],Peak[0]].tolist() for Peak in Peaks],
            'X': [_[1] for _ in Peaks]
        }

        for K in Peaks.keys():
            Peaks[K] = [_ if len(_) == 5 else _[:5] for _ in Peaks[K]]
            Peaks[K] = np.array(Peaks[K]).T.tolist()
            Peaks[K] = [[_ for _ in Peak if _ == _] for Peak in Peaks[K]]

        Peaks['X'] = [[X[G][int(_)] for _ in Peak] for Peak in Peaks['X']]

        Axes[L][1] = Plot.BoxPlots(Peaks['Y'], ['I', 'II', 'III', 'IV', 'V'], Ax=Axes[L][1])
        Axes[L][2] = Plot.BoxPlots(Peaks['X'], ['I', 'II', 'III', 'IV', 'V'], Ax=Axes[L][2])

        if len(XTicks): Axes[L][0].set_xticks(XTicks)
        Axes[L][0].set_ylabel('Voltage [mV]')
        Axes[L][2].set_ylabel('Latency [ms]')
        Axes[L][2].set_yticks(np.arange(0,13,2))


        Mean = ABR[:,Good].mean(axis=1)
        MeanAmp = Mean.max()-Mean.min()
        Mean = savgol_filter(Mean, 11, 3)
        Mean /= (Mean.max()-Mean.min())/MeanAmp
        SEM = ABR.std(axis=1) / (ABR.shape[0]**0.5)


        Peaks = DataAnalysis.GetPeaks(Mean, 1)
        Peaks = [_ for _ in Peaks['Pos'] if X[G][_] > 1]
        Peaks = [Peaks[0]] + [Peak for P,Peak in enumerate(Peaks) if Peak-Peaks[P-1] > 10]

        Axes[L][0].fill_between(X[G], Mean+SEM, Mean-SEM, color=StimColors[Stim], lw=0, alpha=0.6)
        Axes[L][0].plot(X[G][Peaks], Mean[Peaks], 'k*')
        Axes[L][0].plot(X[G], Mean, 'k')
        # Axes[L][0].plot(X[G], DataAnalysis.Normalize(ABR), 'k', alpha=0.3)

for A in range(len(Axes)-1):
    Axes[A][0].plot([0, 3], [max(Axes[A][0].get_ylim())]*2, 'r', lw=4, alpha=0.8)

Axes[-1][0].plot([0, 3], [max(Axes[-1][0].get_ylim())*0.9]*2, 'r', lw=4, alpha=0.8)
Axes[-1][0].plot([-3, 6], [max(Axes[-1][0].get_ylim())]*2, 'b', lw=4, alpha=0.8)

# Axes[0][1].set_ylim([0,1.5])
Axes[-2][0].set_ylim([-8,8])
Axes[-2][0].set_yticks(np.arange(-8,9,4))
Axes[-1][0].set_ylim([-12,14])
Axes[-1][0].set_yticks(np.arange(-12,13,6))

for A in range(len(Axes)):
    Axes[A][1].set_ylim([0,75])
    Axes[A][1].set_yticks(np.arange(0,71,14))

for S in range(len(Axes)-1):
    for Ax in Axes[S]: Ax.set_xticklabels('')

for A in range(len(Axes[-1])-1): Axes[-1][A+1].set_xlabel('ABR peaks')

Axes[-1][0].set_xlabel('Time [ms]')
Axes[0][0].set_title('Mean ABR Trace')
Axes[0][1].set_title('Amplitudes')
Axes[0][2].set_title('Latencies')

plt.figtext(0, 1, 'A', fontsize=14, va='top')
plt.figtext(0, 0.5, 'B', fontsize=14)
# plt.figtext(0, 0.34, 'C', fontsize=14)

for Ax in Axes:
    for A in Ax: Plot.Set(Ax=A)

Plot.Set(Fig=Fig)
Fig.savefig('ABRs.pdf')
plt.show()


#%% P
from scipy import stats

P = {}
for G,Group in Stims.items():
    if G not in P.keys(): P[G] = []
    for S,Stim in enumerate(Group):
        ABR = ABRs[G][Stim]/1000

        Peaks = DataAnalysis.GetPeaks(ABR, 1)
        Peaks = [[P for P in Peak if X[G][P] > 2] for Peak in Peaks['Pos']]
        Peaks = [[Peak[0] if len(Peak) else []] + [P for p,P in enumerate(Peak) if P-Peak[p-1] > 10] for Peak in Peaks]
        Peaks = [[P,Peak] for P,Peak in enumerate(Peaks) if len(Peak) >= 5]
        Good = [_[0] for _ in Peaks]

        Peaks = {'Y': [ABR[Peak[1],Peak[0]].tolist() for Peak in Peaks]}

        for K in Peaks.keys():
            Peaks[K] = [_ if len(_) == 5 else _[:5] for _ in Peaks[K]]
            Peaks[K] = np.array(Peaks[K]).T.tolist()
            Peaks[K] = [[_ for _ in Peak if _ == _] for Peak in Peaks[K]]

        if Stim == 'Sound': P[G].append(Peaks['Y'])


P = [DataAnalysis.Normalize(np.array(P[K][0]).T) for K in ['Control', 'Opto']]
[stats.ttest_ind(P[0][:,_], P[1][:,_], equal_var=False)[1]*5 for _ in range(5)]

#%% Check a recording
File = DataDir + '/OptogeneticExcitation/20140917-CaMKIIaChR2n21-UnitRec/KlustaFiles/n42L_161907_Exp00-00.dat'
Data, Rate = IO.Bin.Read(File)
Rate = Rate['Rate']

from DataAnalysis.DataAnalysis import FilterSignal
plt = ABRPlot.Plot.Return('ṕlt')
Fig, Axes = plt.subplots(5,1, sharex=True)
for i,I in enumerate([0,1,2,3]):
    Axes[i].plot(FilterSignal(Data[:,I], Rate, [300, 6000]))
Axes[-1].plot(Data[:,-1])
plt.show()


#%% Fig 4
AsdfFile = DataDir+'/OptogeneticExcitation/Analysis-Bin2/20141121-DIOChR2n04-UnitRec/20141121-DIOChR2n04-UnitRec_Exp02_AllUnits.asdf'
Id = 9

UnitRec = IO.Asdf.Read(AsdfFile)
Rate = round(1/(UnitRec['RasterX'][1,0]-UnitRec['RasterX'][0,0]))*1000
FigPath = '/'.join(AsdfFile.split('/')[:-3]) + '/Figs/' + AsdfFile.split('/')[-2]
FigFile = '_'.join(AsdfFile.split('/')[-1].split('_')[:-1])+'_Unit'+"{0:04d}".format(Id)
# FigFile = FigPath+'/'+FigFile
Exp = AsdfFile.split('/')[-1][:-14]
Ind = np.where((UnitRec['UnitId'] == Id))[0]
Stims = UnitRec['StimType'][Ind]
Save, Show = False, True

ThisId = {_:[] for _ in ['StimWindow', 'StimType']}
for S,Stim in enumerate(Stims):
    if Stim == 'Sound':
        ThisId['StimWindow'].append([[0,3], [100,103], [200,203]])
        ThisId['StimType'].append([Stim]*3)
    elif Stim == 'Light':
        ThisId['StimWindow'].append([[0,10], [100,110], [200,210]])
        ThisId['StimType'].append([Stim]*3)
    else:
        ThisId['StimWindow'].append([[4,7], [104,107], [204,207], [0,10], [100,110], [200,210]])
        ThisId['StimType'].append(['Sound']*3 + ['Light']*3)

Min = round(min([min((UnitRec['PSTH'][Ind[S]].mean(axis=1)) + (UnitRec['PSTH'][Ind[S]].std(axis=1) / (UnitRec['PSTH'][Ind[S]].shape[0]**0.5))) for S,Stim in enumerate(Stims)]), 2)
Max = round(max([max((UnitRec['PSTH'][Ind[S]].mean(axis=1)) + (UnitRec['PSTH'][Ind[S]].std(axis=1) / (UnitRec['PSTH'][Ind[S]].shape[0]**0.5))) for S,Stim in enumerate(Stims)]), 2)
AxArgs = [[{},{},{},{'ylim':[Min,Max]}]]*3

# Plt = KlPlot.Plot.Return('plt')
# Fig, Axes = Plt.subplots(len(Stims), 2, figsize=(4,3.2*len(Stims)), dpi=100)
# for S,Stim in enumerate(Stims):
#     SpkX = np.arange(UnitRec['Spks'][Ind[S]].shape[1])*1000/Rate
#     Axes[S][0] = KlPlot.WF_BestCh(UnitRec['Spks'][Ind[S]], SpkX, DataAnalysis.UniqueStr(ThisId['StimType'][S], True), Axes[S][0], Save=False, Show=False)
#     Axes[S][1] = KlPlot.LinePSTH(UnitRec['PSTH'][Ind[S]], UnitRec['PSTHX'][:,Ind[S]], ThisId['StimWindow'][S], ThisId['StimType'][S], Axes[S][1], {'ylim': [Min, Max]}, Save=False, Show=False)
# Plt.show()

# KlPlot.LinePSTH(UnitRec['PSTH'][Ind[-1]], UnitRec['PSTHX'][:,Ind[-1]], [[0,3]], ['Sound'], AxArgs={'xlim':[0,50]}, Save=False, Show=True)

print(FigFile)
KlPlot.WF_Raster_PSTH_AllRecs([UnitRec['Spks'][_] for _ in Ind],
                          [UnitRec['Raster'][_] for _ in Ind],
                          UnitRec['RasterX'][:,Ind],
                          [UnitRec['PSTH'][_] for _ in Ind],
                          UnitRec['PSTHX'][:,Ind],
                          Rate, ThisId['StimWindow'], ThisId['StimType'],
                          AxArgs, FigFile, Save=Save, Show=Show)


#%%
Show = True
Save = False
BinSize = 1
Lim = 60

# DataDir is in the first cell

AsdfFiles = sorted(glob(DataDir+'/Optog*/Analysis-Bin1sample/**/*.asdf', recursive=True))

FiringPattern = {
    'Mean': np.array([], dtype='float32'),
    'StimType': np.array([], dtype='<U30'),
    'PSTH': np.array([], dtype='float32')
}

for A, AsdfFile in enumerate(AsdfFiles):
    UnitRec = IO.Asdf.Read(AsdfFile)
    if not UnitRec['PSTH']: continue
    print(AsdfFile)

    if BinSize:
        Rate = round(1/(UnitRec['RasterX'][1,0]-UnitRec['RasterX'][0,0]))*1000
        UnitRec['PSTHX'] = np.tile(np.arange(UnitRec['PSTHX'][:,0][0],
                                             UnitRec['PSTHX'][:,0][-1],
                                             BinSize),
                                   (UnitRec['PSTHX'].shape[1],1)).T

        for i in range(len(UnitRec['PSTH'])):
            UnitRec['PSTH'][i] = np.vstack((UnitRec['PSTH'][i], np.zeros((1,UnitRec['PSTH'][i].shape[1]))))
            UnitRec['PSTH'][i] = UnitRec['PSTH'][i].reshape((int(UnitRec['PSTH'][i].shape[0]/(BinSize*Rate/1000)),
                                                             int(BinSize*Rate/1000),
                                                             UnitRec['PSTH'][i].shape[1])).sum(axis=1)

            UnitRec['PSTH'][i] = UnitRec['PSTH'][i][:-1,:]

    uFiringPattern = np.array([], dtype='float32')
    for U, Unit in enumerate(UnitRec['PSTH']):
        if not Unit.mean(): continue
        if UnitRec['SpkResp'][U] > 0.05: continue

        FP = Unit.mean(axis=1)
        FP -= FP.min()
        FP /= FP.max()

        uFiringPattern = np.hstack((uFiringPattern, FP.mean()))

        if not len(FiringPattern['PSTH']): FiringPattern['PSTH'] = np.array([FP]).T
        else: FiringPattern['PSTH'] = np.hstack((FiringPattern['PSTH'],
                                                 np.array([FP]).T))

    FiringPattern['Mean'] = np.hstack((FiringPattern['Mean'], uFiringPattern))
    FiringPattern['StimType'] = np.hstack((FiringPattern['StimType'], UnitRec['StimType']))


Plt = KlPlot.Plot.Return('plt')
# from IPython import display

Patterns = [[],[]]
Patterns[0],Patterns[1] = np.histogram(FiringPattern['Mean'], 50)
Patterns[0] = DataAnalysis.GetPeaks(Patterns[0], FixedThreshold=1)['Pos']
Patterns = Patterns[1][Patterns[0]]

Start, End = -10, 20
Fig, Axes = Plt.subplots(len(Patterns),1, sharex=True)
for U in range(FiringPattern['PSTH'].shape[1]):
    PSTH = FiringPattern['PSTH'][:,U]

    Max = PSTH.argmax()
    if Max < abs(Start): continue
    elif Max+abs(End) > PSTH.shape[0]: continue

    PSTH = PSTH[Max+Start:Max+End]
    if PSTH[-1] == 1: PSTH[-1] = 0

    if len(PSTH[PSTH==PSTH.max()]) > 1: continue
    # if len(DataAnalysis.GetPeaks(PSTH, FixedThreshold=0.8-PSTH.mean())['Pos']) >= 2:
    #     continue

    Ind = abs(Patterns-FiringPattern['Mean'][U]).argmin()

    Axes[Ind].plot(PSTH, 'k')
    # display.display()
    # display.clear_output(wait=True)
    # Plt.pause(0.2)

FPs = []
for Ax in Axes:
    FPs.append([_.get_ydata() for _ in Ax.get_lines()])
    Ax.set_ylim([0,1])
Plt.show()

Fig, Axes = Plt.subplots(len(Patterns),1)
for f,F in enumerate(FPs):
    Axes[f].plot(np.mean(F, axis=0))
    Axes[f].text(0, 0.5, str(len(F)))
    # Axes[f].set_ylim([0,1])
Plt.show()

# IO.Asdf.Write


#%% Raw data
plt = KlPlot.Plot.Return('Plt')

DataDir = '/home/malfatti/Documents/MSc/Data'
PrmFile = DataDir+'/OptogeneticInhibition/20150417-CaMKIIaArch3n03-UnitRec/KlustaFiles/Exp03.prm'
with open(PrmFile, 'r') as F: Prm = load_source('Prm', '', F)
Files = Prm.traces['raw_data_files']
Stims = Prm.DataInfo['Stims']

Data = Files[-1]
Data = IO.Bin.Read(Data)[0]
Data = DataAnalysis.FilterSignal(Data[:,:-1], 25000, [300, 6000])
