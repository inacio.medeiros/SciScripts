# -*- coding: utf-8 -*-
"""
Created on 20181018
@author: malfatti

Class Functions
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as ssig
import scipy.special as ssp

from itertools import accumulate


def Entropy(Hist):
    Probs = Hist/np.sum(Hist)
    H = -np.sum(Probs[Probs > 0] * np.log(Probs[Probs > 0]))
    HMax = np.log(Hist.shape[0])
    return(H, HMax)


def GetEntropyMI(Hist):
    H = Entropy(Hist)[0]
    Probs = Hist/np.sum(Hist)
    MI = (np.log(Probs.shape[0])-H)/np.log(Probs.shape[0])
    return(MI)


def Coupling(H, HMax):
    Mi = (HMax - H)/HMax
    return(Mi)


def VonMises(Phi, PhiMean, Kappa):
    VM = np.exp(Kappa * (np.cos(Phi - PhiMean)))
    VM /= 2 * np.pi * ssp.iv(0, Kappa)
    return(VM)


## Plots
# Define function to plot channels in the same plot with an offset
def GetSpaces(Signal, AmpF=1.1):
    """ Modified from https://gitlab.com/Malfatti/SciScripts/blob/master/Python3/Deps/DataAnalysis/Plot/Plot.py """
    Spaces = [abs(Signal[:,C].min())*AmpF for C in range(Signal.shape[1]-1)]
    Spaces = [0]+list(accumulate(Spaces))

    return(Spaces)


## Define function to plot a single point and an animation of points
def PlotZ(Z, XLim=None, YLim=None, Title=None, Ax=None):
    Return = True
    if not Ax:
        Fig, Ax = plt.subplots()
        Return = False

    Ax.plot([0, np.real(Z)], [0, np.imag(Z)], 'k-')
    Ax.plot(np.real(Z), np.imag(Z), 'ko')
    if XLim: Ax.set_xlim(XLim)
    if YLim: Ax.set_ylim(YLim)
    if Title: Ax.set_title(Title)

    if Return: return(Ax)
    else: plt.show()


def CheckSignal(Signal, Rate, SpaceAmpF=0.3, TimeWindow=0.2, SkipTime=10, ChLabels=[]):
    for C in range(Signal.shape[1]): Signal[:,C] /= Signal[:,C].max()
    Signal[:,-1] *= 0.6

    Spaces = GetSpaces(Signal, SpaceAmpF)
    if not ChLabels:
        ChLabels = list(range(1, Signal.shape[1]+1))

    SkipTime = int(Rate*SkipTime/1000)

    try:
        Fig, Ax = plt.subplots(dpi=150, figsize=(7,6))
        for S in range(0,Signal.shape[0]-int(TimeWindow*Rate),SkipTime):
            t = np.arange(S, (TimeWindow*Rate)+S, 1)/Rate

            YTicks = []
            for Ch in range(Signal.shape[1]):
                Y = Signal[S:int(TimeWindow*Rate)+S,Ch] - Spaces[Ch]
                YTicks.append(Y.mean())
                Ax.plot(t, Y, 'k')

            XBar = [t[t.shape[0]//2], t[t.shape[0]//2+int(TimeWindow*0.2*Rate)]]
            Ax.plot(XBar, [-Spaces[-1]*1.05]*2, 'k', lw=2)

            Ax.set_yticks(YTicks)
            Ax.set_yticklabels(ChLabels)
            Ax.set_xlabel('Time [s]')
            Ax.set_ylabel('Depth [µm]')

            plt.pause(0.001)
            Ax.clear()
        plt.show()

    except KeyboardInterrupt:
        print('Stopping animation...')
        plt.close('all')
        return(None)


def PlotSignals(Signals, ts, Fs, PSDs, Labels):
    Fig, Axes = plt.subplots(2,1)
    for S,Signal in enumerate(Signals):
        if S != 0: Marker = 'o'; LineStyle = ''
        else: Marker, LineStyle = None, None

        MaxFreq = GetPeaks(PSDs[S])['Pos']

        Axes[0].plot(ts[S], Signal, marker=Marker, linestyle=LineStyle, label=Labels[S])
        Axes[1].plot(Fs[S], PSDs[S], label=Labels[S])
        Axes[1].plot(Fs[S][MaxFreq], PSDs[S][MaxFreq], 'ko')

        Axes[0].set_xlim([0,1])
        Axes[1].set_xlim([0, 500])
    Axes[1].legend()
    plt.show()

    return(None)


def PlotAllTrials(Signal, t):
    plt.figure()
    Axes = [
            plt.subplot2grid((5, 1), (0, 0), rowspan=4),
            plt.subplot2grid((5, 1), (4, 0))
    ]

    TrialNo = Signal.shape[1]
    for Trial in range(TrialNo):
        Axes[0].plot(t-1.5, Signal[:,Trial]+Trial+1, 'k')

    Axes[0].plot([0,0], [1, TrialNo])
    Axes[1].plot(t-1.5, Signal.mean(axis=1))

    Axes[0].set_ylabel('Trial No')
    Axes[1].set_xlabel('Time [s]')
    Axes[1].set_ylabel('ERP')
    plt.show()


def PlotHeatmap(Signal, t):
    plt.figure()
    Axes = [
            plt.subplot2grid((5, 1), (0, 0), rowspan=4),
            plt.subplot2grid((5, 1), (4, 0))
    ]

    TrialNo = Signal.shape[1]
    Axes[0].contourf(t-1.5, np.arange(TrialNo), Signal.T, cmap='inferno')
    Axes[0].plot([0,0], [1, TrialNo])
    Axes[1].plot(t-1.5, Signal.mean(axis=1))

    Axes[0].set_ylabel('Trial No')
    Axes[1].set_xlabel('Time [s]')
    Axes[1].set_ylabel('ERP')
    plt.show()


def PlotSigHilbert(Signal, Signal_Filt, t, Freqs, AmpSxx):
    Fig, Axes = plt.subplots(2,1)
    Axes[0].plot(t, Signal, 'k')
    Axes[1].plot(Freqs, AmpSxx)
    for f,F in enumerate(Freqs):
        Axes[0].plot(t, Signal_Filt[:,f], 'r')
        Axes[0].plot(t, abs(ssig.hilbert(Signal_Filt[:,f])), 'b')
        Axes[0].set_title(str(F)+'Hz')

        plt.pause(0.005)
        for _ in range(2): Axes[0].get_lines()[-1].remove()

    plt.show()

