# -*- coding: utf-8 -*-
"""
Created on 20181122
@author: malfatti
"""
#%% Prepare
import nidaqmx as NI
import numpy as np

Rate = 10000
Duration = 10
Freq = 40
Amplitude = 2

Signal = -np.cos(2 * np.pi * Freq * np.arange(Rate*Duration)/Rate)
Signal -= Signal.min()
Signal *= Amplitude/2
Signal[-1] = 0


#%% Run
with NI.Task() as Task:
    Task.ao_channels.add_ao_voltage_chan("Dev2/ao0")
    Task.write(Signal, auto_start=True)

#%%

Task = NI.Task()
Task.ao_channels.add_ao_voltage_chan('Dev2/ao1')
#Task.timing.cfg_samp_clk_timing(rate=Rate, sample_mode=AcquisitionType.FINITE, samps_per_chan=int(Duration*Rate))

test_Writer = nidaqmx.stream_writers.AnalogSingleChannelWriter(test_Task.out_stream, auto_start=True)

samples = np.append(5*np.ones(30), np.zeros(10))

test_Writer.write_many_sample(samples)
test_Task.wait_until_done()
test_Task.stop()
test_Task.close()


#%%
from PyDAQmx import Task, DAQmx_Val_Volts
import numpy as np

Array = np.sin(2*np.pi*40*np.arange(1000)/1000)
task = Task()
task.CreateAOVoltageChan("/Dev2/ao0", "", -5.0, 5.0, DAQmx_Val_Volts, None)
task.StartTask()
task.WriteAnalogF64(Array.shape[0], 0, 10.0, DAQmx_Val_GroupByScanNumber, Array, byref(sampsPerChanWritten), None)
task.StopTask()
